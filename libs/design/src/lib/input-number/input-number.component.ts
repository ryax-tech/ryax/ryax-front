// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component, Input } from '@angular/core';

@Component({
  selector: 'ryax-input-number',
  templateUrl: './input-number.component.pug',
})
export class InputNumberComponent {
  @Input() disabled = false;
}

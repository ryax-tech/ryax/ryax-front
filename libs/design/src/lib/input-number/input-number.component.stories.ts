// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { moduleMetadata, Story, Meta } from '@storybook/angular';
import { InputNumberComponent } from './input-number.component';
import { NzInputNumberModule } from 'ng-zorro-antd/input-number';

export default {
  title: 'InputNumberComponent',
  component: InputNumberComponent,
  decorators: [
    moduleMetadata({
      imports: [NzInputNumberModule],
    }),
  ],
} as Meta<InputNumberComponent>;

const Template: Story<InputNumberComponent> = (args: InputNumberComponent) => ({
  props: args,
});

export const Base = Template.bind({});
Base.args = {
  disabled: false,
};
Base.argTypes = {
  disabled: {
    control: { type: 'boolean' },
  },
};

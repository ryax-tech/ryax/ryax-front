// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { moduleMetadata, Story, Meta } from '@storybook/angular';
import { ButtonComponent } from './button.component';
import { NzButtonModule } from 'ng-zorro-antd/button';

export default {
  title: 'ButtonComponent',
  component: ButtonComponent,
  decorators: [
    moduleMetadata({
      imports: [NzButtonModule],
    }),
  ],
} as Meta<ButtonComponent>;

const Template: Story<ButtonComponent> = (args: ButtonComponent) => ({
  props: args,
});

export const Primary = Template.bind({});
Primary.args = {
  type: 'primary',
  text: 'button',
  size: 'default',
  loading: false,
  disabled: false,
  danger: false,
  ghost: false,
};
Primary.argTypes = {
  type: {
    options: ['primary', 'default', 'dashed', 'text', 'link'],
    control: { type: 'select' },
  },
  text: {
    control: { type: 'text' },
  },
  size: {
    options: ['large', 'default', 'small'],
    control: { type: 'select' },
  },
  shape: {
    options: [null, 'circle', 'round'],
    control: { type: 'select' },
  },
  loading: {
    control: { type: 'boolean' },
  },
  disabled: {
    control: { type: 'boolean' },
  },
  danger: {
    control: { type: 'boolean' },
  },
  ghost: {
    control: { type: 'boolean' },
  },
  buttonClick: {
    action: 'clicked',
  },
};

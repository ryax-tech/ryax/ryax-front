// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UploadComponent } from './upload.component';
import { NzUploadFile, NzUploadModule } from 'ng-zorro-antd/upload';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

describe('UploadComponent', () => {
  let component: UploadComponent;
  let fixture: ComponentFixture<UploadComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [NzUploadModule],
      declarations: [UploadComponent],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UploadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should allow to register onChange', () => {
    const onChange = jest.fn;

    component.registerOnChange(onChange);

    expect(component['onChange']).toBe(onChange);
  });

  it('should allow to register onTouched', () => {
    const onTouched = jest.fn;

    component.registerOnTouched(onTouched);

    expect(component['onTouched']).toBe(onTouched);
  });

  it('should write file name', () => {
    component.writeValue('name');

    expect(component.fileName).toBe('name');
  });

  it('should prevent default before auto upload and act like input file', () => {
    const onChangeSpy = jest.fn();
    const onTouchedSpy = jest.fn();
    const fakeFile = {
      name: 'File name',
      size: 3,
      uid: 'file-id',
    } as NzUploadFile;

    component.registerOnChange(onChangeSpy);
    component.registerOnTouched(onTouchedSpy);

    component.onInputChange(fakeFile);

    expect(onChangeSpy).toHaveBeenCalledWith(fakeFile);
    expect(onTouchedSpy).toHaveBeenCalledWith();
    expect(component.fileName).toBe('File name');
  });

  it('should also handle change for directory', () => {
    component.nzDirectory = true;
    const onChangeSpy = jest.fn();
    const onTouchedSpy = jest.fn();
    const fakeFile1 = {
      name: 'File name',
      size: 3,
      uid: 'file-id',
    } as NzUploadFile;
    const fakeFile2 = {
      name: 'File name2',
      size: 6,
      uid: 'file-id2',
    } as NzUploadFile;

    component.registerOnChange(onChangeSpy);
    component.registerOnTouched(onTouchedSpy);

    component.onInputChange(fakeFile1);

    expect(onChangeSpy).toHaveBeenCalledWith(fakeFile1);
    expect(onTouchedSpy).toHaveBeenCalledWith();
    expect(component.fileList).toEqual([fakeFile1]);

    onChangeSpy.mockClear();
    onTouchedSpy.mockClear();

    component.onInputChange(fakeFile2);

    expect(onChangeSpy).toHaveBeenCalledWith(fakeFile2);
    expect(onTouchedSpy).toHaveBeenCalledWith();
    expect(component.fileList).toEqual([fakeFile1, fakeFile2]);
  });

  it('should clear file when asked', () => {
    const deleteSpy = jest.spyOn(component.delete, 'emit');
    const onChangeSpy = jest.fn();
    const onTouchedSpy = jest.fn();

    component.fileName = 'Test Name';
    component.registerOnChange(onChangeSpy);
    component.registerOnTouched(onTouchedSpy);

    component.clearFile();

    expect(deleteSpy).toHaveBeenCalledWith();
    expect(onChangeSpy).toHaveBeenCalledWith();
    expect(onTouchedSpy).toHaveBeenCalledWith();
    expect(component.fileName).toBe('');
  });

  it('should also clear for directory', () => {
    component.nzDirectory = true;
    const deleteSpy = jest.spyOn(component.delete, 'emit');
    const onChangeSpy = jest.fn();
    const onTouchedSpy = jest.fn();

    component.fileList = [
      { name: 'File name', size: 3, uid: 'file-id' } as NzUploadFile,
    ];
    component.registerOnChange(onChangeSpy);
    component.registerOnTouched(onTouchedSpy);

    component.clearFile();

    expect(deleteSpy).toHaveBeenCalledWith();
    expect(onChangeSpy).toHaveBeenCalledWith();
    expect(onTouchedSpy).toHaveBeenCalledWith();
    expect(component.fileList).toEqual([]);
  });

  it('should allow to disable/enable file inputs', () => {
    component.setDisabledState(true);
    expect(component.isDisabled).toBe(true);

    component.setDisabledState(false);
    expect(component.isDisabled).toBe(false);
  });
});

// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component, Input } from '@angular/core';

@Component({
  selector: 'ryax-input',
  templateUrl: './input.component.pug',
})
export class InputComponent {
  public passwordVisible = false;

  @Input() public type: 'password' | 'text' | 'textarea' = 'text';
}

#/usr/bin/env bash
set -e
# For debug
# set -x

AUTO_FORMAT="false"

while getopts "f" opt; do
  case "$opt" in
    f)
      AUTO_FORMAT="true"
      ;;
    ?)
      echo "script usage: $(basename $0) [-f]" >&2
      echo "-f : force apply format change"
      exit 1
      ;;
  esac
done

if [ "$AUTO_FORMAT" == "true" ]
then
  FORMAT_OPT=":write"
else
  FORMAT_OPT=""
fi

echo "Linting..."
yarn lint
echo "Check Format..."
yarn format$FORMAT_OPT
echo Done!

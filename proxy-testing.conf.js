module.exports = {
  // Redirect to fake server
  '/api': {
    target: `https://dev-1.ryax.io`,
    secure: false,
    changeOrigin: true,
    ws: true,
    logLevel: "debug"
  },
  '*/ws': {
    target: "wss://dev-1.ryax.io",
    secure: false,
    ws: true,
    logLevel: "debug"
  }
};

// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
// eslint-disable-next-line @typescript-eslint/ban-types
import { CheckForUpdateService } from './modules/utils/services/check-updates.service';

// eslint-disable-next-line @typescript-eslint/ban-types
declare let gtag: Function;

import { Component, OnInit } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { environment } from '../environments/environment';
import { PromptUpdateService } from './modules/utils/services/promp-update.service';

@Component({
  selector: 'ryax-root',
  templateUrl: './app.component.pug',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  constructor(
    private router: Router,
    private checkUpdate: CheckForUpdateService,
    private promptUpdate: PromptUpdateService
  ) {}

  public ngOnInit() {
    if (environment.demoTag) {
      this.setUpAnalytics();
    }
  }

  private setUpAnalytics() {
    this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        gtag('config', environment.demoTag, {
          page_path: event.urlAfterRedirects,
        });
      }
    });
  }
}

// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Route } from '@angular/router';
import { AlreadyLoggedGuard } from './modules/auth/guards/already-logged/already-logged.guard';
import { IsLoggedGuard } from './modules/auth/guards/is-logged/is-logged.guard';
import { HeaderComponent } from './modules/header/header/header.component';
import { LayoutComponent } from './modules/shared/components';

const authRoute: Route = {
  path: 'login',
  canActivate: [AlreadyLoggedGuard],
  loadChildren: () =>
    import('./modules/auth/auth-access.module').then((m) => m.AuthAccessModule),
};

const projectRoute: Route = {
  path: '',
  children: [
    {
      path: '',
      component: LayoutComponent,
      loadChildren: () =>
        import('./modules/project/project-access.module').then(
          (m) => m.ProjectAccessModule
        ),
    },
  ],
};

const dashboardRoute: Route = {
  path: 'dashboard',
  loadChildren: () =>
    import('./modules/dashboard/dashboard.module').then(
      (m) => m.DashboardModule
    ),
};

const userSettingsRoute: Route = {
  path: 'user',
  loadChildren: () =>
    import('./modules/user-settings/user-settings.module').then(
      (m) => m.UserSettingsModule
    ),
};

const workflowRoute: Route = {
  path: 'workflow',
  children: [
    {
      path: '',
      component: LayoutComponent,
      loadChildren: () =>
        import('./modules/workflow/workflow-access.module').then(
          (m) => m.WorkflowAccessModule
        ),
    },
  ],
};

const studioRoute: Route = {
  path: 'studio',
  children: [
    {
      path: '',
      component: LayoutComponent,
      loadChildren: () =>
        import('./modules/studio/studio-access.module').then(
          (m) => m.StudioAccessModule
        ),
    },
  ],
};

const libraryRoute: Route = {
  path: 'library',
  children: [
    {
      path: '',
      component: LayoutComponent,
      loadChildren: () =>
        import('./modules/library/library-access.module').then(
          (m) => m.LibraryAccessModule
        ),
    },
  ],
};

const runsRoute: Route = {
  path: 'runs',
  children: [
    {
      path: '',
      component: LayoutComponent,
      loadChildren: () =>
        import('./modules/runs/runs-access.module').then(
          (m) => m.RunsAccessModule
        ),
    },
  ],
};

const redirect: Route = {
  path: '',
  pathMatch: 'full',
  redirectTo: '/login',
};

const wildcardRoute: Route = {
  path: '**',
  redirectTo: 'dashboard',
};

const loggedRoutes: Route = {
  path: '',
  canActivate: [IsLoggedGuard],
  component: HeaderComponent,
  children: [
    runsRoute,
    dashboardRoute,
    projectRoute,
    workflowRoute,
    studioRoute,
    libraryRoute,
    userSettingsRoute,
    redirect,
  ],
};

export const appRoutes = [loggedRoutes, authRoute, redirect, wildcardRoute];

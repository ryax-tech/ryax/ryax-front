import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CheckForUpdateService } from './services/check-updates.service';
import { PromptUpdateService } from './services/promp-update.service';

@NgModule({
  declarations: [],
  imports: [CommonModule],
  providers: [CheckForUpdateService, PromptUpdateService],
})
export class UtilsModule {}

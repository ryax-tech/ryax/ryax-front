// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Pipe, PipeTransform } from '@angular/core';
import { WorkflowActionLight } from '../../../studio/entities/workflow-action';

@Pipe({
  name: 'filterName',
})
export class FilterNamePipe implements PipeTransform {
  public transform(
    list: WorkflowActionLight[] | null,
    searchTerm: string
  ): WorkflowActionLight[] {
    if (!list) {
      return [];
    }
    return list.filter((value) =>
      value.name.toLowerCase().includes(searchTerm.toLowerCase())
    );
  }
}

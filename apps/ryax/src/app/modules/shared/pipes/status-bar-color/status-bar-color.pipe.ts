// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Pipe, PipeTransform } from '@angular/core';
import {
  ActionRunStatus,
  TimelineColors,
  WorkflowRunStatus,
} from '../../entity';

@Pipe({
  name: 'statusBarColor',
})
export class StatusBarColorPipe implements PipeTransform {
  public transform(
    status: WorkflowRunStatus | ActionRunStatus,
    part: 'start' | 'item' | 'end'
  ): TimelineColors {
    if (part !== 'item') {
      switch (status) {
        case WorkflowRunStatus.Created:
          return TimelineColors.Purple;
        case WorkflowRunStatus.Pending:
        case WorkflowRunStatus.Running:
        case WorkflowRunStatus.Deploying:
        case WorkflowRunStatus.Deployed:
        case WorkflowRunStatus.Error:
          return part === 'start' ? TimelineColors.Red : TimelineColors.Purple;
        case WorkflowRunStatus.Completed:
          return TimelineColors.Red;
        default:
          return TimelineColors.Purple;
      }
    } else {
      switch (status) {
        case ActionRunStatus.Waiting:
          return TimelineColors.Purple;
        case ActionRunStatus.Running:
        case ActionRunStatus.Error:
          return TimelineColors.Gradient;
        case ActionRunStatus.Success:
          return TimelineColors.Red;
        default:
          return TimelineColors.Purple;
      }
    }
  }
}

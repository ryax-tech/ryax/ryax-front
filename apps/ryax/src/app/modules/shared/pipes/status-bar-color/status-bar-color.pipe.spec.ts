// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import {
  ActionRunStatus,
  TimelineColors,
  WorkflowRunStatus,
} from '../../entity';
import { StatusBarColorPipe } from '../index';

describe('StatusBarColorPipe', () => {
  it('create an instance', () => {
    const pipe = new StatusBarColorPipe();
    expect(pipe).toBeTruthy();
  });

  it('should transform status into a color string', () => {
    const pipe = new StatusBarColorPipe();
    expect(pipe.transform(WorkflowRunStatus.Created, 'item')).toEqual(
      TimelineColors.Purple
    );
    expect(pipe.transform(WorkflowRunStatus.Pending, 'start')).toEqual(
      TimelineColors.Red
    );
    expect(pipe.transform(WorkflowRunStatus.Error, 'end')).toEqual(
      TimelineColors.Purple
    );
    expect(pipe.transform(WorkflowRunStatus.Canceled, 'end')).toEqual(
      TimelineColors.Purple
    );
    expect(pipe.transform(WorkflowRunStatus.Completed, 'end')).toEqual(
      TimelineColors.Red
    );
    expect(pipe.transform('' as WorkflowRunStatus, 'start')).toEqual(
      TimelineColors.Purple
    );
    expect(pipe.transform(ActionRunStatus.Waiting, 'item')).toEqual(
      TimelineColors.Purple
    );
    expect(pipe.transform(ActionRunStatus.Running, 'item')).toEqual(
      TimelineColors.Gradient
    );
    expect(pipe.transform(ActionRunStatus.Success, 'item')).toEqual(
      TimelineColors.Red
    );
    expect(pipe.transform('' as ActionRunStatus, 'item')).toEqual(
      TimelineColors.Purple
    );
  });
});

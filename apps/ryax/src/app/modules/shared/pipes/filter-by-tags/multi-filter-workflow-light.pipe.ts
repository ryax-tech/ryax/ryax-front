import { Pipe, PipeTransform } from '@angular/core';
import {
  WorkflowFilters,
  WorkflowLight,
} from '../../../workflow/entities/index';

@Pipe({
  name: 'multiFilterWorkflowLight',
})
export class MultiFilterWorkflowLightPipe implements PipeTransform {
  public transform(
    workflowList: WorkflowLight[] | null,
    filters: WorkflowFilters,
    searchTerm: string
  ): WorkflowLight[] {
    if (!workflowList) {
      return [];
    }
    return workflowList
      .filter((workflow) =>
        workflow.name.toLowerCase().includes(searchTerm.toLowerCase())
      )
      .filter((workflow) => {
        return (
          (filters.deploymentStatus.length === 0 ||
            filters.deploymentStatus.includes(workflow.deploymentStatus)) &&
          (filters.categories.length === 0 ||
            filters.categories.every((cat) =>
              workflow.categories.includes(cat)
            )) &&
          (filters.endpoint.length === 0 ||
            filters.endpoint.includes(workflow.endpoint)) &&
          (filters.trigger.length === 0 ||
            filters.trigger.includes(workflow.trigger))
        );
      });
  }
}

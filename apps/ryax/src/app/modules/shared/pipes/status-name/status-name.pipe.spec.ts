// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { WorkflowRunStatus } from '../../entity';
import { StatusNamePipe } from '../index';

describe('StatusNamePipe', () => {
  it('create an instance', () => {
    const pipe = new StatusNamePipe();
    expect(pipe).toBeTruthy();
  });

  it('should transform status into a name string', () => {
    const pipe = new StatusNamePipe();
    expect(pipe.transform(WorkflowRunStatus.Created)).toEqual('Created');
    expect(pipe.transform(WorkflowRunStatus.Pending)).toEqual('Pending');
    expect(pipe.transform(WorkflowRunStatus.Deploying)).toEqual('Deploying');
    expect(pipe.transform(WorkflowRunStatus.Deployed)).toEqual('Deployed');
    expect(pipe.transform(WorkflowRunStatus.Running)).toEqual('Running');
    expect(pipe.transform(WorkflowRunStatus.Error)).toEqual('Error');
    expect(pipe.transform(WorkflowRunStatus.Completed)).toEqual('Completed');
    expect(pipe.transform('' as WorkflowRunStatus)).toEqual('');
  });
});

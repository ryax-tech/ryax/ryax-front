// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Pipe, PipeTransform } from '@angular/core';
import { WorkflowRunStatus } from '../../entity';

@Pipe({
  name: 'statusName',
})
export class StatusNamePipe implements PipeTransform {
  public transform(value: WorkflowRunStatus): string {
    switch (value) {
      case WorkflowRunStatus.Created:
        return 'Created';
      case WorkflowRunStatus.Pending:
        return 'Pending';
      case WorkflowRunStatus.Deploying:
        return 'Deploying';
      case WorkflowRunStatus.Deployed:
        return 'Deployed';
      case WorkflowRunStatus.Running:
        return 'Running';
      case WorkflowRunStatus.Error:
        return 'Error';
      case WorkflowRunStatus.Canceled:
        return 'Canceled';
      case WorkflowRunStatus.Completed:
        return 'Completed';
      default:
        return '';
    }
  }
}

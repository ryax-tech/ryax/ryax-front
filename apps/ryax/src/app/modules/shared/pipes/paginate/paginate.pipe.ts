import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'paginate',
})
export class PaginatePipe implements PipeTransform {
  public transform<T>(array: T[], pageNumber: number, pageSize: number): T[] {
    return [...array.slice((pageNumber - 1) * pageSize, pageNumber * pageSize)];
  }
}

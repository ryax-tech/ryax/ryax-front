// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Pipe, PipeTransform } from '@angular/core';
import { ColorsEnum, WorkflowRunStatus } from '../../entity';

@Pipe({
  name: 'statusTagColor',
})
export class StatusTagColorPipe implements PipeTransform {
  public transform(value: WorkflowRunStatus): string {
    switch (value) {
      case WorkflowRunStatus.Created:
      case WorkflowRunStatus.Pending:
        return ColorsEnum.Default;
      case WorkflowRunStatus.Deploying:
      case WorkflowRunStatus.Deployed:
      case WorkflowRunStatus.Running:
        return ColorsEnum.Primary;
      case WorkflowRunStatus.Canceled:
      case WorkflowRunStatus.Error:
        return ColorsEnum.Danger;
      case WorkflowRunStatus.Completed:
        return ColorsEnum.Success;
      default:
        return ColorsEnum.Default;
    }
  }
}

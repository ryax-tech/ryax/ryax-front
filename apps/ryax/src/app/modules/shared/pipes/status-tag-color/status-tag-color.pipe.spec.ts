// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { ColorsEnum, WorkflowRunStatus } from '../../entity';
import { StatusTagColorPipe } from '../index';

describe('StatusIconColorPipe', () => {
  it('create an instance', () => {
    const pipe = new StatusTagColorPipe();
    expect(pipe).toBeTruthy();
  });

  it('should transform status into a color string', () => {
    const pipe = new StatusTagColorPipe();
    expect(pipe.transform(WorkflowRunStatus.Created)).toEqual(
      ColorsEnum.Default
    );
    expect(pipe.transform(WorkflowRunStatus.Pending)).toEqual(
      ColorsEnum.Default
    );
    expect(pipe.transform(WorkflowRunStatus.Running)).toEqual(
      ColorsEnum.Primary
    );
    expect(pipe.transform(WorkflowRunStatus.Deploying)).toEqual(
      ColorsEnum.Primary
    );
    expect(pipe.transform(WorkflowRunStatus.Deployed)).toEqual(
      ColorsEnum.Primary
    );
    expect(pipe.transform(WorkflowRunStatus.Error)).toEqual(ColorsEnum.Danger);
    expect(pipe.transform(WorkflowRunStatus.Canceled)).toEqual(
      ColorsEnum.Danger
    );
    expect(pipe.transform(WorkflowRunStatus.Completed)).toEqual(
      ColorsEnum.Success
    );
    expect(pipe.transform('' as WorkflowRunStatus)).toEqual(ColorsEnum.Default);
  });
});

// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component, Input } from '@angular/core';
import { WorkflowActionLight } from '../../../studio/entities/workflow-action';

@Component({
  selector: 'ryax-action-mini-card',
  templateUrl: './action-mini-card.component.pug',
  styleUrls: ['./action-mini-card.component.scss'],
})
export class ActionMiniCardComponent {
  @Input() public action!: WorkflowActionLight;
  @Input() public active = false;
}

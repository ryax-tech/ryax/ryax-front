// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component, Input } from '@angular/core';
import { ActionRunStatus, TimelineColors } from '../../entity';

@Component({
  selector: 'ryax-timeline-item',
  templateUrl: './timeline-item.component.pug',
  styleUrls: ['./timeline-item.component.scss'],
})
export class TimelineItemComponent {
  @Input() public color: TimelineColors = TimelineColors.Purple;
  @Input() public status: ActionRunStatus | undefined;
  @Input() public position: number | undefined;
}

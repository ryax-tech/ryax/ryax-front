// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { ColorsEnum, ActionRunStatus } from '../../entity';

import { StatusIconComponent } from './status-icon.component';

describe('StatusIconComponent', () => {
  let component: StatusIconComponent;
  let fixture: ComponentFixture<StatusIconComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [NzIconModule],
      declarations: [StatusIconComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StatusIconComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should transform status into a theme string', () => {
    expect(component.getTheme(ActionRunStatus.Waiting)).toEqual('outline');
    expect(component.getTheme(ActionRunStatus.Running)).toEqual('outline');
    expect(component.getTheme(ActionRunStatus.Error)).toEqual('fill');
    expect(component.getTheme(ActionRunStatus.Success)).toEqual('fill');
    expect(component.getTheme('' as ActionRunStatus)).toEqual('outline');
  });

  it('should transform status into a icon string', () => {
    expect(component.getType(ActionRunStatus.Waiting)).toEqual('clock-circle');
    expect(component.getType(ActionRunStatus.Running)).toEqual('loading');
    expect(component.getType(ActionRunStatus.Error)).toEqual('close-circle');
    expect(component.getType(ActionRunStatus.Success)).toEqual('check-circle');
    expect(component.getType('' as ActionRunStatus)).toEqual('clock-circle');
  });

  it('should transform status into a color string', () => {
    expect(component.getColor(ActionRunStatus.Waiting)).toEqual(
      ColorsEnum.Default
    );
    expect(component.getColor(ActionRunStatus.Running)).toEqual(
      ColorsEnum.Primary
    );
    expect(component.getColor(ActionRunStatus.Error)).toEqual(
      ColorsEnum.Danger
    );
    expect(component.getColor(ActionRunStatus.Success)).toEqual(
      ColorsEnum.Success
    );
    expect(component.getColor('' as ActionRunStatus)).toEqual(
      ColorsEnum.Default
    );
  });
});

// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component, Input } from '@angular/core';
import { ColorsEnum, ActionRunStatus } from '../../entity';

@Component({
  selector: 'ryax-status-icon',
  templateUrl: './status-icon.component.pug',
  styleUrls: ['./status-icon.component.scss'],
})
export class StatusIconComponent {
  @Input() public status: ActionRunStatus = ActionRunStatus.Waiting;

  public getType(value: ActionRunStatus): string {
    switch (value) {
      case ActionRunStatus.Waiting:
        return 'clock-circle';
      case ActionRunStatus.Pending:
      case ActionRunStatus.Running:
        return 'loading';
      case ActionRunStatus.Error:
      case ActionRunStatus.Canceled:
        return 'close-circle';
      case ActionRunStatus.Success:
        return 'check-circle';
      default:
        return 'clock-circle';
    }
  }

  public getColor(value: ActionRunStatus): string {
    switch (value) {
      case ActionRunStatus.Waiting:
        return ColorsEnum.Default;
      case ActionRunStatus.Pending:
        return ColorsEnum.Processing;
      case ActionRunStatus.Running:
        return ColorsEnum.Primary;
      case ActionRunStatus.Error:
      case ActionRunStatus.Canceled:
        return ColorsEnum.Danger;
      case ActionRunStatus.Success:
        return ColorsEnum.Success;
      default:
        return ColorsEnum.Default;
    }
  }

  public getTheme(value: ActionRunStatus): 'fill' | 'outline' | 'twotone' {
    switch (value) {
      case ActionRunStatus.Waiting:
      case ActionRunStatus.Pending:
      case ActionRunStatus.Running:
        return 'outline';
      case ActionRunStatus.Error:
      case ActionRunStatus.Success:
      case ActionRunStatus.Canceled:
        return 'fill';
      default:
        return 'outline';
    }
  }
}

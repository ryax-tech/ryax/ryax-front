// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { HttpClient } from '@angular/common/http';
import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  Output,
  SimpleChanges,
} from '@angular/core';
import { RunIOData } from '../../entity';
import { saveAs } from 'file-saver';

@Component({
  selector: 'ryax-module-execution-ios',
  templateUrl: './module-execution-ios.component.pug',
  styleUrls: ['./module-execution-ios.component.scss'],
})
export class ModuleExecutionIosComponent implements OnChanges {
  @Input() public data: RunIOData[] = [];
  @Output() public showModal = new EventEmitter<void>();
  public passwordVisibility: { [name: string]: boolean } = {};

  constructor(private http: HttpClient) {}

  public ngOnChanges(changes: SimpleChanges): void {
    if (changes['data'].currentValue) {
      changes['data'].currentValue.forEach((io: RunIOData) => {
        if (io.type === 'password') {
          Object.assign(this.passwordVisibility, {
            [io.id]: false,
          });
        }
      });
    }
  }

  public isFile(data: string) {
    return data === 'file';
  }

  public isDirectory(data: string) {
    return data === 'directory';
  }

  public isPassword(data: string) {
    return data === 'password';
  }

  public isText(data: string) {
    return data === 'longstring';
  }

  public downloadFile(url?: string | number) {
    if (url) {
      const urlStr: string = url.toString();
      const filename = urlStr.substring(urlStr.lastIndexOf('/') + 1);

      this.http.get('' + url, { responseType: 'blob' }).subscribe((blob) => {
        saveAs(blob, filename);
      });
    }
  }
}

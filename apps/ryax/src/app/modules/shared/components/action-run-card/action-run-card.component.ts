// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  Output,
  SimpleChanges,
} from '@angular/core';
import { DateTime, Duration, Interval } from 'luxon';
import { ActionRun, AttemptsData, isRunning } from '../../entity';
import { RunsFacade } from '../../../runs/state/runs.facade';
import { BehaviorSubject, Observable, of } from 'rxjs';
import {
  distinctUntilChanged,
  map,
  take,
  withLatestFrom,
} from 'rxjs/operators';

@Component({
  selector: 'ryax-action-run-card',
  templateUrl: './action-run-card.component.pug',
  styleUrls: ['./action-run-card.component.scss'],
})
export class ActionRunCardComponent implements OnChanges {
  @Input({ required: true }) public showDetails!: boolean;
  @Output() public showDetailsChange: EventEmitter<boolean> =
    new EventEmitter<boolean>();
  @Input({ required: true }) public index!: number;
  @Input({ required: true }) public actionRunInput!: ActionRun;
  public logs$ = this.facade.logs$;
  public currentAttempt$: Observable<AttemptsData | null>;
  public currentAttemptIndex$: Observable<number> | null;
  public showLogs$: Observable<boolean>;
  private actionRunSubject = new BehaviorSubject<ActionRun | null>(null);
  public currentActionRun$!: Observable<ActionRun | null>; // Observable for UI updates

  public dateFormat = "HH'h' mm'm' ss.S's' dd/MM/y";

  constructor(private readonly facade: RunsFacade) {
    // Initialize `currentActionRun$` to listen for distinct updates
    this.currentActionRun$ = this.actionRunSubject
      .asObservable()
      .pipe(
        distinctUntilChanged(
          (prev, curr) => JSON.stringify(prev) == JSON.stringify(curr)
        )
      );

    // Initialize empty dummy observables to prevent errors until `actionRunInput` is properly set
    this.currentAttemptIndex$ = of(-1);
    this.currentAttempt$ = of(null);

    this.showLogs$ = this.logs$.pipe(
      map((logs) => {
        // Check if logs and logs[this.index] are defined
        const logGroup = logs?.[this.index];
        if (!logGroup) {
          return false; // Fallback value if data is missing
        }
        return logGroup.show ?? false;
      })
    );
  }

  async ngOnChanges(changes: SimpleChanges) {
    if (changes['actionRunInput'] && changes['actionRunInput'].firstChange) {
      // Initialize relevant observables based on the new `actionRunInput`
      this.currentAttemptIndex$ = this.facade.currentAttempts$.pipe(
        map((currentAttempts) => currentAttempts[this.index] ?? -1)
      );
      this.currentAttempt$ = this.currentAttemptIndex$.pipe(
        map((index) => this.actionRunInput.attempts[index] ?? null),
        distinctUntilChanged()
      );
      this.currentAttempt$.subscribe(() => {
        this.refreshLogs();
      });
    }
    if (changes['actionRunInput'] && changes['actionRunInput'].currentValue) {
      // Push new value to the subject
      this.actionRunSubject.next(changes['actionRunInput'].currentValue);
    }
    // Automatically open the details if an action in an interesting status
    if (
      changes['actionRunInput'] &&
      (changes['actionRunInput'].currentValue.status === 'Running' ||
        changes['actionRunInput'].currentValue.status === 'Error')
    ) {
      this.showDetails = true;
      this.showDetailsChange.emit(this.showDetails);
    }
  }

  public isLoadingLogs(): Observable<boolean> {
    return this.logs$.pipe(
      // Use startWith(0) to handle potential null or empty emission from currentAttemptIndex$
      withLatestFrom(
        this.currentAttemptIndex$?.pipe(map((index) => index || 0)) ?? of(0)
      ),
      map(([logEntries, attemptIndex]) => {
        // Extracting the log for the specific action and attempt
        const currentLog = logEntries?.[this.index]?.attempts[attemptIndex];

        // Return `loading` status if `currentLog` exists, otherwise false
        return currentLog?.loading ?? false;
      })
    );
  }

  public logsValue(): Observable<string> {
    return this.logs$.pipe(
      // Handle potential null for currentAttemptIndex$ using fallback or default emission
      withLatestFrom(
        this.currentAttemptIndex$?.pipe(map((index) => index || 0)) ?? of(0)
      ),
      map(([logs, currentIndex]) => {
        // Extract the log value for the specific action and attempt
        const currentLog = logs?.[this.index]?.attempts[currentIndex];

        // Return the log value if it exists, otherwise return an empty string
        if (currentLog && currentLog.error) {
          return 'ERROR LOADING LOGS: ' + currentLog.error.message;
        }
        return currentLog?.value ?? '';
      })
    );
  }

  public getWaitingTime(currentAttempt: AttemptsData): Duration {
    if (!currentAttempt || !currentAttempt.submittedDate) {
      return Duration.fromMillis(0);
    }

    if (!currentAttempt || !currentAttempt.startedDate) {
      return Interval.fromDateTimes(
        currentAttempt.submittedDate,
        DateTime.now()
      ).toDuration();
    } else {
      return Interval.fromDateTimes(
        currentAttempt.submittedDate,
        currentAttempt.startedDate
      ).toDuration();
    }
  }
  public getExecutionTime(currentAttempt: AttemptsData): Duration {
    if (!currentAttempt || !currentAttempt.startedDate) {
      return Duration.fromMillis(0);
    }

    if (!currentAttempt.endedDate) {
      return Interval.fromDateTimes(
        currentAttempt.startedDate,
        DateTime.now()
      ).toDuration();
    } else {
      return Interval.fromDateTimes(
        currentAttempt.startedDate,
        currentAttempt.endedDate
      ).toDuration();
    }
  }
  public getTotalTime(currentAttempt: AttemptsData): Duration {
    const waiting_time = this.getWaitingTime(currentAttempt);
    const running_time = this.getExecutionTime(currentAttempt);
    if (waiting_time && waiting_time.isValid) {
      if (running_time && running_time.isValid) {
        return waiting_time.plus(running_time);
      } else {
        return waiting_time;
      }
    }
    if (running_time && running_time.isValid) {
      return running_time;
    }
    return Duration.fromMillis(0);
  }

  public toggleDetails() {
    this.showDetails = !this.showDetails;
    this.showDetailsChange.emit(this.showDetails);
  }

  public getLogsTrigger() {
    this.showLogs$.pipe(take(1)).subscribe((show) => {
      if (show) {
        this.facade.hideLogs(this.index);
      } else {
        this.facade.showLogs(this.index);
      }
      this.refreshLogs();
    });
  }

  /**
   * Refreshes the logs based on the current state and conditions. Retrieves logs if they are not currently loading or available, and the action run status indicates they should be fetched.
   *
   * @return {void} This method does not return a value.
   */
  public refreshLogs() {
    this.logs$
      .pipe(
        take(1),
        withLatestFrom(
          this.currentAttemptIndex$?.pipe(
            map((index) => (index !== undefined && index !== null ? index : -1))
          ) ?? of(-1)
        )
      )
      .subscribe(([logs, attemptIndex]) => {
        const logsContent =
          logs[this.index]?.attempts[attemptIndex]?.value ?? null;
        const logsIsLoading =
          logs[this.index]?.attempts[attemptIndex]?.loading ?? false;
        const logsAreShown = logs[this.index]?.show ?? false;
        if (
          !logsIsLoading &&
          logsAreShown &&
          (!logsContent || isRunning(this.actionRunInput.status))
        ) {
          this.facade.getLogs(this.index, attemptIndex);
        }
      });
  }

  public selectAttempt(attemptIndex: number): void {
    this.facade.setCurrentAttempt(this.index, attemptIndex - 1);
  }
}

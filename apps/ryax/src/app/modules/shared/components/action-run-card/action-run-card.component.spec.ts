// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ActionRunCardComponent } from './action-run-card.component';

describe('ModuleExecutionCardComponent', () => {
  let component: ActionRunCardComponent;
  let fixture: ComponentFixture<ActionRunCardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ActionRunCardComponent],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ActionRunCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should hide or show details when asked and send them to parent component', () => {
    component.showDetailsChange.emit = jest.fn();

    component.toggleDetails();

    expect(component.showDetails).toBeTruthy();
    expect(component.showDetailsChange.emit).toHaveBeenCalledWith(true);

    component.showDetailsChange.emit = jest.fn();
    component.toggleDetails();

    expect(component.showDetails).toBeFalsy();
    expect(component.showDetailsChange.emit).toHaveBeenCalledWith(false);
  });
});

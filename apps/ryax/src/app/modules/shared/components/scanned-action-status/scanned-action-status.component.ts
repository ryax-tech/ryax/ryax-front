import { Component, Input } from '@angular/core';
import { ScannedActionStatus } from '../../../library/entities/scanned-action-status.enum';

@Component({
  selector: 'ryax-scanned-action-status',
  templateUrl: './scanned-action-status.component.pug',
  styleUrls: ['./scanned-action-status.component.scss'],
})
export class ScannedActionStatusComponent {
  @Input() public status: ScannedActionStatus = ScannedActionStatus.SCANNING;

  public getColor(status: ScannedActionStatus) {
    switch (status) {
      case ScannedActionStatus.BUILT:
      case ScannedActionStatus.SCANNED:
        return 'green';
      case ScannedActionStatus.SCANNING:
        return 'blue';
      case ScannedActionStatus.SCAN_ERROR:
      case ScannedActionStatus.BUILD_ERROR:
        return 'red';
      default:
        return 'default';
    }
  }
}

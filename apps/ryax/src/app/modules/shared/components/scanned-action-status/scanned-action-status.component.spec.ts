import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ScannedActionStatusComponent } from './scanned-action-status.component';

describe('ScannedActionStatusComponent', () => {
  let component: ScannedActionStatusComponent;
  let fixture: ComponentFixture<ScannedActionStatusComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ScannedActionStatusComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ScannedActionStatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

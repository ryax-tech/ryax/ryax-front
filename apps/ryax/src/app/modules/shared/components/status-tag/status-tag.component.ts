// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component, Input } from '@angular/core';
import { WorkflowRunStatus } from '../../entity';

@Component({
  selector: 'ryax-status-tag',
  templateUrl: './status-tag.component.pug',
  styleUrls: ['./status-tag.component.scss'],
})
export class StatusTagComponent {
  @Input() public status: WorkflowRunStatus = WorkflowRunStatus.Created;
}

// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import {
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnChanges,
  Output,
  ViewChild,
} from '@angular/core';
import { NzNotificationService } from 'ng-zorro-antd/notification';

@Component({
  selector: 'ryax-console-display',
  templateUrl: './console-display.component.pug',
  styleUrls: ['./console-display.component.scss'],
})
export class ConsoleDisplayComponent implements OnChanges {
  @ViewChild('scrollPane', { static: true }) scrollFrame!: ElementRef;
  @Input() textContent: string | null = null;
  @Input() loading: boolean | null = false;
  @Input() refreshButton = false;
  @Output() refreshEmitter: EventEmitter<boolean> = new EventEmitter<boolean>();

  private isNearBottom = true;

  constructor(private notification: NzNotificationService) {}

  ngOnChanges(): void {
    if (this.isNearBottom) {
      // Wait for the DOM to stabilize before scroll, so it reaches the actual bottom
      setTimeout(() => this.scrollToBottom(), 0);
    }
  }

  public refresh() {
    this.refreshEmitter.emit(true);
  }

  scrolled(): void {
    this.isNearBottom = this.isUserNearBottom();
  }

  textContentSplit() {
    if (this.textContent) {
      return this.textContent.split('\n');
    }
    return null;
  }

  scrollToBottom(): void {
    const scrollContainer = this.scrollFrame.nativeElement;
    scrollContainer.scroll({
      top: scrollContainer.scrollHeight,
      left: 0,
      behavior: 'smooth',
    });
  }

  scrollToTop() {
    const scrollContainer = this.scrollFrame.nativeElement;
    scrollContainer.scroll({
      top: 0,
      left: 0,
      behavior: 'smooth',
    });
  }

  public copied(success: boolean) {
    if (success) {
      this.notification.create('success', 'Copied to clipboard!', '');
    } else {
      this.notification.create(
        'error',
        'Uh oh!',
        'Copy did not work as expected.'
      );
    }
  }

  trackByIndex(index: number): number {
    return index;
  }

  private isUserNearBottom(): boolean {
    const scrollContainer = this.scrollFrame.nativeElement;
    const threshold = 150;
    const position = scrollContainer.scrollTop + scrollContainer.offsetHeight;
    const height = scrollContainer.scrollHeight;
    return position > height - threshold;
  }
}

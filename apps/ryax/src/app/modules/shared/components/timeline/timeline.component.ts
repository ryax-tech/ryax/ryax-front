// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component, Input } from '@angular/core';
import { TimelineColors } from '../../entity';

@Component({
  selector: 'ryax-timeline',
  templateUrl: './timeline.component.pug',
  styleUrls: ['./timeline.component.scss'],
})
export class TimelineComponent {
  @Input() public startColor: TimelineColors = TimelineColors.Purple;
  @Input() public endColor: TimelineColors = TimelineColors.Purple;
}

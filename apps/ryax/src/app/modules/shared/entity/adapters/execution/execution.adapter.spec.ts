// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { WorkflowRunDto } from '../../dtos';
import { ActionRunStatus, WorkflowRunStatus } from '../../runs';
import { WorkflowRunAdapter } from './workflowRunAdapter';

describe('ExecutionAdapter', () => {
  const executionAdapter: WorkflowRunAdapter = new WorkflowRunAdapter();

  it('should adapt WorkflowExecutionDto into WorkflowExecution', () => {
    const workflowExecutionDto: WorkflowRunDto = {
      id: 'exec-id',
      workflow_definition_id: '',
      started_at: '2022-01-14T18:01:06.801174',
      last_result_at: '2022-01-14T18:01:06.801174',
      submitted_at: '2022-01-14T18:01:06.801174',
      total_steps: 34,
      completed_steps: 13,
      state: 'Created',
      runs: [
        {
          id: 'child-exec-id',
          error_message: '',
          action: {
            id: '',
            kind: 'SOURCE',
            description: 'child-description',
            technical_name: 'child-name',
            name: 'test',
            version: '1.0.0',
            module_id: 'test',
            inputs: [
              {
                id: 'input-id',
                value: 'Test value',
                display_name: 'input-name',
                help: '',
                enum_values: [],
                type: 'input-type',
                optional: false,
              },
            ],
            outputs: [
              {
                id: 'input-id',
                value: 'Test value',
                display_name: 'output-name',
                help: '',
                enum_values: [],
                type: 'output-type',
                optional: false,
              },
            ],
          },
          state: 'Waiting',
          results: [],
        },
        {
          id: 'child-exec-id2',
          state: 'Waiting',
          error_message: '',
          action: {
            id: '',
            module_id: '123',
            kind: 'SOURCE',
            description: 'child-description2',
            name: 'Child Name 2',
            technical_name: 'child-name2',
            version: '2.0.0',
            inputs: [],
            outputs: [],
          },
          results: [],
        },
      ],
    };
    const result = executionAdapter.adapt(workflowExecutionDto);
    expect(result).toEqual({
      id: 'exec-id',
      status: WorkflowRunStatus.Created,
      startedAt: new Date('2022-01-14T18:01:06.801174'),
      duration: 0,
      step: 13,
      totalSteps: 34,
      moduleExecutions: [
        {
          id: 'child-exec-id',
          name: 'child-name',
          version: '1.0.0',
          description: 'child-description',
          status: ActionRunStatus.Waiting,
          inputs: [
            {
              id: 'input-id',
              value: 'Test value',
              name: 'input-name',
              type: 'input-type',
              enumValues: [],
              help: '',
            },
          ],
          outputs: [
            {
              id: 'input-id',
              value: 'Test value',
              name: 'output-name',
              type: 'output-type',
              enumValues: [],
              help: '',
            },
          ],
        },
        {
          id: 'child-exec-id2',
          name: 'Child Name 2',
          version: '2.0.0',
          description: 'child-description2',
          status: ActionRunStatus.Waiting,
          inputs: [],
          outputs: [],
        },
      ],
    });
  });
});

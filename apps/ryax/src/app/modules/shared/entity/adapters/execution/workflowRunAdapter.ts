// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { InputDto } from '../../../../workflow/entities/dtos/action.dto';
import { ActionRunDto, WorkflowRunDto } from '../../dtos';

import {
  RunIOData,
  ActionRun,
  ActionRunStatus,
  WorkflowRun,
  WorkflowRunStatus,
} from '../../runs';
import { DateTime, Interval } from 'luxon';

export class WorkflowRunAdapter {
  public adapt(dto: WorkflowRunDto): WorkflowRun {
    return {
      id: dto.id,
      workflowId: dto.workflow_definition_id,
      status: dto.state as WorkflowRunStatus,
      startedAt: dto.started_at
        ? DateTime.fromISO(dto.started_at)
        : DateTime.invalid('Not started yet'),
      duration: this.getDuration(dto),
      step: dto.completed_steps,
      totalSteps: dto.total_steps,
      actionRuns: dto.runs ? this.adaptActionRun(dto.runs) : [],
    };
  }

  public getDuration(dto: WorkflowRunDto): string {
    if (!dto.started_at || !dto.last_result_at) {
      return `Can't calculate`;
    } else {
      return Interval.fromDateTimes(
        DateTime.fromISO(dto.started_at),
        DateTime.fromISO(dto.last_result_at)
      )
        .toDuration()
        .toFormat("mm'm' ss.SSS's'");
    }
  }

  public adaptActionRun(dtos: ActionRunDto[]): ActionRun[] {
    return dtos.map((dto, index) => ({
      id: index.toString(),
      definitionId: dto.action.id,
      description: dto.action.description,
      name: dto.action.custom_name ? dto.action.custom_name : dto.action.name,
      version: dto.action.version,
      status: dto.state as ActionRunStatus,
      inputs: this.adaptActionRunIOData(dto.action.inputs),
      outputs: this.adaptActionRunIOData(dto.action.outputs),
      attempts: dto.results.map((result) => ({
        id: result.id,
        submittedDate: result.submitted_at_date
          ? DateTime.fromISO(result.submitted_at_date)
          : null,
        startedDate: result.started_at_date
          ? DateTime.fromISO(result.started_at_date)
          : null,
        endedDate: result.ended_at_date
          ? DateTime.fromISO(result.ended_at_date)
          : null,
        errorMessage: result.error_message,
      })),
      errorMessage: dto.error_message,
    }));
  }

  public adaptActionRunIOData(dtos: InputDto[]): RunIOData[] {
    return dtos.map((dto) => {
      return {
        id: dto.id,
        name: dto.display_name,
        value: dto.value,
        enumValues: dto.enum_values,
        help: dto.help,
        type: dto.type,
      };
    });
  }
}

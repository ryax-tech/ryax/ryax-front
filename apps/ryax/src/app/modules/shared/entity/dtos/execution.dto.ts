// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { ActionDto } from '../../../workflow/entities/dtos/action.dto';

export interface WorkflowRunDto {
  id: string;
  workflow_definition_id: string;
  submitted_at: string;
  started_at: string;
  last_result_at: string;
  state: WorkflowRunStateDto;
  completed_steps: number;
  total_steps: number;
  runs?: ActionRunDto[];
}

export interface ActionRunDto {
  id: string;
  state: ActionRunStateDto;
  action: ActionDto;
  error_message: string;
  results: ExecutionResultsDto[];
}

export interface ExecutionResultsDto {
  id: string;
  submitted_at_date?: string;
  started_at_date?: string;
  ended_at_date?: string;
  error_message: string;
  state: string;
}

export type WorkflowRunStateDto =
  | 'None'
  | 'Created'
  | 'Pending'
  | 'Deploying'
  | 'Deployed'
  | 'Running'
  | 'Completed'
  | 'Canceled'
  | 'Error';
export type ActionRunStateDto =
  | 'Success'
  | 'Error'
  | 'Running'
  | 'Waiting'
  | 'Pending'
  | 'Canceled';

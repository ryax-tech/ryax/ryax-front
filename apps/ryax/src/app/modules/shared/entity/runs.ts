// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { DateTime } from 'luxon';

export enum WorkflowRunStatus {
  None = 'None',
  Created = 'Created',
  Pending = 'Pending',
  Deploying = 'Deploying',
  Deployed = 'Deployed',
  Running = 'Running',
  Completed = 'Completed',
  Canceled = 'Canceled',
  Error = 'Error',
}

export enum ActionRunStatus {
  Success = 'Success',
  Error = 'Error',
  Canceled = 'Canceled',
  Pending = 'Pending',
  Running = 'Running',
  Waiting = 'Waiting',
}

export function isRunning(status: ActionRunStatus): boolean {
  return [ActionRunStatus.Running].includes(status);
}

export interface WorkflowRun {
  id: string;
  workflowId: string;
  status: WorkflowRunStatus;
  startedAt: DateTime;
  actionRuns: ActionRun[];
  step: number;
  totalSteps: number;
  duration: string;
}

export interface ActionRun {
  id: string;
  definitionId: string;
  name: string;
  version: string;
  description: string;
  status: ActionRunStatus;
  inputs: RunIOData[];
  outputs: RunIOData[];
  attempts: AttemptsData[];
  errorMessage: string;
}

export interface AttemptsData {
  id: string;
  submittedDate: DateTime | null;
  startedDate: DateTime | null;
  endedDate: DateTime | null;
  errorMessage: string;
}

export interface RunIOData {
  id: string;
  name: string;
  value?: string | number;
  enumValues: string[];
  help: string;
  type: string;
}

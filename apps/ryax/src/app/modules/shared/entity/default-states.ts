import { HttpErrorResponse } from '@angular/common/http';

export interface DefaultValueState<T> {
  value: T;
  loading: boolean;
  error: HttpErrorResponse | null;
}

export interface DefaultValueStateWithPagination<T> {
  value: T;
  total: number;
  currentPage: number;
  loading: boolean;
  error: HttpErrorResponse | null;
}

export interface LoadingState {
  loading: boolean;
  error: HttpErrorResponse | null;
}

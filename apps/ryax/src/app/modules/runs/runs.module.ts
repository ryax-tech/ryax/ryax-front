// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AuthModule } from '../auth/auth.module';
import { SharedModule } from '../shared/shared.module';
import { RunDetailsComponent } from './components/run-details/run-details.component';
import { PortalFormContentComponent } from './components/portal-form-content/portal-form-content.component';
import { PortalFormComponent } from './components/portal-form/portal-form.component';
import { RunsComponent } from './components/runs/runs.component';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { RunsFacade } from './state/runs.facade';
import {
  RunsFeatureKey,
  RunsReducerProvider,
  RunsReducerToken,
} from './state/effects';
import { RunsEffects } from './state/effects/runs.effects';
import { RunsApi } from './services/run-api/runs-api.service';

@NgModule({
  declarations: [
    PortalFormComponent,
    PortalFormContentComponent,
    RunsComponent,
    RunDetailsComponent,
  ],
  imports: [
    StoreModule.forFeature(RunsFeatureKey, RunsReducerToken),
    EffectsModule.forFeature([RunsEffects]),
    CommonModule,
    RouterModule,
    SharedModule,
    AuthModule,
  ],
  providers: [RunsReducerProvider, RunsApi, RunsFacade],
  exports: [RunsComponent],
})
export class RunsModule {}

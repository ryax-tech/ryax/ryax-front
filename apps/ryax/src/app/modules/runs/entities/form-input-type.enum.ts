// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
export enum FormInputType {
  STRING = 'STRING',
  LONGSTRING = 'LONGSTRING',
  INTEGER = 'INTEGER',
  FLOAT = 'FLOAT',
  PASSWORD = 'PASSWORD',
  ENUM = 'ENUM',
  FILE = 'FILE',
  DIRECTORY = 'DIRECTORY',
  BOOLEAN = 'BOOLEAN',
}

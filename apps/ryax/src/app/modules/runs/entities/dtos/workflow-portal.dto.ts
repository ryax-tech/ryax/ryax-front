// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { WorkflowModuleIODto } from './workflow-module-io.dto';

export interface WorkflowPortalDto {
  workflow_definition_id: string;
  workflow_deployment_id: string;
  name: string;
  outputs?: WorkflowModuleIODto[];
}

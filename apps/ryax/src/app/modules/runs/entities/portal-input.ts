// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { FormInputType } from './form-input-type.enum';

export interface PortalInput {
  id: string;
  technicalName: string;
  displayName: string;
  type: FormInputType;
  help: string;
  optional: boolean;
  enumValues?: string[];
}

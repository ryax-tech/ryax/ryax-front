// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { WorkflowModuleIODto, WorkflowPortalDto } from '../../dtos';
import { FormInputType, Portal, PortalInput } from '../../index';

export class WorkflowPortalAdapter {
  public adapt(dto: WorkflowPortalDto, workflowId: string): Portal {
    return {
      id: dto.workflow_definition_id,
      name: dto.name,
      // customName: dto.custom_name,
      // description: dto.description,
      // accessPath: dto.access_path,
      outputs: dto.outputs
        ? dto.outputs.map((output) => this.adaptIos(output))
        : undefined,
      workflowId,
    };
  }

  public adaptIos(dto: WorkflowModuleIODto): PortalInput {
    return {
      id: dto.id,
      technicalName: dto.technical_name,
      displayName: dto.display_name,
      type: dto.type as FormInputType,
      help: dto.help,
      optional: dto.optional,
      enumValues: dto.enum_values,
    };
  }
}

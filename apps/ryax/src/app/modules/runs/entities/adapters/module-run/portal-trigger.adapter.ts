// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { PortalTriggerDto } from '../../dtos';
import { WorkflowRunIdentifier } from '../../index';

export class PortalTriggerAdapter {
  public adapt(dto: PortalTriggerDto): WorkflowRunIdentifier {
    return {
      id: dto.workflow_run_id,
    };
  }
}

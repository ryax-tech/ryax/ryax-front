//reducers
import { Portal } from '../entities';
import { WorkflowRun } from '../../shared/entity';
import { createReducer, on } from '@ngrx/store';
import {
  hideLogs,
  loadLogs,
  loadLogsError,
  loadLogsSuccess,
  loadPortal,
  loadPortalError,
  loadPortalSuccess,
  loadResults,
  loadResultsError,
  loadResultsSuccess,
  loadWorkflowRun,
  loadWorkflowRunError,
  loadWorkflowRunSuccess,
  sendFormData,
  sendFormDataError,
  sendFormDataSuccess,
  setCurrentAttempt,
  showLogs,
  stopRefreshingWorkflowRun,
} from './actions/runs.actions';
import { DefaultValueState } from '../../shared/entity/default-states';

export interface ActionRunLogs {
  [actionIndex: number]: {
    show: boolean;
    attempts: { [attemptIndex: number]: DefaultValueState<string | null> };
  };
}

// Represent the state of the current workflow run its portal (if any).
export interface RunsState {
  portal: DefaultValueState<Portal | null>;
  portalWorkflowRunId: DefaultValueState<string | null>;
  workflowRun: DefaultValueState<WorkflowRun | null>;
  refreshingWorkflowRun: boolean;
  logs: ActionRunLogs;
  results: DefaultValueState<string>;
  // in the current workflow run for each action run index get the current attempt selected index
  currentAttempts: { [actionRunIndex: number]: number | null };
}

const initialRunsState = {
  portal: { value: null, loading: false, error: null },
  portalWorkflowRunId: { value: null, loading: false, error: null },
  workflowRun: { value: null, loading: false, error: null },
  refreshingWorkflowRun: true,
  logs: {},
  results: { value: '', loading: false, error: null },
  currentAttempts: {},
};

export const runsReducer = createReducer<RunsState>(
  initialRunsState,
  on(loadPortal, (state) => ({
    ...state,
    portal: {
      value: null,
      loading: true,
      error: null,
    },
  })),
  on(loadPortalSuccess, (state, { portal }) => ({
    ...state,
    portal: {
      value: portal,
      loading: false,
      error: null,
    },
  })),
  on(loadPortalError, (state, { error }) => ({
    ...state,
    portal: {
      value: null,
      error: error,
      loading: false,
    },
  })),
  on(sendFormData, (state) => ({
    ...state,
    portalWorkflowRunId: {
      value: null,
      loading: true,
      error: null,
    },
  })),
  on(sendFormDataSuccess, (state, { workflowRunId }) => ({
    ...state,
    portalWorkflowRunId: {
      value: workflowRunId,
      loading: false,
      error: null,
    },
  })),
  on(sendFormDataError, (state, { error }) => ({
    ...state,
    portalWorkflowRunId: {
      value: null,
      loading: false,
      error: error,
    },
  })),
  on(loadWorkflowRun, (state) => ({
    ...state,
    workflowRun: {
      value: null,
      loading: true,
      error: null,
    },
    refreshingWorkflowRun: true,
    logs: {},
    currentAttempts: {},
  })),
  on(stopRefreshingWorkflowRun, (state) => ({
    ...state,
    refreshingWorkflowRun: false,
  })),
  on(loadWorkflowRunSuccess, (state, { workflowRun }) => {
    const updatedAttempts = workflowRun.actionRuns.reduce(
      (newCurrentAttempt, actionRun, index) => {
        if (
          state.currentAttempts[index] === null ||
          state.currentAttempts[index] === undefined ||
          state.currentAttempts[index] == -1
        ) {
          return {
            ...newCurrentAttempt,
            [index]: actionRun.attempts.length - 1 ?? null,
          };
        } else {
          return {
            ...newCurrentAttempt,
            [index]: state.currentAttempts[index],
          };
        }
      },
      {}
    );
    return {
      ...state,
      workflowRun: {
        value: workflowRun,
        loading: false,
        error: null,
      },
      currentAttempts: updatedAttempts, // Update the attempts here
    };
  }),
  on(loadWorkflowRunError, (state, { error }) => ({
    ...state,
    workflowRun: {
      value: null,
      loading: false,
      error: error,
    },
  })),

  on(showLogs, (state, { actionIndex }) => ({
    ...state,
    logs: {
      ...state.logs,
      [actionIndex]: {
        show: true,
        attempts: state.logs[actionIndex]?.attempts || {
          value: null,
          loading: true,
          error: null,
        },
      },
    },
  })),
  on(hideLogs, (state, { actionIndex }) => ({
    ...state,
    logs: {
      ...state.logs,
      [actionIndex]: {
        show: false,
        attempts: state.logs[actionIndex]?.attempts || {
          value: null,
          loading: false,
          error: null,
        },
      },
    },
  })),
  on(loadLogs, (state, { actionIndex, attemptIndex }) => ({
    ...state,
    logs: {
      ...state.logs,
      [actionIndex]: {
        ...(state.logs[actionIndex] || {}),
        attempts: {
          ...(state.logs[actionIndex].attempts || {}),
          [attemptIndex]: {
            value:
              state.logs[actionIndex].attempts[attemptIndex]?.value || null,
            loading: true,
            error: null,
          },
        },
      },
    },
  })),
  on(loadLogsSuccess, (state, { actionIndex, attemptIndex, logs }) => ({
    ...state,
    logs: {
      ...state.logs,
      [actionIndex]: {
        ...(state.logs[actionIndex] || {}),
        attempts: {
          ...(state.logs[actionIndex].attempts || {}),
          [attemptIndex]: {
            value: logs,
            loading: false,
            error: null,
          },
        },
      },
    },
  })),
  on(loadLogsError, (state, { actionIndex, attemptIndex, error }) => ({
    ...state,
    logs: {
      ...state.logs,
      [actionIndex]: {
        ...(state.logs[actionIndex] || {}),
        attempts: {
          ...(state.logs[actionIndex].attempts || {}),
          [attemptIndex]: {
            value:
              state.logs[actionIndex].attempts[attemptIndex]?.value || null,
            loading: false,
            error: error,
          },
        },
      },
    },
  })),

  on(loadResults, (state) => ({
    ...state,
    results: {
      value: '',
      loading: true,
      error: null,
    },
  })),
  on(loadResultsSuccess, (state, { results }) => ({
    ...state,
    results: {
      value: results,
      loading: false,
      error: null,
    },
  })),
  on(loadResultsError, (state, { error }) => ({
    ...state,
    results: {
      value: '',
      loading: false,
      error: error,
    },
  })),
  on(setCurrentAttempt, (state, { actionIndex, attemptIndex }) => ({
    ...state,
    currentAttempts: {
      ...state.currentAttempts,
      [actionIndex]: attemptIndex,
    },
  }))
);

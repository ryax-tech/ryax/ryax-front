import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import {
  selectFormDataUploading,
  selectWorkflowRunLoading,
  selectLogs,
  selectPortal,
  selectPortalLoading,
  selectResults,
  selectResultsLoading,
  selectWorkflowRun,
  selectCurrentAttempts,
} from './run.selectors';
import {
  loadLogs,
  loadPortal,
  loadResults,
  loadWorkflowRun,
  sendFormData,
  stopRefreshingWorkflowRun,
  deleteRun,
  stopRun,
  setCurrentAttempt,
  hideLogs,
  showLogs,
} from './actions/runs.actions';
import { ProjectState } from '../../project/state/reducers';
import { PortalFormData } from '../entities';

@Injectable({
  providedIn: 'root',
})
export class RunsFacade {
  public portal$ = this.store.select(selectPortal);
  public portalLoading$ = this.store.select(selectPortalLoading);
  public formDataUploading$ = this.store.select(selectFormDataUploading);
  public workflowRun$ = this.store.select(selectWorkflowRun);
  public workflowRunLoading$ = this.store.select(selectWorkflowRunLoading);
  public results$ = this.store.select(selectResults);
  public resultsLoading$ = this.store.select(selectResultsLoading);
  public logs$ = this.store.select(selectLogs);
  public currentAttempts$ = this.store.select(selectCurrentAttempts);

  constructor(private readonly store: Store<ProjectState>) {}

  public getPortal(workflowId: string) {
    this.store.dispatch(loadPortal(workflowId));
  }

  public triggerPortalRun(formData: PortalFormData) {
    this.store.dispatch(sendFormData(formData));
  }

  public getRunStatus(workflowRunId: string) {
    this.store.dispatch(loadWorkflowRun(workflowRunId));
  }

  public getLogs(actionIndex: number, attemptIndex: number) {
    this.store.dispatch(loadLogs(actionIndex, attemptIndex));
  }
  public showLogs(actionIndex: number) {
    this.store.dispatch(showLogs(actionIndex));
  }
  public hideLogs(actionIndex: number) {
    this.store.dispatch(hideLogs(actionIndex));
  }

  public getResults(workflowRunId: string) {
    this.store.dispatch(loadResults(workflowRunId));
  }

  public stopRefresh(workflowRunId: string) {
    this.store.dispatch(stopRefreshingWorkflowRun(workflowRunId));
  }

  public stopRun(workflowRunId: string) {
    this.store.dispatch(stopRun(workflowRunId));
  }
  public deleteRun(workflowRunId: string) {
    this.store.dispatch(deleteRun(workflowRunId));
  }

  public setCurrentAttempt(actionIndex: number, attemptIndex: number) {
    this.store.dispatch(setCurrentAttempt(actionIndex, attemptIndex));
  }
}

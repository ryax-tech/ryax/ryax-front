// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { RunsState } from './run.reducer';
import { createFeatureSelector, createSelector } from '@ngrx/store';
import { GlobalRunsState, RunsFeatureKey } from './effects';

const selectRunStateFn = (state: GlobalRunsState) => state.runs;
const selectPortalLoadingFn = (state: RunsState) => state.portal.loading;
const selectPortalFn = (state: RunsState) => state.portal.value;
const selectFormDataUploadingFn = (state: RunsState) =>
  state.portalWorkflowRunId.loading;
const selectWorkflowRunIdFn = (state: RunsState) =>
  state.portalWorkflowRunId.value;

const selectRefreshingFn = (state: RunsState) => state.refreshingWorkflowRun;
const selectWorkflowRunLoadingFn = (state: RunsState) =>
  state.workflowRun.loading;
const selectWorkflowRunFn = (state: RunsState) => state.workflowRun.value;
const selectResultsLoadingFn = (state: RunsState) => state.results.loading;
const selectResultsFn = (state: RunsState) => state.results.value;
const selectCurrentAttemptsFn = (state: RunsState) => state.currentAttempts;
const selectLogsFn = (state: RunsState) => state.logs;

export const selectGlobalRunsState =
  createFeatureSelector<GlobalRunsState>(RunsFeatureKey);
export const selectRunsState = createSelector(
  selectGlobalRunsState,
  selectRunStateFn
);
export const selectPortal = createSelector(selectRunsState, selectPortalFn);
export const selectPortalLoading = createSelector(
  selectRunsState,
  selectPortalLoadingFn
);
export const selectFormDataUploading = createSelector(
  selectRunsState,
  selectFormDataUploadingFn
);
export const selectWorkflowRunLoading = createSelector(
  selectRunsState,
  selectWorkflowRunLoadingFn
);
export const selectCurrentAttempts = createSelector(
  selectRunsState,
  selectCurrentAttemptsFn
);
export const selectWorkflowRunId = createSelector(
  selectRunsState,
  selectWorkflowRunIdFn
);
export const selectWorkflowRun = createSelector(
  selectRunsState,
  selectWorkflowRunFn
);
export const selectLogs = createSelector(selectRunsState, selectLogsFn);
export const selectResults = createSelector(selectRunsState, selectResultsFn);
export const selectResultsLoading = createSelector(
  selectRunsState,
  selectResultsLoadingFn
);
export const selectRefreshing = createSelector(
  selectRunsState,
  selectRefreshingFn
);

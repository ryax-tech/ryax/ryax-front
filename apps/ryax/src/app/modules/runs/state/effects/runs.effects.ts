//effects
import { of } from 'rxjs';
import {
  catchError,
  delay,
  map,
  switchMap,
  tap,
  withLatestFrom,
} from 'rxjs/operators';
import { Portal, WorkflowRunIdentifier } from '../../entities';
import { HttpErrorResponse } from '@angular/common/http';
import { RunsApi } from '../../services/run-api/runs-api.service';
import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { RunsActions } from '../actions';
import { Store } from '@ngrx/store';
import { RunsState } from '../run.reducer';
import {
  selectPortal,
  selectRefreshing,
  selectWorkflowRun,
} from '../run.selectors';
import { WorkflowRun, WorkflowRunStatus } from '../../../shared/entity';
import { NzNotificationService } from 'ng-zorro-antd/notification';

@Injectable()
export class RunsEffects {
  loadPortalInputs$ = createEffect(() =>
    this.actions$.pipe(
      ofType(RunsActions.loadPortal),
      switchMap(({ workflowId }) =>
        this.runsApi.loadPortal(workflowId).pipe(
          map((portal) => RunsActions.loadPortalSuccess(portal)),
          catchError((err: HttpErrorResponse) => {
            return of(RunsActions.loadPortalError(err));
          })
        )
      )
    )
  );
  sendFormData$ = createEffect(() =>
    this.actions$.pipe(
      ofType(RunsActions.sendFormData),
      withLatestFrom(this.store$.select(selectPortal), (action, portal) => {
        return {
          formData: action.portalFormData,
          portal: portal as Portal,
        };
      }),
      switchMap(({ formData, portal }) =>
        this.runsApi.sendFormData(portal.workflowId, formData).pipe(
          map((workflowId: WorkflowRunIdentifier) =>
            RunsActions.sendFormDataSuccess(workflowId.id)
          ),
          catchError((err: HttpErrorResponse) => {
            return of(RunsActions.sendFormDataError(err));
          })
        )
      )
    )
  );

  navigateToRunPage$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(RunsActions.sendFormDataSuccess),
        tap(({ workflowRunId }) => {
          this.router.navigate(['runs', workflowRunId]).then((r) => {
            if (!r) {
              console.error(r);
            }
          });
        })
      ),
    { dispatch: false }
  ); // Required to avoid infinite loop!

  loadWorkflowRun$ = createEffect(() =>
    this.actions$.pipe(
      ofType(RunsActions.loadWorkflowRun),
      switchMap(({ workflowRunId }) =>
        this.runsApi.getWorkflowRunStatus(workflowRunId).pipe(
          switchMap((workflowRun: WorkflowRun) => {
            if (
              workflowRun.status != WorkflowRunStatus.Completed &&
              workflowRun.status != WorkflowRunStatus.Canceled &&
              workflowRun.status != WorkflowRunStatus.Error
            ) {
              return [
                RunsActions.loadWorkflowRunSuccess(workflowRun),
                RunsActions.keepRefreshingWorkflowRun(workflowRunId),
              ];
            } else {
              return [RunsActions.loadWorkflowRunSuccess(workflowRun)];
            }
          }),
          catchError((err: HttpErrorResponse) => {
            return of(RunsActions.loadWorkflowRunError(err));
          })
        )
      )
    )
  );

  refreshWorkflowRun$ = createEffect(() =>
    this.actions$.pipe(
      ofType(RunsActions.keepRefreshingWorkflowRun),
      delay(5000),
      withLatestFrom(
        this.store$.select(selectRefreshing),
        (action, refreshing) => ({
          workflowRunId: action.workflowRunId,
          refreshing,
        })
      ),
      switchMap(({ workflowRunId, refreshing }) =>
        this.runsApi.getWorkflowRunStatus(workflowRunId).pipe(
          switchMap((workflowRun: WorkflowRun) => {
            if (
              workflowRun.status != WorkflowRunStatus.Completed &&
              workflowRun.status != WorkflowRunStatus.Canceled &&
              workflowRun.status != WorkflowRunStatus.Error &&
              refreshing
            ) {
              return [
                RunsActions.loadWorkflowRunSuccess(workflowRun),
                RunsActions.keepRefreshingWorkflowRun(workflowRunId),
              ];
            } else {
              return [RunsActions.loadWorkflowRunSuccess(workflowRun)];
            }
          }),
          catchError((err: HttpErrorResponse) => {
            return of(RunsActions.loadWorkflowRunError(err));
          })
        )
      )
    )
  );

  loadLogs$ = createEffect(() =>
    this.actions$.pipe(
      ofType(RunsActions.loadLogs),
      withLatestFrom(
        this.store$.select(selectWorkflowRun),
        (event, workflowRun) => ({
          actionIndex: event.actionIndex,
          attemptIndex: event.attemptIndex,
          workflowRun: workflowRun,
        })
      ),
      switchMap(({ actionIndex, attemptIndex, workflowRun }) => {
        // Validate indices and necessary workflowRun data
        const actionRun = workflowRun?.actionRuns?.[actionIndex];
        const attempt = actionRun?.attempts?.[attemptIndex];

        if (!actionRun || !attempt) {
          // If invalid data, dispatch a loadLogsError action immediately
          return of(
            RunsActions.loadLogsSuccess(actionIndex, attemptIndex, null)
          );
        }

        // Proceed with the API call if data is valid
        return this.runsApi
          .getActionRunLogs(attemptIndex, actionIndex, workflowRun)
          .pipe(
            map(({ actionIndex, logs }) =>
              RunsActions.loadLogsSuccess(actionIndex, attemptIndex, logs)
            ),
            catchError((err: HttpErrorResponse) =>
              of(RunsActions.loadLogsError(actionIndex, attemptIndex, err))
            )
          );
      })
    )
  );

  loadResults$ = createEffect(() =>
    this.actions$.pipe(
      ofType(RunsActions.loadResults),
      switchMap(({ workflowRunId }) =>
        this.runsApi.getResults(workflowRunId).pipe(
          map((results) => {
            return RunsActions.loadResultsSuccess(results);
          }),
          catchError((err: HttpErrorResponse) => {
            return of(RunsActions.loadResultsError(err));
          })
        )
      )
    )
  );

  stopWorkflowRun$ = createEffect(() =>
    this.actions$.pipe(
      ofType(RunsActions.stopRun),
      switchMap(({ workflowRunId }) =>
        this.runsApi.stopWorkflowRun(workflowRunId).pipe(
          map(() => {
            return RunsActions.stopSuccess();
          }),
          catchError((err: HttpErrorResponse) => {
            return of(RunsActions.stopError(err));
          })
        )
      )
    )
  );

  deleteWorkflowRun$ = createEffect(() =>
    this.actions$.pipe(
      ofType(RunsActions.deleteRun),
      switchMap(({ workflowRunId }) =>
        this.runsApi.deleteWorkflowRun(workflowRunId).pipe(
          map(() => {
            return RunsActions.deleteSuccess();
          }),
          catchError((err: HttpErrorResponse) => {
            return of(RunsActions.deleteError(err));
          })
        )
      )
    )
  );

  // TODO manage each error individually and show them on the right component, not just in a notification...
  displayErrors$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(
          RunsActions.loadWorkflowRunError,
          RunsActions.loadResultsError,
          RunsActions.loadLogsError,
          RunsActions.sendFormDataError,
          RunsActions.loadPortalError,
          RunsActions.stopError,
          RunsActions.deleteError
        ),
        tap((errAction) => {
          console.log(errAction);
        }),
        tap((errAction) =>
          this.notification.create(
            'error',
            'Something went wrong ! http error : ' + errAction.error.status,
            errAction.error.message + ': ' + errAction.error.error.error
          )
        )
      ),
    { dispatch: false }
  );

  constructor(
    private actions$: Actions,
    private readonly runsApi: RunsApi,
    private store$: Store<RunsState>,
    private router: Router,
    private notification: NzNotificationService
  ) {}
}

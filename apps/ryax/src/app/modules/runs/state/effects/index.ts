import { InjectionToken } from '@angular/core';
import { ActionReducerMap } from '@ngrx/store';
import { runsReducer, RunsState } from '../run.reducer';

export interface GlobalRunsState {
  runs: RunsState;
}

export const RunsFeatureKey = 'RunsDomain';

export const RunsReducerToken = new InjectionToken<
  ActionReducerMap<GlobalRunsState>
>(RunsFeatureKey);

export const RunsReducerProvider = {
  provide: RunsReducerToken,
  useValue: {
    runs: runsReducer,
  },
};

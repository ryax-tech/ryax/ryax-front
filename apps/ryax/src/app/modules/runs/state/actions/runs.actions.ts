// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { HttpErrorResponse } from '@angular/common/http';
import { createAction } from '@ngrx/store';
import { Portal, PortalFormData } from '../../entities';
import { WorkflowRun } from '../../../shared/entity';

export const loadPortal = createAction(
  '[Runs] Get Portal inputs',
  (workflowId: string) => ({ workflowId })
);
export const loadPortalSuccess = createAction(
  '[Runs] Load Portal inputs success',
  (portal: Portal) => ({ portal })
);
export const loadPortalError = createAction(
  '[Runs] Load Portal error',
  (error: HttpErrorResponse) => ({ error })
);

export const sendFormData = createAction(
  '[Runs] Send Portal form data',
  (portalFormData: PortalFormData) => ({ portalFormData })
);
export const sendFormDataSuccess = createAction(
  '[Runs] Send Portal form data success',
  (workflowRunId: string) => ({ workflowRunId })
);
export const sendFormDataError = createAction(
  '[Run] Send Portal form data error',
  (error: HttpErrorResponse) => ({ error })
);

export const loadWorkflowRun = createAction(
  '[Run] Load workflow run',
  (workflowRunId: string) => ({ workflowRunId })
);
export const keepRefreshingWorkflowRun = createAction(
  '[Run] Keep Refresh workflow run',
  (workflowRunId: string) => ({ workflowRunId })
);
export const stopRefreshingWorkflowRun = createAction(
  '[Run] Stop Refresh workflow run',
  (workflowRunId: string) => ({ workflowRunId })
);
export const loadWorkflowRunSuccess = createAction(
  '[Run] Load workflow run success',
  (workflowRun: WorkflowRun) => ({ workflowRun })
);
export const loadWorkflowRunError = createAction(
  '[Run] Load workflow run error',
  (error: HttpErrorResponse) => ({ error })
);

export const loadLogs = createAction(
  '[Run] Load run logs for this action and attempt',
  (actionIndex: number, attemptIndex: number) => ({ actionIndex, attemptIndex })
);
export const showLogs = createAction(
  '[Run] Show run logs for this action and attempt',
  (actionIndex: number) => ({ actionIndex })
);
export const hideLogs = createAction(
  '[Run] Hide run logs for this action and attempt',
  (actionIndex: number) => ({ actionIndex })
);
export const loadLogsSuccess = createAction(
  '[Run] Load run logs success',
  (actionIndex: number, attemptIndex: number, logs: string | null) => ({
    actionIndex,
    attemptIndex,
    logs,
  })
);
export const loadLogsError = createAction(
  '[Run] Load run logs error',
  (actionIndex: number, attemptIndex: number, error: HttpErrorResponse) => ({
    actionIndex,
    attemptIndex,
    error,
  })
);

export const loadResults = createAction(
  '[Run] Load run Results',
  (workflowRunId: string) => ({ workflowRunId })
);
export const loadResultsSuccess = createAction(
  '[Run] Load run Results success',
  (results: string) => ({ results })
);
export const loadResultsError = createAction(
  '[Run] Load run Results error',
  (error: HttpErrorResponse) => ({ error })
);

export const stopRun = createAction(
  '[Run] Stop run',
  (workflowRunId: string) => ({ workflowRunId })
);
export const stopSuccess = createAction('[Run] Stop run success');
export const stopError = createAction(
  '[Run] Stop run error',
  (error: HttpErrorResponse) => ({ error })
);

export const deleteRun = createAction(
  '[Run] Delete run',
  (workflowRunId: string) => ({ workflowRunId })
);
export const deleteSuccess = createAction('[Run] Delete run success');
export const deleteError = createAction(
  '[Run] Delete run error',
  (error: HttpErrorResponse) => ({ error })
);
export const setCurrentAttempt = createAction(
  '[Run] Set current attempt',
  (actionIndex: number, attemptIndex: number) => ({ actionIndex, attemptIndex })
);

// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { runsRoutes } from './runs.routes';
import { RunsModule } from './runs.module';

@NgModule({
  imports: [CommonModule, RunsModule, RouterModule.forChild(runsRoutes)],
})
export class RunsAccessModule {}

// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { RunDetailsComponent } from './components/run-details/run-details.component';
import { RunsComponent } from './components/runs/runs.component';
import { Routes } from '@angular/router';
import { PortalFormComponent } from './components/portal-form/portal-form.component';

export const runsRoutes: Routes = [
  {
    path: '',
    component: RunsComponent,
    children: [
      {
        path: ':workflowRunId',
        component: RunDetailsComponent,
      },
      {
        path: 'form/:workflowId',
        component: PortalFormComponent,
      },
    ],
  },
];

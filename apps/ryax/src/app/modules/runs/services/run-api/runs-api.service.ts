// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { WorkflowRun } from '../../../shared/entity';
import { WorkflowRunAdapter } from '../../../shared/entity/adapters';
import { WorkflowRunDto } from '../../../shared/entity/dtos';
import { WorkflowRunIdentifier, Portal, PortalFormData } from '../../entities';
import {
  PortalTriggerAdapter,
  WorkflowPortalAdapter,
} from '../../entities/adapters';
import { PortalTriggerDto, WorkflowPortalDto } from '../../entities/dtos';

@Injectable()
export class RunsApi {
  private baseUrl = '/api/runner';

  constructor(private readonly http: HttpClient) {}

  public loadPortal(workflowId: string): Observable<Portal> {
    return this.http
      .get<WorkflowPortalDto>(`${this.baseUrl}/portals/${workflowId}`, {
        responseType: 'json',
      })
      .pipe(map((dto) => new WorkflowPortalAdapter().adapt(dto, workflowId)));
  }

  public sendFormData(
    workflowId: string,
    data: PortalFormData
  ): Observable<WorkflowRunIdentifier> {
    const formData = new FormData();

    Object.entries(data).forEach(([key, value]) => {
      if (value instanceof Array) {
        for (let i = 0; i < value.length; i++) {
          formData.append(key, value[i]);
        }
      } else {
        formData.append(key, value);
      }
    });

    return this.http
      .post<PortalTriggerDto>(
        `${this.baseUrl}/portals/${workflowId}`,
        formData,
        { responseType: 'json' }
      )
      .pipe(map((dto) => new PortalTriggerAdapter().adapt(dto)));
  }

  public getWorkflowRunStatus(workflowRunId: string): Observable<WorkflowRun> {
    return this.http
      .get<WorkflowRunDto>(`${this.baseUrl}/workflow_runs/${workflowRunId}`)
      .pipe(map((dto) => new WorkflowRunAdapter().adapt(dto)));
  }

  public getActionRunLogs(
    attemptIndex: number,
    actionIndex: number,
    workflowRun: WorkflowRun | null
  ): Observable<{ actionIndex: number; logs: string }> {
    if (
      !workflowRun ||
      !workflowRun.actionRuns ||
      !workflowRun.actionRuns[actionIndex]
    ) {
      return of({ actionIndex, logs: '' });
    }
    return this.http
      .get<{ id: string; log: string }>(
        `${this.baseUrl}/workflow_runs/${workflowRun.actionRuns[actionIndex].attempts[attemptIndex].id}/logs`
      )
      .pipe(
        map((dto) => {
          return {
            actionIndex,
            logs: dto.log ? dto.log : 'Nothing was logged yet.',
          };
        })
      );
  }

  // TODO create a endpoint for the trigger logs
  //public getTriggerLogs(workflowId: string, ): Observable<ActionRunLog | null> {
  //  return this.http.get<{ id: string, log: string }>(
  //      `${this.baseUrl}/workflow/${workflowId}/logs`
  //    ).pipe(
  //      map(dto => {
  //        return {logs}
  //      })
  //    )
  //}

  public getResults(workflowRunId: string): Observable<string> {
    return this.http
      .get<{ [key: string]: string }>(
        `${this.baseUrl}/run-results/${workflowRunId}`
      )
      .pipe(map((value) => JSON.stringify(value, undefined, 2)));
  }
  public stopWorkflowRun(workflowRunId: string) {
    return this.http.post<void>(
      `${this.baseUrl}/workflow_runs/${workflowRunId}/cancel`,
      {}
    );
  }

  public deleteWorkflowRun(workflowRunId: string) {
    return this.http.delete<void>(
      `${this.baseUrl}/workflow_runs/${workflowRunId}`
    );
  }
}

// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { take } from 'rxjs/operators';
import { ActionRun, WorkflowRunStatus } from '../../../shared/entity';
import { RunsFacade } from '../../state/runs.facade';

@Component({
  selector: 'ryax-run-details',
  templateUrl: './run-details.component.pug',
  styleUrls: ['./run-details.component.scss'],
})
export class RunDetailsComponent implements OnInit, OnDestroy {
  public workflowRunId = this.route.snapshot.params['workflowRunId'];
  public workflowRun$ = this.facade.workflowRun$;
  public results$ = this.facade.results$;
  public loadingResults$ = this.facade.resultsLoading$;
  public showAllDetails: boolean[] = [];
  public showResults = false;
  public resultUrl = '';
  public loading$ = this.facade.workflowRunLoading$;

  constructor(
    private readonly facade: RunsFacade,
    private route: ActivatedRoute
  ) {}

  public async ngOnInit() {
    this.workflowRun$.pipe(take(1)).subscribe((workflowRun) => {
      if (workflowRun) {
        this.showAllDetails = workflowRun.actionRuns.map(() => false);
      }
    });
    this.facade.getRunStatus(this.workflowRunId);
  }

  public areAllDetailsOpen(): boolean {
    return this.showAllDetails.reduce(
      (answer, current) => answer || current,
      false
    );
  }

  public toggleAllDetails() {
    if (this.areAllDetailsOpen()) {
      this.showAllDetails = this.showAllDetails.map(() => false);
    } else {
      this.showAllDetails = this.showAllDetails.map(() => true);
    }
  }

  public trackByFn(index: number, item: ActionRun) {
    return item.id;
  }

  public async displayResults() {
    this.facade.getResults(this.workflowRunId);
    this.resultUrl = '/api/runner/results/' + this.workflowRunId;
    this.showResults = true;
  }

  public handleCancel(): void {
    this.showResults = false;
  }

  public stopRun(id: string) {
    this.facade.stopRun(id);
  }

  public deleteRun(id: string) {
    this.facade.deleteRun(id);
  }

  ngOnDestroy(): void {
    this.facade.stopRefresh(this.workflowRunId);
  }

  protected readonly WorkflowRunStatus = WorkflowRunStatus;
}

// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';
import { ActionRunStatus, WorkflowRunStatus } from '../../../shared/entity';
import { mockPipe } from '../../../shared/pipes';

import { RunDetailsComponent } from './run-details.component';
import { RunsFacade } from '../../state/runs.facade';

describe('PortalExecutionComponent', () => {
  let component: RunDetailsComponent;
  let fixture: ComponentFixture<RunDetailsComponent>;

  const portalStoreSpy = {
    getExecutionStatus: jest.fn(),
    execution$: of({
      id: 'exec_id',
      status: WorkflowRunStatus.Created,
      startedAt: new Date(0),
      moduleExecutions: [
        {
          id: 'module-id',
          name: 'module-name',
          version: 'module-version',
          description: 'module-description',
          status: ActionRunStatus.Waiting,
        },
      ],
    }),
    executionId$: of('exec_id'),
  };

  const routeSpy = {
    snapshot: {
      url: [
        {
          path: 'execution',
        },
      ],
    },
  };

  beforeEach(async () => {
    portalStoreSpy.getExecutionStatus.mockClear();

    await TestBed.configureTestingModule({
      declarations: [
        RunDetailsComponent,
        mockPipe('statusTagColor'),
        mockPipe('statusName'),
        mockPipe('statusBarColor'),
      ],
      providers: [{ provide: ActivatedRoute, useValue: routeSpy }],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();

    TestBed.overrideProvider(RunsFacade, { useValue: portalStoreSpy });
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RunDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call execution and set variables onInit - portal case', () => {
    routeSpy.snapshot.url[0].path = 'execution';

    expect(portalStoreSpy.getExecutionStatus).toHaveBeenCalled();
    expect(component.showAllDetails).toEqual([false]);
  });

  it('should call execution and set variables onInit - execution detail case', () => {
    routeSpy.snapshot.url[0].path = 'execution_id';

    expect(portalStoreSpy.getExecutionStatus).toHaveBeenCalled();
    expect(component.showAllDetails).toEqual([false]);
  });

  it('should give a state depending on the details shown', () => {
    expect(component.areAllDetailsOpen()).toEqual(false);

    component.showAllDetails[0] = true;

    expect(component.areAllDetailsOpen()).toEqual(true);
  });

  it('should show all details when asked', () => {
    component.toggleAllDetails();

    expect(component.showAllDetails).toEqual([true]);
  });

  it('should hide all details when asked', () => {
    component.toggleAllDetails();
    component.toggleAllDetails();

    expect(component.showAllDetails).toEqual([false]);
  });
});

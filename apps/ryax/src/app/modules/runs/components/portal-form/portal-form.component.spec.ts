// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';

import { PortalFormComponent } from './portal-form.component';

describe('PortalFormComponent', () => {
  let component: PortalFormComponent;
  let fixture: ComponentFixture<PortalFormComponent>;

  const portalStoreSpy = {
    loadPortalInputs: jest.fn(),
    portalInputs$: of(['testInput', 'testInput2']),
    sendFormData: jest.fn(),
  };
  const routeSpy = {
    parent: {
      snapshot: {
        params: {
          workflowId: 'a',
          workflowPortalId: 'b',
        },
      },
    },
  };
  const formData = {
    'f946b952-488c-4358-bbb1-60b04e46de34': 'aze',
  };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PortalFormComponent],
      imports: [RouterTestingModule],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: routeSpy,
        },
      ],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();

    //TestBed.overrideProvider(RunSelectors, {useValue: portalStoreSpy});
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PortalFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should try to load form inputs definition on init', () => {
    expect(portalStoreSpy.loadPortalInputs).toHaveBeenCalled();
  });

  it('should send form data to store', () => {
    component.onSubmit(formData);
    expect(portalStoreSpy.sendFormData).toHaveBeenCalledWith(formData);
  });
});

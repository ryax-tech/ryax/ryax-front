// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PortalFormData } from '../../entities';
import { Location } from '@angular/common';
import { RunsFacade } from '../../state/runs.facade';

@Component({
  selector: 'ryax-portal-form',
  templateUrl: './portal-form.component.pug',
  styleUrls: ['./portal-form.component.scss'],
})
export class PortalFormComponent implements OnInit {
  public portal$ = this.facade.portal$;
  public portalLoading$ = this.facade.portalLoading$;
  public formDataUploading$ = this.facade.formDataUploading$;
  public displayReturn = true;

  constructor(
    private route: ActivatedRoute,
    private location: Location,
    private facade: RunsFacade
  ) {}

  public ngOnInit(): void {
    this.facade.getPortal(this.route.snapshot.params['workflowId']);
  }

  public onSubmit(data: PortalFormData) {
    this.facade.triggerPortalRun(data);
  }

  public back() {
    this.location.back();
  }
}

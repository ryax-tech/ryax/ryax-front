// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  Output,
  SimpleChanges,
} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FormInputType, PortalFormData, PortalInput } from '../../entities';

@Component({
  selector: 'ryax-portal-form-content',
  templateUrl: './portal-form-content.component.pug',
  styleUrls: ['./portal-form-content.component.scss'],
})
export class PortalFormContentComponent implements OnChanges {
  @Input() portalInputs: PortalInput[] | null = [];
  @Output() submitEmitter: EventEmitter<PortalFormData> =
    new EventEmitter<PortalFormData>();

  public form: FormGroup = this.formBuilder.group({});

  public textCheck = [FormInputType.STRING];
  public textareaCheck = [FormInputType.LONGSTRING];
  public passwordCheck = [FormInputType.PASSWORD];
  public numberCheck = [FormInputType.INTEGER, FormInputType.FLOAT];
  public selectCheck = [FormInputType.ENUM];
  public fileCheck = [FormInputType.FILE];
  public dirCheck = [FormInputType.DIRECTORY];
  public boolCheck = [FormInputType.BOOLEAN];

  constructor(private readonly formBuilder: FormBuilder) {}

  public ngOnChanges(changes: SimpleChanges): void {
    if (changes['portalInputs'])
      this.initForm(changes['portalInputs'].currentValue);
  }

  public onSubmit() {
    this.submitEmitter.emit(this.form.value);
  }

  public matchInput(check: FormInputType[], outputType: FormInputType) {
    return check.includes(outputType);
  }

  public switchType(input: HTMLInputElement) {
    if (input.type === 'password') {
      input.type = 'text';
    } else {
      input.type = 'password';
    }
  }

  private initForm(inputs: PortalInput[]) {
    const newForm = this.formBuilder.group({});
    inputs.forEach((item) => {
      const outputControl = this.formBuilder.control(
        null,
        item.optional ? [] : [Validators.required]
      );
      newForm.addControl(item.id, outputControl);
    });
    this.form = newForm;
  }
}

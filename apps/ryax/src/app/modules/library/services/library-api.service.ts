// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { WorkflowModuleAdapter } from '../../studio/entities/adapters/workflow-module/workflow-module.adapter';
import { WorkflowModuleDetailedDto } from '../../studio/entities/dtos/workflow-module.dto';
import {
  WorkflowActionDetailed,
  WorkflowActionLight,
} from '../../studio/entities/workflow-action';
import { RepositoryAdapter } from '../entities/adapters/repository.adapter';
import {
  RepositoryDetailedDto,
  RepositoryLightDto,
  ScannedActionDto,
} from '../entities/dtos/repository.dto';
import {
  RepositoryDetailed,
  RepositoryLight,
  ScannedAction,
} from '../entities/repository';

@Injectable({
  providedIn: 'root',
})
export class LibraryApiService {
  constructor(private http: HttpClient) {}

  public getRepositories(): Observable<RepositoryLight[]> {
    return this.http
      .get<RepositoryLightDto[]>(`/api/repository/v2/sources`)
      .pipe(
        map((dtos) =>
          dtos.map((dto) => new RepositoryAdapter().adaptLight(dto))
        )
      );
  }

  public getRepository(id: string): Observable<RepositoryDetailed> {
    return this.http
      .get<RepositoryDetailedDto>(`/api/repository/v2/sources/${id}`)
      .pipe(map((dto) => new RepositoryAdapter().adaptDetailed(dto)));
  }

  public getActionList(): Observable<WorkflowActionLight[]> {
    return this.http.get<WorkflowActionLight[]>('/api/studio/modules');
  }

  public getAction(id: string): Observable<WorkflowActionDetailed> {
    return this.http
      .get<WorkflowModuleDetailedDto>('/api/studio/modules/' + id)
      .pipe(map((module) => new WorkflowModuleAdapter().adaptDetailed(module)));
  }

  public deleteAction(id: string): Observable<void> {
    return this.http.delete<void>('/api/studio/modules/' + id);
  }

  public newScan(id: string, branch: string): Observable<RepositoryDetailed> {
    return this.http
      .post<RepositoryDetailedDto>(
        `/api/repository/v2/sources/${id}/scan`,
        branch ? { branch } : null
      )
      .pipe(map((dto) => new RepositoryAdapter().adaptDetailed(dto)));
  }

  public build(id: string): Observable<ScannedAction> {
    return this.http
      .post<ScannedActionDto>(`/api/repository/modules/${id}/build`, null)
      .pipe(map((dto) => new RepositoryAdapter().adaptBuiltActions(dto)));
  }

  public cancelBuild(id: string): Observable<ScannedAction> {
    return this.http
      .post<ScannedActionDto>(
        `/api/repository/v2/actions/${id}/cancel_build`,
        null
      )
      .pipe(map((dto) => new RepositoryAdapter().adaptBuiltActions(dto)));
  }

  public cancelAll(id: string): Observable<RepositoryDetailed> {
    return this.http
      .post<RepositoryDetailedDto>(
        `/api/repository/v2/sources/${id}/cancel_all_builds`,
        null
      )
      .pipe(map((dto) => new RepositoryAdapter().adaptDetailed(dto)));
  }

  public buildAll(id: string): Observable<RepositoryDetailed> {
    return this.http
      .post<RepositoryDetailedDto>(
        `/api/repository/v2/sources/${id}/build`,
        null
      )
      .pipe(map((dto) => new RepositoryAdapter().adaptDetailed(dto)));
  }

  public addRepository(formValues: unknown): Observable<RepositoryLight> {
    return this.http
      .post<RepositoryLightDto>(`/api/repository/sources`, formValues)
      .pipe(map((dto) => new RepositoryAdapter().adaptLight(dto)));
  }

  public deleteRepository(id: string): Observable<void> {
    return this.http.delete<void>(`/api/repository/sources/${id}`);
  }
}

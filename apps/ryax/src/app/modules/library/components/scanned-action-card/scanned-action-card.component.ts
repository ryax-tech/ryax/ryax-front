import { Component, Input } from '@angular/core';
import { ScannedAction } from '../../entities/repository';

@Component({
  selector: 'ryax-scanned-action-card',
  templateUrl: './scanned-action-card.component.pug',
  styleUrls: ['./scanned-action-card.component.scss'],
})
export class ScannedActionCardComponent {
  @Input() public action: ScannedAction | undefined;
  @Input() public built = false;
  @Input() public active = false;
}

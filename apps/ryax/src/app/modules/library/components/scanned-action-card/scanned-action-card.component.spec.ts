import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ScannedActionCardComponent } from './scanned-action-card.component';

describe('ScannedActionCardComponent', () => {
  let component: ScannedActionCardComponent;
  let fixture: ComponentFixture<ScannedActionCardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ScannedActionCardComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ScannedActionCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

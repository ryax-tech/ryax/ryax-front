import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LibraryFacade } from '../../state/facade/library.facade';

@Component({
  selector: 'ryax-library',
  templateUrl: './library.component.pug',
  styleUrls: ['./library.component.scss'],
})
export class LibraryComponent implements OnInit {
  public repositories$ = this.facade.repositories$;
  public repositoriesLoading$ = this.facade.repositoriesLoading$;
  public actionList$ = this.facade.actionList$;
  public actionListLoading$ = this.facade.actionListLoading$;
  public searchTerm = '';
  public pageNumber = 1;
  public pageSize = 15;
  public displayModal = false;
  public withCreds = false;
  public gitInfoForm = this.fb.group({
    name: this.fb.control('', Validators.required),
    url: this.fb.control('', Validators.required),
  });

  constructor(private facade: LibraryFacade, private fb: FormBuilder) {}

  public ngOnInit(): void {
    this.facade.getRepositories();
    this.facade.getActionList();
  }

  public refresh(): void {
    this.facade.getRepositories();
    this.facade.getActionList();
  }

  public addCredentials() {
    this.withCreds = true;
    (this.gitInfoForm as FormGroup).addControl(
      'username',
      this.fb.control('', Validators.required)
    );
    (this.gitInfoForm as FormGroup).addControl(
      'password',
      this.fb.control('', Validators.required)
    );
  }

  public removeCredentials() {
    this.withCreds = false;
    (this.gitInfoForm as FormGroup).removeControl('username');
    (this.gitInfoForm as FormGroup).removeControl('password');
  }

  public addNewRepo() {
    this.displayModal = false;
    this.facade.addRepository(this.gitInfoForm.value);
  }

  public cancelNewRepo() {
    this.displayModal = false;
    this.gitInfoForm.reset();
  }
}

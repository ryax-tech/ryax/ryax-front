import { Component, Input } from '@angular/core';
import { LastScanSummary } from '../../entities/repository';

@Component({
  selector: 'ryax-scan-summary',
  templateUrl: './scan-summary.component.pug',
  styleUrls: ['./scan-summary.component.scss'],
})
export class ScanSummaryComponent {
  @Input() public summary: LastScanSummary | undefined;
}

import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NzModalService } from 'ng-zorro-antd/modal';
import { ScannedAction } from '../../entities/repository';
import { LibraryFacade } from '../../state/facade/library.facade';

@Component({
  selector: 'ryax-repository',
  templateUrl: './repository.component.pug',
  styleUrls: ['./repository.component.scss'],
})
export class RepositoryComponent implements OnInit, OnDestroy {
  public currentTab = 0;
  public repositoryFocusedAction$ = this.facade.repositoryFocusedAction$;
  public repository$ = this.facade.repository$;
  public repositoryLoading$ = this.facade.repositoryLoading$;
  public displayModal = false;
  public branchName = '';
  public repositoryPendingActions$ = this.facade.repositoryPendingActions$;

  constructor(
    private facade: LibraryFacade,
    private route: ActivatedRoute,
    private modal: NzModalService
  ) {}

  public ngOnInit() {
    this.facade.getRepository(this.route.snapshot.url[1].path);
  }

  public openActionDetails(action: ScannedAction) {
    this.facade.openDetails(action.id);
  }

  public newScan() {
    this.displayModal = false;
    this.facade.newScan(this.branchName);
  }

  public cancelScan() {
    this.displayModal = false;
    this.branchName = '';
  }

  public buildAll() {
    this.facade.buildAll();
  }

  public cancelAll() {
    this.facade.cancelAll();
  }

  public delete() {
    this.modal.confirm({
      nzTitle: 'Are you sure you want to delete?',
      nzContent: `This will delete the repository saved in Ryax and won't affect your git.`,
      nzOkText: 'Yes',
      nzOkType: 'primary',
      nzOkDanger: true,
      nzOnOk: () => this.facade.deleteRepository(),
      nzCancelText: 'No',
    });
  }

  public tracker(index: number, item: ScannedAction) {
    return JSON.stringify(item);
  }

  public ngOnDestroy() {
    this.facade.closeDetails();
    this.facade.forceStopRefresh();
  }
}

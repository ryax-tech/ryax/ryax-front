import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { NzModalService } from 'ng-zorro-antd/modal';
import { Subscription } from 'rxjs';
import { filter, map, withLatestFrom } from 'rxjs/operators';
import { LibraryFacade } from '../../state/facade/library.facade';

@Component({
  selector: 'ryax-action-details',
  templateUrl: './action-details.component.pug',
  styleUrls: ['./action-details.component.scss'],
})
export class ActionDetailsComponent implements OnInit, OnDestroy {
  public action$ = this.facade.action$;
  public actionLoading$ = this.facade.actionLoading$;
  public currentVersion = this.fb.control({});
  public showLockFile = false;
  private sub = new Subscription();

  constructor(
    private facade: LibraryFacade,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private modal: NzModalService
  ) {}

  public ngOnInit(): void {
    this.facade.getAction(this.route.snapshot.url[1].path);

    this.sub.add(
      this.action$.pipe(filter((action) => !!action)).subscribe((action) => {
        if (action) {
          this.currentVersion.patchValue(action.version, { emitEvent: false });
        }
      })
    );

    this.sub.add(
      this.currentVersion.valueChanges
        .pipe(
          withLatestFrom(this.action$),
          map(
            ([name, action]) =>
              action?.versions.find((version) => version.name === name)?.id
          )
        )
        .subscribe((id) => {
          if (id) {
            this.facade.changeVersion(id);
          }
        })
    );
  }

  public deleteAction() {
    this.modal.confirm({
      nzTitle: 'Are you sure you want to delete?',
      nzContent: 'This will delete the action and might impact your workflows.',
      nzOkText: 'Yes',
      nzOkType: 'primary',
      nzOkDanger: true,
      nzOnOk: () => this.facade.deleteAction(),
      nzCancelText: 'No',
    });
  }

  public ngOnDestroy() {
    this.sub.unsubscribe();
  }

  public toggleDisplayLockfile() {
    this.showLockFile = !this.showLockFile;
  }
}

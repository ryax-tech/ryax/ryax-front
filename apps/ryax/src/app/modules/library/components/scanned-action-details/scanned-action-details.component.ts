import { Component } from '@angular/core';
import { LibraryFacade } from '../../state/facade/library.facade';

@Component({
  selector: 'ryax-scanned-action-details',
  templateUrl: './scanned-action-details.component.pug',
  styleUrls: ['./scanned-action-details.component.scss'],
})
export class ScannedActionDetailsComponent {
  public action$ = this.facade.repositoryFocusedAction$;
  public showLockFile = false;

  constructor(private facade: LibraryFacade) {}

  public build(id: string) {
    this.facade.build(id);
  }

  public cancelBuild(id: string) {
    this.facade.cancelBuild(id);
  }

  public toggleDisplayLockfile() {
    this.showLockFile = !this.showLockFile;
  }
}

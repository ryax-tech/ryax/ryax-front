import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ScannedActionDetailsComponent } from './scanned-action-details.component';

describe('ScannedActionDetailsComponent', () => {
  let component: ScannedActionDetailsComponent;
  let fixture: ComponentFixture<ScannedActionDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ScannedActionDetailsComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ScannedActionDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

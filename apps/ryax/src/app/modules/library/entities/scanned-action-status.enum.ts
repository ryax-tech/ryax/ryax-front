export enum ScannedActionStatus {
  BUILDING = 'Building',
  BUILD_ERROR = 'Build Error',
  BUILD_PENDING = 'Build Pending',
  BUILT = 'Built',
  CANCELLED = 'Cancelled',
  CANCELLING = 'Cancelling',
  SCANNED = 'Scanned',
  SCANNING = 'Scanning',
  SCAN_ERROR = 'Scan Error',
  STARTING = 'Starting',
}

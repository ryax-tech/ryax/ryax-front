import { DateTime } from 'luxon';
import { ScannedActionStatus } from './scanned-action-status.enum';

export interface RepositoryLight {
  id: string;
  name: string;
  url: string;
  username: string;
  passwordSet: boolean;
  lastScanSummary?: LastScanSummary;
}

export interface LastScanSummary {
  building: number;
  built: number;
  error: number;
  scanned: number;
  scanDate: DateTime;
}

export interface RepositoryDetailed {
  id: string;
  name: string;
  url: string;
  lastScan?: {
    builtActions: ScannedAction[];
    notBuiltActions: ScannedAction[];
    commitSha: string;
    scanDate: DateTime;
  };
}

export interface ScannedAction {
  buildError: string;
  creationDate: DateTime;
  description: string;
  lockfile: string;
  dynamicOutputs: boolean;
  id: string;
  kind: string;
  metadataPath: string;
  name: string;
  scanErrors: string;
  commitSha: string;
  status: ScannedActionStatus;
  technicalName: string;
  type: string;
  version: string;
  lastBuild: DateTime;
}

// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

import { DateTime } from 'luxon';
import {
  RepositoryDetailedDto,
  RepositoryLightDto,
  ScannedActionDto,
} from '../dtos/repository.dto';
import {
  RepositoryDetailed,
  RepositoryLight,
  ScannedAction,
} from '../repository';
import { ScannedActionStatus } from '../scanned-action-status.enum';

export class RepositoryAdapter {
  public adaptLight(dto: RepositoryLightDto): RepositoryLight {
    let repo: RepositoryLight = {
      id: dto.id,
      name: dto.name,
      url: dto.url,
      username: dto.username,
      passwordSet: dto.password_is_set,
    };
    if (dto.last_scan_summary) {
      repo = {
        ...repo,
        lastScanSummary: {
          built: dto.last_scan_summary.actions_built,
          building: dto.last_scan_summary.actions_building,
          error: dto.last_scan_summary.actions_error,
          scanned: dto.last_scan_summary.actions_scanned,
          scanDate: DateTime.fromISO(dto.last_scan_summary.scan_date),
        },
      };
    }
    return repo;
  }

  public adaptDetailed(dto: RepositoryDetailedDto): RepositoryDetailed {
    let repo: RepositoryDetailed = {
      id: dto.id,
      name: dto.name,
      url: dto.url,
    };
    if (dto.last_scan) {
      repo = {
        ...repo,
        lastScan: {
          builtActions: dto.last_scan.built_actions.map((built) =>
            this.adaptBuiltActions(built)
          ),
          notBuiltActions: dto.last_scan.not_built_actions.map((notBuilt) =>
            this.adaptBuiltActions(notBuilt)
          ),
          commitSha: dto.last_scan.sha,
          scanDate: DateTime.fromISO(dto.last_scan.scan_date),
        },
      };
    }
    return repo;
  }

  public adaptBuiltActions(dto: ScannedActionDto): ScannedAction {
    return {
      buildError: dto.build_error,
      lastBuild: DateTime.fromISO(dto.last_build),
      creationDate: DateTime.fromISO(dto.creation_date),
      description: dto.description,
      lockfile: dto.lockfile,
      dynamicOutputs: dto.dynamic_outputs,
      id: dto.id,
      kind: dto.kind,
      metadataPath: dto.metadata_path,
      name: dto.name,
      scanErrors: dto.scan_errors.toString(),
      commitSha: dto.sha,
      status: dto.status as ScannedActionStatus,
      technicalName: dto.technical_name,
      type: dto.type,
      version: dto.version,
    };
  }
}

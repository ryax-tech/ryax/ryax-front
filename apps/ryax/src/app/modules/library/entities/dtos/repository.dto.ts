export interface RepositoryLightDto {
  id: string;
  name: string;
  url: string;
  username: string;
  password_is_set: boolean;
  last_scan_summary?: {
    actions_building: number;
    actions_built: number;
    actions_error: number;
    actions_scanned: number;
    scan_date: string;
  };
}

export interface RepositoryDetailedDto {
  id: string;
  name: string;
  url: string;
  last_scan: {
    built_actions: ScannedActionDto[];
    not_built_actions: ScannedActionDto[];
    sha: string;
    scan_date: string;
  };
}

export interface ScannedActionDto {
  build_error: string;
  build_status: string;
  last_build: string;
  creation_date: string;
  description: string;
  lockfile: string;
  dynamic_outputs: boolean;
  id: string;
  kind: string;
  metadata_path: string;
  name: string;
  scan_errors: string[];
  sha: string;
  status: string;
  technical_name: string;
  type: string;
  version: string;
}

// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { of } from 'rxjs';
import {
  catchError,
  delay,
  map,
  switchMap,
  tap,
  withLatestFrom,
} from 'rxjs/operators';
import { WorkflowActionDetailed } from '../../../studio/entities/workflow-action';
import { RepositoryDetailed } from '../../entities/repository';
import { LibraryApiService } from '../../services/library-api.service';
import { LibraryState } from '../reducers/index';
import { LibraryActions } from '../actions';
import {
  selectAction,
  selectForceStopRefresh,
  selectRepository,
} from '../selectors/library.selectors';
import { NzNotificationService } from 'ng-zorro-antd/notification';

@Injectable()
export class LibraryEffects {
  getRepositories$ = createEffect(() =>
    this.actions$.pipe(
      ofType(LibraryActions.getRepositories),
      switchMap(() =>
        this.libraryApiService.getRepositories().pipe(
          map((repositories) => {
            return LibraryActions.getRepositoriesSuccess(repositories);
          }),
          catchError((err: HttpErrorResponse) => {
            return of(LibraryActions.getRepositoriesError(err));
          })
        )
      )
    )
  );

  getRepository$ = createEffect(() =>
    this.actions$.pipe(
      ofType(LibraryActions.getRepository),
      switchMap(({ id }) =>
        this.libraryApiService.getRepository(id).pipe(
          switchMap((repository) => {
            return [
              LibraryActions.getRepositorySuccess(repository),
              LibraryActions.keepRefreshingRepository(repository),
            ];
          }),
          catchError((err: HttpErrorResponse) => {
            return of(LibraryActions.getRepositoryError(err));
          })
        )
      )
    )
  );

  refreshRepository$ = createEffect(() =>
    this.actions$.pipe(
      ofType(
        LibraryActions.buildSuccess,
        LibraryActions.buildAllSuccess,
        LibraryActions.keepRefreshingRepository
      ),
      withLatestFrom(this.store$.select(selectRepository)),
      withLatestFrom(
        this.store$.select(selectForceStopRefresh),
        ([, repo], forceStop) => ({
          repo: repo as RepositoryDetailed,
          forceStop,
        })
      ),
      switchMap(({ repo, forceStop }) => {
        if (forceStop) {
          return [LibraryActions.forceStopRefreshSuccess()];
        }
        return this.libraryApiService.getRepository(repo.id).pipe(
          delay(1000),
          map((repository) => {
            if (repository.lastScan) {
              const keepRefreshing = repository.lastScan.notBuiltActions.reduce(
                (result, action) =>
                  result ||
                  action.status === 'Building' ||
                  action.status === 'Starting' ||
                  action.status === 'Cancelling' ||
                  action.status === 'Build Pending',
                false
              );
              if (keepRefreshing) {
                return LibraryActions.keepRefreshingRepository(repository);
              }
            }
            return LibraryActions.getRepositorySuccess(repository);
          }),
          catchError((err: HttpErrorResponse) =>
            of(LibraryActions.getRepositoryError(err))
          )
        );
      })
    )
  );

  handleStop$ = createEffect(() =>
    this.actions$.pipe(
      ofType(LibraryActions.forceStopRefresh),
      delay(2000),
      map(() => LibraryActions.forceStopRefreshSuccess())
    )
  );

  getActionList$ = createEffect(() =>
    this.actions$.pipe(
      ofType(LibraryActions.getActionList),
      switchMap(() =>
        this.libraryApiService.getActionList().pipe(
          map((actions) => {
            return LibraryActions.getActionListSuccess(actions);
          }),
          catchError((err: HttpErrorResponse) => {
            return of(LibraryActions.getActionListError(err));
          })
        )
      )
    )
  );

  getAction$ = createEffect(() =>
    this.actions$.pipe(
      ofType(LibraryActions.getAction),
      switchMap(({ id }) =>
        this.libraryApiService.getAction(id).pipe(
          map((action) => {
            return LibraryActions.getActionSuccess(action);
          }),
          catchError((err: HttpErrorResponse) => {
            return of(LibraryActions.getActionError(err));
          })
        )
      )
    )
  );

  deleteAction$ = createEffect(() =>
    this.actions$.pipe(
      ofType(LibraryActions.deleteAction),
      withLatestFrom(
        this.store$.select(selectAction),
        (a, action) => action as WorkflowActionDetailed
      ),
      switchMap((action) =>
        this.libraryApiService.deleteAction(action.id).pipe(
          map(() => {
            this.router.navigateByUrl('/library');
            return LibraryActions.deleteActionSuccess();
          }),
          catchError((err: HttpErrorResponse) => {
            return of(LibraryActions.deleteActionError(err));
          })
        )
      )
    )
  );

  newScan$ = createEffect(() =>
    this.actions$.pipe(
      ofType(LibraryActions.newScan),
      withLatestFrom(this.store$.select(selectRepository), (action, repo) => ({
        branch: action.branch,
        repo: repo as RepositoryDetailed,
      })),
      switchMap(({ branch, repo }) =>
        this.libraryApiService.newScan(repo.id, branch).pipe(
          map((scan) => {
            return LibraryActions.newScanSuccess(scan);
          }),
          catchError((err: HttpErrorResponse) => {
            return of(LibraryActions.newScanError(err));
          })
        )
      )
    )
  );

  build$ = createEffect(() =>
    this.actions$.pipe(
      ofType(LibraryActions.build),
      switchMap(({ id }) =>
        this.libraryApiService.build(id).pipe(
          map((action) => {
            return LibraryActions.buildSuccess(action);
          }),
          catchError((err: HttpErrorResponse) => {
            return of(LibraryActions.buildError(err));
          })
        )
      )
    )
  );

  cancel_build$ = createEffect(() =>
    this.actions$.pipe(
      ofType(LibraryActions.cancelBuild),
      switchMap(({ id }) =>
        this.libraryApiService.cancelBuild(id).pipe(
          map((action) => {
            return LibraryActions.buildSuccess(action);
          }),
          catchError((err: HttpErrorResponse) => {
            return of(LibraryActions.buildError(err));
          })
        )
      )
    )
  );

  buildAll$ = createEffect(() =>
    this.actions$.pipe(
      ofType(LibraryActions.buildAll),
      withLatestFrom(
        this.store$.select(selectRepository),
        (action, repo) => repo as RepositoryDetailed
      ),
      switchMap((repo) =>
        this.libraryApiService.buildAll(repo.id).pipe(
          map((repo) => {
            return LibraryActions.buildAllSuccess(repo);
          }),
          catchError((err: HttpErrorResponse) => {
            return of(LibraryActions.buildAllError(err));
          })
        )
      )
    )
  );

  cancelAll$ = createEffect(() =>
    this.actions$.pipe(
      ofType(LibraryActions.cancelAll),
      withLatestFrom(
        this.store$.select(selectRepository),
        (action, repo) => repo as RepositoryDetailed
      ),
      switchMap((repo) =>
        this.libraryApiService.cancelAll(repo.id).pipe(
          map((repo) => {
            return LibraryActions.buildAllSuccess(repo);
          }),
          catchError((err: HttpErrorResponse) => {
            return of(LibraryActions.buildAllError(err));
          })
        )
      )
    )
  );

  addRepository$ = createEffect(() =>
    this.actions$.pipe(
      ofType(LibraryActions.addRepository),
      switchMap(({ formValues }) =>
        this.libraryApiService.addRepository(formValues).pipe(
          map((repo) => {
            return LibraryActions.addRepositorySuccess(repo);
          }),
          catchError((err: HttpErrorResponse) => {
            return of(LibraryActions.addRepositoryError(err));
          })
        )
      )
    )
  );

  deleteRepository$ = createEffect(() =>
    this.actions$.pipe(
      ofType(LibraryActions.deleteRepository),
      withLatestFrom(
        this.store$.select(selectRepository),
        (action, repo) => repo as RepositoryDetailed
      ),
      switchMap((repo) =>
        this.libraryApiService.deleteRepository(repo.id).pipe(
          map(() => {
            this.router.navigateByUrl('/library');
            return LibraryActions.deleteRepositorySuccess();
          }),
          catchError((err: HttpErrorResponse) => {
            return of(LibraryActions.deleteRepositoryError(err));
          })
        )
      )
    )
  );

  displayErrors$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(
          LibraryActions.getRepositoriesError,
          LibraryActions.getRepositoryError,
          LibraryActions.deleteRepositoryError,
          LibraryActions.addRepositoryError,
          LibraryActions.newScanError,
          LibraryActions.buildError,
          LibraryActions.buildAllError,
          LibraryActions.deleteActionError,
          LibraryActions.getActionError,
          LibraryActions.getActionListError
        ),
        tap((errAction) => {
          console.error(errAction.error);
          this.notification.create(
            'error',
            'Something went wrong ! http error : ' + errAction.error.status,
            errAction.error.message
          );
        })
      ),
    { dispatch: false }
  );

  constructor(
    private actions$: Actions,
    private libraryApiService: LibraryApiService,
    private store$: Store<LibraryState>,
    private router: Router,
    private notification: NzNotificationService
  ) {}
}

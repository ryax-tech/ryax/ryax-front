// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { GlobalLibraryState } from '../reducers';
import { LibraryActions } from '../actions';
import {
  selectAction,
  selectActionList,
  selectActionListLoading,
  selectActionLoading,
  selectRepositories,
  selectRepositoriesLoading,
  selectRepository,
  selectRepositoryFocusedAction,
  selectRepositoryLoading,
  selectPendingActions,
} from '../selectors/library.selectors';

@Injectable({
  providedIn: 'root',
})
export class LibraryFacade {
  public repositories$ = this.store.select(selectRepositories);
  public repositoriesLoading$ = this.store.select(selectRepositoriesLoading);
  public repository$ = this.store.select(selectRepository);
  public repositoryLoading$ = this.store.select(selectRepositoryLoading);
  public repositoryPendingActions$ = this.store.select(selectPendingActions);
  public repositoryFocusedAction$ = this.store.select(
    selectRepositoryFocusedAction
  );
  public actionList$ = this.store.select(selectActionList);
  public actionListLoading$ = this.store.select(selectActionListLoading);
  public action$ = this.store.select(selectAction);
  public actionLoading$ = this.store.select(selectActionLoading);

  constructor(private readonly store: Store<GlobalLibraryState>) {}

  public getRepositories() {
    this.store.dispatch(LibraryActions.getRepositories());
  }

  public getRepository(id: string) {
    this.store.dispatch(LibraryActions.getRepository(id));
  }

  public openDetails(id: string) {
    this.store.dispatch(LibraryActions.selectFocusedAction(id));
  }

  public closeDetails() {
    this.store.dispatch(LibraryActions.selectFocusedAction(null));
  }

  public getActionList() {
    this.store.dispatch(LibraryActions.getActionList());
  }

  public getAction(id: string) {
    this.store.dispatch(LibraryActions.getAction(id));
  }

  public changeVersion(id: string) {
    this.store.dispatch(LibraryActions.getAction(id));
  }

  public deleteAction() {
    this.store.dispatch(LibraryActions.deleteAction());
  }

  public newScan(branch: string) {
    this.store.dispatch(LibraryActions.newScan(branch));
  }

  public build(id: string) {
    this.store.dispatch(LibraryActions.build(id));
  }

  public cancelBuild(id: string) {
    this.store.dispatch(LibraryActions.cancelBuild(id));
  }

  public buildAll() {
    this.store.dispatch(LibraryActions.buildAll());
  }

  public cancelAll() {
    this.store.dispatch(LibraryActions.cancelAll());
  }

  public forceStopRefresh() {
    this.store.dispatch(LibraryActions.forceStopRefresh());
  }

  public addRepository(formValues: unknown) {
    this.store.dispatch(LibraryActions.addRepository(formValues));
  }

  public deleteRepository() {
    this.store.dispatch(LibraryActions.deleteRepository());
  }
}

// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { createFeatureSelector, createSelector } from '@ngrx/store';
import {
  GlobalLibraryState,
  LibraryFeatureKey,
  LibraryState,
} from '../reducers';

const selectLibraryFn = (state: GlobalLibraryState) => state.library;
const selectRepositoriesFn = (state: LibraryState) => state.repositories.value;
const selectRepositoriesLoadingFn = (state: LibraryState) =>
  state.repositories.loading;
const selectRepositoriesErrorFn = (state: LibraryState) =>
  state.repositories.error;
const selectRepositoryFn = (state: LibraryState) =>
  state.currentRepository.value;
const selectRepositoryLoadingFn = (state: LibraryState) =>
  state.currentRepository.loading;
const selectRepositoryErrorFn = (state: LibraryState) =>
  state.currentRepository.error;
const selectRepositoryFocusedActionFn = (state: LibraryState) => {
  if (state.currentRepository.value?.lastScan) {
    return [
      ...state.currentRepository.value.lastScan.notBuiltActions,
      ...state.currentRepository.value.lastScan.builtActions,
    ].find((action) => action.id === state.focusedActionId);
  } else {
    return null;
  }
};
const selectPendingActionsFn = (state: LibraryState) => {
  if (state.currentRepository.value?.lastScan) {
    return state.currentRepository.value.lastScan.notBuiltActions.filter(
      (action) =>
        action.status === 'Build Pending' || action.status === 'Building'
    );
  } else {
    return [];
  }
};
const selectForceStopRefreshFn = (state: LibraryState) =>
  state.forceStopRefresh;
const selectActionListFn = (state: LibraryState) => state.actions.value;
const selectActionListLoadingFn = (state: LibraryState) =>
  state.actions.loading;
const selectActionListErrorFn = (state: LibraryState) => state.actions.error;
const selectActionFn = (state: LibraryState) => state.currentActionDetail.value;
const selectActionLoadingFn = (state: LibraryState) =>
  state.currentActionDetail.loading;
const selectActionErrorFn = (state: LibraryState) =>
  state.currentActionDetail.error;

export const selectGlobalLibraryState =
  createFeatureSelector<GlobalLibraryState>(LibraryFeatureKey);
export const selectLibraryState = createSelector(
  selectGlobalLibraryState,
  selectLibraryFn
);
export const selectRepositories = createSelector(
  selectLibraryState,
  selectRepositoriesFn
);
export const selectRepositoriesLoading = createSelector(
  selectLibraryState,
  selectRepositoriesLoadingFn
);
export const selectRepositoriesError = createSelector(
  selectLibraryState,
  selectRepositoriesErrorFn
);
export const selectRepository = createSelector(
  selectLibraryState,
  selectRepositoryFn
);
export const selectRepositoryLoading = createSelector(
  selectLibraryState,
  selectRepositoryLoadingFn
);
export const selectRepositoryError = createSelector(
  selectLibraryState,
  selectRepositoryErrorFn
);
export const selectRepositoryFocusedAction = createSelector(
  selectLibraryState,
  selectRepositoryFocusedActionFn
);
export const selectForceStopRefresh = createSelector(
  selectLibraryState,
  selectForceStopRefreshFn
);
export const selectActionList = createSelector(
  selectLibraryState,
  selectActionListFn
);
export const selectActionListLoading = createSelector(
  selectLibraryState,
  selectActionListLoadingFn
);
export const selectActionListError = createSelector(
  selectLibraryState,
  selectActionListErrorFn
);
export const selectAction = createSelector(selectLibraryState, selectActionFn);
export const selectActionLoading = createSelector(
  selectLibraryState,
  selectActionLoadingFn
);
export const selectActionError = createSelector(
  selectLibraryState,
  selectActionErrorFn
);
export const selectPendingActions = createSelector(
  selectLibraryState,
  selectPendingActionsFn
);

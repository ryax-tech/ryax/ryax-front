// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
export * from './library.reducer';

import { InjectionToken } from '@angular/core';
import { ActionReducerMap } from '@ngrx/store';
import { libraryReducer, LibraryState } from './library.reducer';

export interface GlobalLibraryState {
  library: LibraryState;
}

export const LibraryFeatureKey = 'globalLibraryDomain';

export const LibraryReducerToken = new InjectionToken<
  ActionReducerMap<GlobalLibraryState>
>(LibraryFeatureKey);

export const LibraryReducerProvider = {
  provide: LibraryReducerToken,
  useValue: {
    library: libraryReducer,
  },
};

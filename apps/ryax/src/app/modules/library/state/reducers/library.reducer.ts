// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { createReducer, on } from '@ngrx/store';
import {
  DefaultValueState,
  LoadingState,
} from '../../../shared/entity/default-states';
import {
  WorkflowActionDetailed,
  WorkflowActionLight,
} from '../../../studio/entities/workflow-action';
import { RepositoryDetailed, RepositoryLight } from '../../entities/repository';
import { LibraryActions } from '../actions';

export interface LibraryState {
  repositories: DefaultValueState<RepositoryLight[]>;
  currentRepository: DefaultValueState<RepositoryDetailed | null>;
  focusedActionId: string | null;
  forceStopRefresh: boolean;
  actions: DefaultValueState<WorkflowActionLight[]>;
  currentActionDetail: DefaultValueState<WorkflowActionDetailed | null>;
  buildAction: LoadingState;
  deleteRepo: LoadingState;
}

export const initialLibraryState: LibraryState = {
  repositories: {
    value: [],
    loading: false,
    error: null,
  },
  currentRepository: {
    value: null,
    loading: false,
    error: null,
  },
  focusedActionId: null,
  forceStopRefresh: false,
  actions: {
    value: [],
    loading: false,
    error: null,
  },
  currentActionDetail: {
    value: null,
    loading: false,
    error: null,
  },
  buildAction: {
    loading: false,
    error: null,
  },
  deleteRepo: {
    loading: false,
    error: null,
  },
};

export const libraryReducer = createReducer<LibraryState>(
  initialLibraryState,
  on(LibraryActions.getRepositories, (state) => ({
    ...state,
    repositories: {
      ...state.repositories,
      loading: true,
      error: null,
    },
  })),
  on(LibraryActions.getRepositoriesSuccess, (state, { repositories }) => ({
    ...state,
    repositories: {
      value: repositories,
      loading: false,
      error: null,
    },
  })),
  on(LibraryActions.getRepositoriesError, (state, { error }) => ({
    ...state,
    repositories: {
      ...state.repositories,
      loading: false,
      error,
    },
  })),
  on(LibraryActions.getRepository, (state) => ({
    ...state,
    currentRepository: {
      ...state.currentRepository,
      loading: true,
      error: null,
    },
  })),
  on(
    LibraryActions.getRepositorySuccess,
    LibraryActions.keepRefreshingRepository,
    (state, { repository }) => ({
      ...state,
      currentRepository: {
        value: repository,
        loading: false,
        error: null,
      },
    })
  ),
  on(LibraryActions.forceStopRefresh, (state) => ({
    ...state,
    forceStopRefresh: true,
  })),
  on(LibraryActions.forceStopRefreshSuccess, (state) => ({
    ...state,
    forceStopRefresh: false,
  })),
  on(LibraryActions.getRepositoryError, (state, { error }) => ({
    ...state,
    currentRepository: {
      ...state.currentRepository,
      loading: false,
      error,
    },
  })),
  on(LibraryActions.selectFocusedAction, (state, { actionId }) => ({
    ...state,
    focusedActionId: actionId,
  })),
  on(LibraryActions.getActionList, (state) => ({
    ...state,
    actions: {
      ...state.actions,
      loading: true,
      error: null,
    },
  })),
  on(LibraryActions.getActionListSuccess, (state, { actions }) => ({
    ...state,
    actions: {
      value: actions,
      loading: false,
      error: null,
    },
  })),
  on(LibraryActions.getActionListError, (state, { error }) => ({
    ...state,
    actions: {
      ...state.actions,
      loading: false,
      error,
    },
  })),
  on(LibraryActions.getAction, (state) => ({
    ...state,
    currentActionDetail: {
      ...state.currentActionDetail,
      loading: true,
      error: null,
    },
  })),
  on(LibraryActions.getActionSuccess, (state, { action }) => ({
    ...state,
    currentActionDetail: {
      value: action,
      loading: false,
      error: null,
    },
  })),
  on(LibraryActions.getActionError, (state, { error }) => ({
    ...state,
    currentActionDetail: {
      ...state.currentActionDetail,
      loading: false,
      error,
    },
  })),
  on(LibraryActions.deleteAction, (state) => ({
    ...state,
    currentActionDetail: {
      ...state.currentActionDetail,
      loading: true,
      error: null,
    },
  })),
  on(LibraryActions.deleteActionSuccess, (state) => ({
    ...state,
    currentActionDetail: {
      value: null,
      loading: false,
      error: null,
    },
  })),
  on(LibraryActions.deleteActionError, (state, { error }) => ({
    ...state,
    currentActionDetail: {
      ...state.currentActionDetail,
      loading: false,
      error,
    },
  })),
  on(LibraryActions.newScan, (state) => ({
    ...state,
    currentRepository: {
      ...state.currentRepository,
      loading: true,
      error: null,
    },
  })),
  on(LibraryActions.newScanSuccess, (state, { newScan }) => ({
    ...state,
    currentRepository: {
      value: newScan,
      loading: false,
      error: null,
    },
  })),
  on(LibraryActions.newScanError, (state, { error }) => ({
    ...state,
    currentRepository: {
      ...state.currentRepository,
      loading: false,
      error,
    },
  })),
  on(LibraryActions.build, (state) => ({
    ...state,
    buildAction: {
      loading: true,
      error: null,
    },
  })),
  on(LibraryActions.cancelBuild, (state) => ({
    ...state,
    buildAction: {
      loading: true,
      error: null,
    },
  })),
  on(LibraryActions.buildSuccess, (state) => ({
    ...state,
    buildAction: {
      loading: false,
      error: null,
    },
  })),
  on(LibraryActions.buildError, (state, { error }) => ({
    ...state,
    buildAction: {
      loading: false,
      error,
    },
  })),
  on(LibraryActions.addRepository, (state) => ({
    ...state,
    repositories: {
      ...state.repositories,
      loading: true,
      error: null,
    },
  })),
  on(LibraryActions.addRepositorySuccess, (state, { repo }) => ({
    ...state,
    repositories: {
      value: [...state.repositories.value, repo],
      loading: false,
      error: null,
    },
  })),
  on(LibraryActions.addRepositoryError, (state, { error }) => ({
    ...state,
    repositories: {
      ...state.repositories,
      loading: false,
      error,
    },
  })),
  on(LibraryActions.deleteRepository, (state) => ({
    ...state,
    deleteRepo: {
      loading: true,
      error: null,
    },
  })),
  on(LibraryActions.deleteRepositorySuccess, (state) => ({
    ...state,
    deleteRepo: {
      loading: false,
      error: null,
    },
  })),
  on(LibraryActions.deleteRepositoryError, (state, { error }) => ({
    ...state,
    deleteRepo: {
      loading: false,
      error,
    },
  }))
);

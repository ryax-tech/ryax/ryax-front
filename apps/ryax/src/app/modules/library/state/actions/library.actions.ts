// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { HttpErrorResponse } from '@angular/common/http';
import { createAction } from '@ngrx/store';
import {
  WorkflowActionDetailed,
  WorkflowActionLight,
} from '../../../studio/entities/workflow-action';
import {
  RepositoryDetailed,
  RepositoryLight,
  ScannedAction,
} from '../../entities/repository';

export const getRepositories = createAction('[Library] Get repositories');

export const getRepositoriesSuccess = createAction(
  '[Library] Get repositories success',
  (repositories: RepositoryLight[]) => ({ repositories })
);

export const getRepositoriesError = createAction(
  '[Library] Get repositories error',
  (error: HttpErrorResponse) => ({ error })
);

export const getRepository = createAction(
  '[Library] Get repository',
  (id: string) => ({ id })
);

export const keepRefreshingRepository = createAction(
  '[Library] Keep refreshing repository',
  (repository: RepositoryDetailed) => ({ repository })
);

export const forceStopRefresh = createAction(
  '[Library] Stop refreshing repository'
);

export const forceStopRefreshSuccess = createAction(
  '[Library] Stopped refreshing repository'
);

export const getRepositorySuccess = createAction(
  '[Library] Get repository success',
  (repository: RepositoryDetailed) => ({ repository })
);

export const getRepositoryError = createAction(
  '[Library] Get repository error',
  (error: HttpErrorResponse) => ({ error })
);

export const selectFocusedAction = createAction(
  '[Library] Select focused action',
  (actionId: string | null) => ({ actionId })
);

export const getActionList = createAction('[Library] Get action list');

export const getActionListSuccess = createAction(
  '[Library] Get action list success',
  (actions: WorkflowActionLight[]) => ({ actions })
);

export const getActionListError = createAction(
  '[Library] Get action list error',
  (error: HttpErrorResponse) => ({ error })
);

export const getAction = createAction('[Library] Get action', (id: string) => ({
  id,
}));

export const getActionSuccess = createAction(
  '[Library] Get action success',
  (action: WorkflowActionDetailed) => ({ action })
);

export const getActionError = createAction(
  '[Library] Get action error',
  (error: HttpErrorResponse) => ({ error })
);

export const deleteAction = createAction('[Library] Delete action');

export const deleteActionSuccess = createAction(
  '[Library] Delete action success'
);

export const deleteActionError = createAction(
  '[Library] Delete action error',
  (error: HttpErrorResponse) => ({ error })
);

export const newScan = createAction('[Library] New scan', (branch: string) => ({
  branch,
}));

export const newScanSuccess = createAction(
  '[Library] New scan success',
  (newScan: RepositoryDetailed) => ({ newScan })
);

export const newScanError = createAction(
  '[Library] New scan error',
  (error: HttpErrorResponse) => ({ error })
);

export const build = createAction('[Library] Build', (id: string) => ({ id }));

export const cancelBuild = createAction(
  '[Library] Cancel Build',
  (id: string) => ({ id })
);

export const cancelAll = createAction('[Library] Cancel all');

export const buildSuccess = createAction(
  '[Library] Build success',
  (action: ScannedAction) => ({ action })
);

export const buildError = createAction(
  '[Library] Build error',
  (error: HttpErrorResponse) => ({ error })
);

export const buildAll = createAction('[Library] Build all');

export const buildAllSuccess = createAction(
  '[Library] Build all success',
  (repo: RepositoryDetailed) => ({ repo })
);

export const buildAllError = createAction(
  '[Library] Build all error',
  (error: HttpErrorResponse) => ({ error })
);

export const addRepository = createAction(
  '[Library] Add repository',
  (formValues: unknown) => ({ formValues })
);

export const addRepositorySuccess = createAction(
  '[Library] Add repository success',
  (repo: RepositoryLight) => ({ repo })
);

export const addRepositoryError = createAction(
  '[Library] Add repository error',
  (error: HttpErrorResponse) => ({ error })
);

export const deleteRepository = createAction('[Library] Delete repository');

export const deleteRepositorySuccess = createAction(
  '[Library] Delete repository success'
);

export const deleteRepositoryError = createAction(
  '[Library] Delete repository error',
  (error: HttpErrorResponse) => ({ error })
);

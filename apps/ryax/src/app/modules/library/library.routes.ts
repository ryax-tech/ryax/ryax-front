// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { LibraryComponent } from './components/library/library.component';
import { ActionDetailsComponent } from './components/action-details/action-details.component';
import { RepositoryComponent } from './components/repository/repository.component';
import { Routes } from '@angular/router';

export const libraryRoutes: Routes = [
  {
    path: '',
    component: LibraryComponent,
  },
  {
    path: 'action/:actionId',
    component: ActionDetailsComponent,
  },
  {
    path: 'repository/:repositoryId',
    component: RepositoryComponent,
  },
  {
    path: '',
    pathMatch: 'full',
    redirectTo: '',
  },
];

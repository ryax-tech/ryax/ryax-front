import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { SharedModule } from '../shared/shared.module';
import { ActionDetailsComponent } from './components/action-details/action-details.component';
import { LibraryComponent } from './components/library/library.component';
import { RepositoryComponent } from './components/repository/repository.component';
import { ScanSummaryComponent } from './components/scan-summary/scan-summary.component';
import { ScannedActionCardComponent } from './components/scanned-action-card/scanned-action-card.component';
import { ScannedActionDetailsComponent } from './components/scanned-action-details/scanned-action-details.component';
import { LibraryApiService } from './services/library-api.service';
import { LibraryEffects } from './state/effects/library.effects';
import {
  LibraryFeatureKey,
  LibraryReducerProvider,
  LibraryReducerToken,
} from './state/reducers/index';

@NgModule({
  imports: [
    CommonModule,
    StoreModule.forFeature(LibraryFeatureKey, LibraryReducerToken),
    EffectsModule.forFeature([LibraryEffects]),
    RouterModule,
    SharedModule,
  ],
  declarations: [
    LibraryComponent,
    ActionDetailsComponent,
    RepositoryComponent,
    ScannedActionCardComponent,
    ScannedActionDetailsComponent,
    ScanSummaryComponent,
  ],
  providers: [LibraryReducerProvider, LibraryApiService],
})
export class LibraryModule {}

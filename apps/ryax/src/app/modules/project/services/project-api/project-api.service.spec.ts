// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { ProjectDto } from '../../entities/dtos/index';
import { Project } from '../../entities/index';

import { ProjectApiService } from './project-api.service';

describe('ProjectApiService', () => {
  let service: ProjectApiService;
  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [ProjectApiService],
    });
    service = TestBed.inject(ProjectApiService);
    httpTestingController = TestBed.inject(HttpTestingController);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should get project list when asked', () => {
    const projects: Project[] = [
      { name: 'toto', id: 'test1', current: true, creationDate: new Date() },
    ];
    const projectsDto: ProjectDto[] = [
      { name: 'toto', id: 'test1', current: true, creation_date: new Date() },
    ];

    service.getProjects().subscribe((response) => {
      expect(response).toEqual(projects);
    });

    const request = httpTestingController.expectOne(
      '/authorization/v2/projects'
    );
    expect(request.request.method).toEqual('GET');
    request.flush(projectsDto);
  });

  it('should set current project when asked', () => {
    service.setProject('p_id').subscribe((response) => {
      expect(response).toEqual(null);
    });

    const request = httpTestingController.expectOne(
      '/authorization/v2/projects/current'
    );
    expect(request.request.method).toEqual('POST');
    expect(request.request.body).toEqual({ project_id: 'p_id' });
    request.flush(null);
  });
});

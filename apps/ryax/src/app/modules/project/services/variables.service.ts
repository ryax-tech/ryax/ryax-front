// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Variable } from '../entities/variable';
import { VariableAdapter } from '../entities/adapters/variable.adapter';
import { VariableDto } from '../entities/dtos/variable.dto';

@Injectable({
  providedIn: 'root',
})
export class VariablesApiService {
  private baseUrl = '/api/studio/v2/user-objects';

  constructor(private http: HttpClient) {}

  public getVariables(): Observable<Variable[]> {
    return this.http
      .get<VariableDto[]>(this.baseUrl)
      .pipe(map((dto) => new VariableAdapter().adapt(dto)));
  }

  public addVariable(variable: Variable) {
    return this.http.post(this.baseUrl, variable);
  }

  public addFileVariable(variable: Variable) {
    const formData = new FormData();
    const file: File = <File>variable.value;
    formData.append('name', variable.name);
    formData.append('description', variable.description);
    formData.append('type', variable.type);
    formData.append('file', file, file.name);
    return this.http.post(this.baseUrl + '/file', formData, {
      responseType: 'json',
    });
  }

  public editVariable(variable: Variable) {
    return this.http.put(this.baseUrl + `/${variable.id}`, variable);
  }

  public editFileVariable(variable: Variable) {
    const formData = new FormData();
    const file: File = <File>variable.value;
    formData.append('name', variable.name);
    formData.append('description', variable.description);
    formData.append('type', variable.type);
    formData.append('file', file, file.name);
    return this.http.put(this.baseUrl + `/${variable.id}/file`, formData, {
      responseType: 'json',
    });
  }

  public deleteVariable(variableId: string) {
    return this.http.delete(this.baseUrl + `/${variableId}`);
  }
}

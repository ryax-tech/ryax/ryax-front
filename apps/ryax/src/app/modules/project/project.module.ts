// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { SharedModule } from '../shared/shared.module';
import { HasProjectsComponent } from './components/has-projects/has-projects.component';
import { ProjectNameComponent } from './components/project-name/project-name.component';
import { ProjectUrlComponent } from './components/project-url/project-url.component';
import { ProjectComponent } from './components/project/project.component';
import { ProjectApiService } from './services/project-api/project-api.service';
import { ProjectEffects, VariableEffects } from './state/effects';
import {
  ProjectFeatureKey,
  ProjectReducerProvider,
  ProjectReducerToken,
} from './state/reducers';
import { ProjectSettingsComponent } from './components/project-settings/project-settings.component';
import { UserApiAuthModule } from '../user-api-auth/user-api-auth.module';
import { VariablesListComponent } from './components/variables-list/variables-list.component';

@NgModule({
  declarations: [
    ProjectComponent,
    HasProjectsComponent,
    ProjectNameComponent,
    ProjectUrlComponent,
    ProjectSettingsComponent,
    VariablesListComponent,
  ],
  imports: [
    CommonModule,
    RouterModule,
    StoreModule.forFeature(ProjectFeatureKey, ProjectReducerToken),
    EffectsModule.forFeature([ProjectEffects, VariableEffects]),
    SharedModule,
    UserApiAuthModule,
  ],
  providers: [ProjectReducerProvider, ProjectApiService],
  exports: [HasProjectsComponent, ProjectNameComponent, ProjectUrlComponent],
})
export class ProjectModule {}

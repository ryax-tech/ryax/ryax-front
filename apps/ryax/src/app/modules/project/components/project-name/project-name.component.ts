import { Component } from '@angular/core';
import { ProjectFacade } from '../../state/facade';

@Component({
  selector: 'ryax-project-name',
  templateUrl: './project-name.component.pug',
  styleUrls: ['./project-name.component.scss'],
})
export class ProjectNameComponent {
  public projectName$ = this.facade.projectName$;

  constructor(private facade: ProjectFacade) {}
}

// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { ProjectFacade } from '../../state/facade';

@Component({
  selector: 'ryax-has-projects',
  templateUrl: './has-projects.component.pug',
})
export class HasProjectsComponent implements OnInit {
  public projects$ = this.projectFacade.projects$;

  constructor(private projectFacade: ProjectFacade) {}

  public ngOnInit() {
    this.projectFacade.getList();
  }

  public hasProjects(): Observable<boolean> {
    return this.projects$.pipe(
      map((projects) => {
        return projects.length >= 2;
      })
    );
  }
}

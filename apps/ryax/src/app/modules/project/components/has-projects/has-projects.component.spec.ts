// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { cold } from 'jest-marbles';
import { of } from 'rxjs';
import { Project } from '../../entities/index';
import { ProjectFacade } from '../../state/facade/index';

import { HasProjectsComponent } from './has-projects.component';

describe('HasProjectsComponent', () => {
  let component: HasProjectsComponent;
  let fixture: ComponentFixture<HasProjectsComponent>;
  const projectFacadeSpy = {
    getList: jest.fn(),
    projects$: of([]),
  };
  const mockProjectList: Project[] = [
    {
      name: 'Test',
      creationDate: new Date(),
      current: true,
      id: 'test1',
    },
    {
      name: 'Test 2',
      creationDate: new Date(),
      id: 'test2',
    },
  ];

  beforeEach(async () => {
    projectFacadeSpy.getList.mockClear();

    await TestBed.configureTestingModule({
      declarations: [HasProjectsComponent],
      providers: [{ provide: ProjectFacade, useValue: projectFacadeSpy }],
    }).compileComponents();
  });

  describe('Common cases', () => {
    beforeEach(() => {
      fixture = TestBed.createComponent(HasProjectsComponent);
      component = fixture.componentInstance;
      fixture.detectChanges();
    });

    it('should create', () => {
      expect(component).toBeTruthy();
    });

    it('should get list on init', () => {
      expect(projectFacadeSpy.getList).toHaveBeenCalled();
    });
  });

  describe('When user has 2 projects or more', () => {
    beforeEach(() => {
      TestBed.overrideProvider(ProjectFacade, {
        useValue: {
          getList: jest.fn(),
          projects$: of(mockProjectList),
        },
      }).compileComponents();

      fixture = TestBed.createComponent(HasProjectsComponent);
      component = fixture.componentInstance;
      fixture.detectChanges();
    });

    it('should return true', () => {
      expect(component.hasProjects()).toBeObservable(cold('(a|)', { a: true }));
    });
  });

  describe('When user has 1 projects or less', () => {
    beforeEach(() => {
      TestBed.overrideProvider(ProjectFacade, {
        useValue: {
          getList: jest.fn(),
          projects$: of([mockProjectList[0]]),
        },
      });

      fixture = TestBed.createComponent(HasProjectsComponent);
      component = fixture.componentInstance;
      fixture.detectChanges();
    });

    it('should return false ', () => {
      expect(component.hasProjects()).toBeObservable(
        cold('(a|)', { a: false })
      );
    });
  });
});

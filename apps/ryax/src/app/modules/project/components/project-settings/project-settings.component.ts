import { Component, OnInit } from '@angular/core';
import { ProjectFacade } from '../../state/facade';
import { first } from 'rxjs/operators';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'ryax-project-settings',
  templateUrl: './project-settings.component.pug',
  styleUrls: ['./project-settings.component.scss'],
})
export class ProjectSettingsComponent implements OnInit {
  public projectId$ = this.projectFacade.projectId$;
  public projectName$ = this.projectFacade.projectName$;
  public loading$ = this.projectFacade.loading$;
  validateForm: FormGroup;

  constructor(private projectFacade: ProjectFacade, private fb: FormBuilder) {
    this.validateForm = this.fb.group({
      name: ['', [Validators.required, Validators.maxLength(32)]],
    });
  }

  ngOnInit(): void {
    this.setName();
  }

  async submitForm() {
    const projectId = await this.projectId$.pipe(first()).toPromise();
    if (projectId) {
      this.projectFacade.editName(projectId, this.validateForm.value.name);
      this.projectFacade.getList();
    }
  }

  cancel(): void {
    this.setName();
  }

  setName(): void {
    this.projectName$.subscribe((value) =>
      this.validateForm.patchValue({ name: value })
    );
  }
}

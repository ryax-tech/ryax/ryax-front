import { Component } from '@angular/core';
import { NzNotificationService } from 'ng-zorro-antd/notification';
import { ProjectFacade } from '../../state/facade/index';

@Component({
  selector: 'ryax-project-url',
  templateUrl: './project-url.component.pug',
  styleUrls: ['./project-url.component.scss'],
})
export class ProjectUrlComponent {
  public projectId$ = this.facade.projectId$;
  public baseUrl = window.location.origin + '/user-api/';

  constructor(
    private facade: ProjectFacade,
    private notification: NzNotificationService
  ) {}

  public copied(success: boolean) {
    if (success) {
      this.notification.create('success', 'Copied to clipboard!', '');
    } else {
      this.notification.create(
        'error',
        'Uh oh!',
        'Copy did not work as expected.'
      );
    }
  }
}

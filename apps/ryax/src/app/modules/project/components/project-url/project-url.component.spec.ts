import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjectUrlComponent } from './project-url.component';

describe('ProjectUrlComponent', () => {
  let component: ProjectUrlComponent;
  let fixture: ComponentFixture<ProjectUrlComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ProjectUrlComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectUrlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

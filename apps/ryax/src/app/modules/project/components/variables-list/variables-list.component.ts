import { Component, OnInit } from '@angular/core';
import { Variable } from '../../entities/variable';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { VariablesFacade } from '../../state/facade/variables.facade';

@Component({
  selector: 'ryax-variables-list',
  templateUrl: './variables-list.component.pug',
  styleUrls: ['./variables-list.component.scss'],
})
export class VariablesListComponent implements OnInit {
  variableList$ = this.facade.variables$;
  loading$ = this.facade.loading$;

  form: FormGroup = this.fb.group({});
  openEditModal = false;
  editedVariable: Variable | null = null;
  passwordVisible = false;
  isCreation = true;

  outputTypes: string[] = [
    'string',
    'integer',
    'password',
    'longstring',
    'float',
    'boolean',
    'file',
    'directory',
  ]; //, 'enum']

  constructor(private fb: FormBuilder, private facade: VariablesFacade) {}

  ngOnInit(): void {
    this.facade.getVariables();
  }

  noSpaceOrSpecialCharsValidator = (
    control: FormControl
  ): { [s: string]: boolean } => {
    const nameRegexp = /[!@#$%^&*()+\-=[\]{};':"\\|,.<>/?]/;
    if (control.value && nameRegexp.test(control.value)) {
      return { invalidName: true };
    }
    if ((control.value as string).indexOf(' ') >= 0) {
      return { cannotContainSpace: true };
    }
    return {};
  };

  editOrAddVariable(toEditVariable: Variable | null) {
    this.openEditModal = true;
    if (toEditVariable) {
      this.editedVariable = toEditVariable;
      this.isCreation = false;
    } else {
      this.editedVariable = {
        id: null,
        name: '',
        value: null,
        type: 'string',
        description: '',
      };
      this.isCreation = true;
    }

    this.form = this.fb.group({
      id: [this.editedVariable.id, []],
      name: [
        this.editedVariable.name,
        [Validators.required, this.noSpaceOrSpecialCharsValidator],
      ],
      description: [this.editedVariable.description, []],
      type: [this.editedVariable.type, [Validators.nullValidator]],
      // TODO: Validate properly the value depending on the type
      value: [this.editedVariable.value, [Validators.nullValidator]],
    });

    this.form.get('type')?.valueChanges.subscribe(() => {
      // reset value on type change
      if (this.form) {
        this.form.get('value')?.patchValue('');
      }
    });
  }

  public handleCancel() {
    this.openEditModal = false;
  }

  public handleOk() {
    if (!this.form?.valid) {
      return;
    }
    const variable = this.form.value as Variable;
    if (variable.id) {
      // This variable already exists this is an edit
      this.facade.editVariable(variable);
    } else {
      // No id set. This is a variable creation
      this.facade.addVariable(variable);
    }
    this.openEditModal = false;
  }

  getType() {
    if (!this.form) {
      return null;
    }
    return this.form.get('type')?.value;
  }

  deleteVariable(variable: Variable) {
    if (variable.id) {
      this.facade.deleteVariable(variable.id);
    }
  }
}

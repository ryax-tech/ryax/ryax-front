import { VariableDto } from '../dtos/variable.dto';
import { RyaxIOType, Variable } from '../variable';

export class VariableAdapter {
  public adapt(dtos: VariableDto[]): Variable[] {
    return dtos.map((dto) => this.adaptOne(dto));
  }

  public adaptOne(dto: VariableDto): Variable {
    return {
      id: dto.id === undefined ? null : dto.id,
      name: dto.name,
      description: dto.description,
      value: dto.value,
      type: dto.type as RyaxIOType,
    };
  }
}

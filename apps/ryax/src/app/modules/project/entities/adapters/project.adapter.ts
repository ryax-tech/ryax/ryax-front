// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { ProjectDto } from '../dtos/index';
import { Project } from '../project';

export class ProjectAdapter {
  public adapt(dto: ProjectDto): Project {
    return {
      id: dto.id,
      name: dto.name,
      creationDate: dto.creation_date,
      current: dto.current,
    };
  }
}

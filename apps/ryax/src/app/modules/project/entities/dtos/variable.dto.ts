export interface VariableDto {
  id: string;
  name: string;
  description: string;
  type: string;
  value: unknown;
}

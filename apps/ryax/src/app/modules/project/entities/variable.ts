// TODO: use a shared object for that
export type RyaxIOType =
  | 'string'
  | 'longstring'
  | 'integer'
  | 'float'
  | 'password'
  | 'enum'
  | 'file'
  | 'directory'
  | 'boolean'
  | 'table';

export interface Variable {
  id: string | null;
  name: string;
  description: string;
  type: RyaxIOType;
  value: unknown;
}

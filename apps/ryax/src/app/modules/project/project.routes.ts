// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { ProjectComponent } from './components/project/project.component';
import { ProjectSettingsComponent } from './components/project-settings/project-settings.component';

export const projectRoutes = [
  {
    path: 'projects',
    component: ProjectComponent,
  },
  {
    path: 'settings',
    component: ProjectSettingsComponent,
  },
];

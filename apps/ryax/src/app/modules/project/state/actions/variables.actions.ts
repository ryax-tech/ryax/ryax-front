import { HttpErrorResponse } from '@angular/common/http';
import { createAction } from '@ngrx/store';
import { Variable } from '../../entities/variable';

export const getVariables = createAction('[Variables] Get variables list');

export const getVariablesSuccess = createAction(
  '[Variables] Get variables success',
  (variables: Variable[]) => ({ variables })
);

export const getVariablesError = createAction(
  '[Variables] Get variables error',
  (error: HttpErrorResponse) => ({ error })
);

export const addVariable = createAction(
  '[Variables] Add a new variable',
  (variable: Variable) => ({ variable })
);

export const deleteVariable = createAction(
  '[Variables] Delete a variable',
  (variableId: string) => ({ variableId })
);

export const deleteVariablesError = createAction(
  '[Variables] Delete variable Error',
  (err: HttpErrorResponse) => ({
    message: err.error.error,
    workflows: err.error.workflows,
  })
);

export const editVariable = createAction(
  '[Variables] Edit a variable',
  (variable: Variable) => ({ variable })
);

export const editVariablesError = createAction(
  '[Variables] Edit variable Error',
  (err: HttpErrorResponse) => ({
    message: err.error.error,
    workflows: err.error.workflows,
  })
);

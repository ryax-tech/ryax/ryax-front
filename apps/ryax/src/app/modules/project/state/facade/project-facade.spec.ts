// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { TestBed } from '@angular/core/testing';
import { Store } from '@ngrx/store';
import { ProjectFacade } from './project-facade.service';

describe('ProjectFacade', () => {
  let service: ProjectFacade;
  const storeSpy = {
    dispatch: jest.fn(),
    select: jest.fn(),
  };

  beforeEach(() => {
    storeSpy.dispatch.mockClear();

    TestBed.configureTestingModule({
      providers: [ProjectFacade, { provide: Store, useValue: storeSpy }],
    });
    service = TestBed.inject(ProjectFacade);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should dispatch get project list', () => {
    service.getList();
    expect(storeSpy.dispatch).toHaveBeenCalledWith({
      type: '[Project] Get Project list',
    });
  });

  it('should dispatch set project', () => {
    service.activateProject('id');
    expect(storeSpy.dispatch).toHaveBeenCalledWith({
      type: '[Project] Activate Project',
      projectId: 'id',
    });
  });
});

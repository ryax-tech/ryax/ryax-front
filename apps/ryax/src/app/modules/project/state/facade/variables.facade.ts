// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { VariablesActions } from '../actions';
import { VariablesState } from '../reducers';
import { Variable } from '../../entities/variable';
import {
  selectVariablesList,
  selectVariablesListLoading,
} from '../selectors/variables.selectors';

@Injectable({
  providedIn: 'root',
})
export class VariablesFacade {
  public variables$ = this.store.select(selectVariablesList);
  public loading$ = this.store.select(selectVariablesListLoading);

  constructor(private readonly store: Store<VariablesState>) {}

  public getVariables() {
    this.store.dispatch(VariablesActions.getVariables());
  }

  public addVariable(variable: Variable) {
    this.store.dispatch(VariablesActions.addVariable(variable));
  }

  public editVariable(variable: Variable) {
    this.store.dispatch(VariablesActions.editVariable(variable));
  }

  public deleteVariable(variableId: string) {
    this.store.dispatch(VariablesActions.deleteVariable(variableId));
  }
}

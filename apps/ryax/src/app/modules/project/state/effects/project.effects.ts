// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { NzNotificationService } from 'ng-zorro-antd/notification';
import { of } from 'rxjs';
import { catchError, map, switchMap, tap } from 'rxjs/operators';
import { Project } from '../../entities';
import { ProjectApiService } from '../../services/project-api/project-api.service';
import { ProjectActions } from '../actions';

@Injectable()
export class ProjectEffects {
  projects$ = createEffect(() =>
    this.actions$.pipe(
      ofType(ProjectActions.getList),
      switchMap(() =>
        this.projectApiService.getProjects().pipe(
          map((projects: Project[]) => ProjectActions.loadSuccess(projects)),
          catchError((err: HttpErrorResponse) => {
            return of(ProjectActions.loadError(err));
          })
        )
      )
    )
  );

  setProject$ = createEffect(() =>
    this.actions$.pipe(
      ofType(ProjectActions.activate),
      switchMap((action) =>
        this.projectApiService.setProject(action.projectId).pipe(
          map(() => action.projectId),
          map((projectId) => ProjectActions.setSuccess(projectId)),
          catchError((err: HttpErrorResponse) => {
            return of(ProjectActions.setError(err));
          })
        )
      )
    )
  );

  updateProjectName$ = createEffect(() =>
    this.actions$.pipe(
      ofType(ProjectActions.editName),
      switchMap(({ projectId, name }) =>
        this.projectApiService.editName(name, projectId).pipe(
          map(() => {
            return ProjectActions.editNameSuccess();
          }),
          catchError((err: HttpErrorResponse) => {
            return of(ProjectActions.editNameError(err));
          })
        )
      )
    )
  );

  displayErrors$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(ProjectActions.loadError, ProjectActions.setError),
        tap((errAction) =>
          this.notification.create(
            'error',
            'Something went wrong ! http error : ' + errAction.error.status,
            errAction.error.message
          )
        )
      ),
    { dispatch: false }
  );

  constructor(
    private actions$: Actions,
    private projectApiService: ProjectApiService,
    private notification: NzNotificationService
  ) {}
}

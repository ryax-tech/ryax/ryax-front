// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { HttpErrorResponse } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { Actions } from '@ngrx/effects';
import { provideMockActions } from '@ngrx/effects/testing';
import { provideMockStore } from '@ngrx/store/testing';
import { cold, hot } from 'jest-marbles';
import { NzNotificationService } from 'ng-zorro-antd/notification';
import { Observable } from 'rxjs';
import { ProjectApiService } from '../../services/project-api/project-api.service';
import { ProjectEffects } from './project.effects';
import { ProjectActions } from '../actions';

describe('ProjectEffects', () => {
  let effects: ProjectEffects;
  let actions$: Observable<Actions>;
  const projectApiServiceSpy = {
    getProjects: jest.fn(),
    setProject: jest.fn(),
  };

  const notifSpy = {
    create: jest.fn(),
  };

  beforeEach(() => {
    projectApiServiceSpy.getProjects.mockClear();

    TestBed.configureTestingModule({
      providers: [
        ProjectEffects,
        provideMockStore(),
        provideMockActions(() => actions$),
        { provide: ProjectApiService, useValue: projectApiServiceSpy },
        { provide: NzNotificationService, useValue: notifSpy },
      ],
    });
    effects = TestBed.inject(ProjectEffects);
  });

  it('should be created', () => {
    expect(effects).toBeTruthy();
  });

  it('should try to get project list and return success when a list is received', () => {
    projectApiServiceSpy.getProjects.mockImplementation(() =>
      cold('-a|', { a: [] })
    );

    actions$ = hot('-a', {
      a: ProjectActions.getList(),
    });

    const expected$ = hot('--a', { a: ProjectActions.loadSuccess([]) });

    expect(effects.projects$).toBeObservable(expected$);
    expect(expected$).toSatisfyOnFlush(() => {
      expect(projectApiServiceSpy.getProjects).toHaveBeenCalled();
    });
  });

  it('should try to get project list and return error when it fails', () => {
    projectApiServiceSpy.getProjects.mockImplementation(() =>
      cold('-#', {}, { error: '400' })
    );

    actions$ = hot('-a', {
      a: ProjectActions.getList(),
    });

    const expected$ = hot('--a', {
      a: ProjectActions.loadError({ error: '400' } as HttpErrorResponse),
    });

    expect(effects.projects$).toBeObservable(expected$);
    expect(expected$).toSatisfyOnFlush(() => {
      expect(projectApiServiceSpy.getProjects).toHaveBeenCalled();
    });
  });

  it('should try to set project list and return success when it worked', () => {
    projectApiServiceSpy.setProject.mockImplementation(() =>
      cold('-a|', { a: null })
    );

    actions$ = hot('-a', {
      a: ProjectActions.activate('id'),
    });

    const expected$ = hot('--a', { a: ProjectActions.setSuccess('id') });

    expect(effects.setProject$).toBeObservable(expected$);
    expect(expected$).toSatisfyOnFlush(() => {
      expect(projectApiServiceSpy.setProject).toHaveBeenCalledWith('id');
    });
  });

  it('should try to set project list and return error when it fails', () => {
    projectApiServiceSpy.setProject.mockImplementation(() =>
      cold('-#', {}, { error: '400' })
    );

    actions$ = hot('-a', {
      a: ProjectActions.activate('id'),
    });

    const expected$ = hot('--a', {
      a: ProjectActions.setError({ error: '400' } as HttpErrorResponse),
    });

    expect(effects.setProject$).toBeObservable(expected$);
    expect(expected$).toSatisfyOnFlush(() => {
      expect(projectApiServiceSpy.setProject).toHaveBeenCalledWith('id');
    });
  });
});

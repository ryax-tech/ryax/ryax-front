import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { catchError, map, switchMap, tap } from 'rxjs/operators';
import { HttpErrorResponse } from '@angular/common/http';
import { of } from 'rxjs';
import { NzNotificationService } from 'ng-zorro-antd/notification';
import { Variable } from '../../entities/variable';
import { VariablesActions } from '../actions';
import { VariablesApiService } from '../../services/variables.service';
import { Router } from '@angular/router';

@Injectable()
export class VariableEffects {
  variables$ = createEffect(() =>
    this.actions$.pipe(
      ofType(VariablesActions.getVariables),
      switchMap(() =>
        this.variablesApiService.getVariables().pipe(
          map((variables: Variable[]) =>
            VariablesActions.getVariablesSuccess(variables)
          ),
          catchError((err: HttpErrorResponse) => {
            return of(VariablesActions.getVariablesError(err));
          })
        )
      )
    )
  );

  addVariable$ = createEffect(() =>
    this.actions$.pipe(
      ofType(VariablesActions.addVariable),
      switchMap(({ variable }) => {
        let apiCall;
        if (variable.value instanceof File) {
          apiCall = this.variablesApiService.addFileVariable(variable);
        } else {
          apiCall = this.variablesApiService.addVariable(variable);
        }
        return apiCall.pipe(
          map(() => VariablesActions.getVariables()),
          catchError((err: HttpErrorResponse) => {
            return of(VariablesActions.getVariablesError(err));
          })
        );
      })
    )
  );

  editVariables$ = createEffect(() =>
    this.actions$.pipe(
      ofType(VariablesActions.editVariable),
      switchMap(({ variable }) => {
        let apiCall;
        if (variable.value instanceof File) {
          apiCall = this.variablesApiService.editFileVariable(variable);
        } else {
          apiCall = this.variablesApiService.editVariable(variable);
        }
        return apiCall.pipe(
          map(() => VariablesActions.getVariables()),
          catchError((err: HttpErrorResponse) => {
            return of(VariablesActions.editVariablesError(err));
          })
        );
      })
    )
  );

  deleteVariables$ = createEffect(() =>
    this.actions$.pipe(
      ofType(VariablesActions.deleteVariable),
      switchMap(({ variableId }) =>
        this.variablesApiService.deleteVariable(variableId).pipe(
          map(() => VariablesActions.getVariables()),
          catchError((err: HttpErrorResponse) => {
            return of(VariablesActions.deleteVariablesError(err));
          })
        )
      )
    )
  );

  deleteVariableErrors$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(
          VariablesActions.deleteVariablesError,
          VariablesActions.editVariablesError
        ),
        tap(({ workflows, message }) =>
          this.notification.create(
            'error',
            'Unable to delete variable: ' + message,
            'The variable is currently in use in these workflows: ' +
              workflows.map(
                (workflow: { id: string; name: string }) =>
                  `${workflow.name} (${workflow.id})`
              )
          )
        )
      ),
    { dispatch: false }
  );

  displayErrors$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(VariablesActions.getVariablesError),
        tap((errAction) =>
          this.notification.create(
            'error',
            'Something went wrong ! http error : ' + errAction.error.status,
            errAction.error.message + ': ' + errAction.error.error.error
          )
        )
      ),
    { dispatch: false }
  );

  constructor(
    private actions$: Actions,
    private notification: NzNotificationService,
    private variablesApiService: VariablesApiService,
    private router: Router
  ) {}
}

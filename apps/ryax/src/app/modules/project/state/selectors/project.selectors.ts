// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { createFeatureSelector, createSelector } from '@ngrx/store';
import { ProjectFeatureKey, ProjectListState, ProjectState } from '../reducers';

const selectProjectFn = (state: ProjectState) => state.project;
const selectCurrentProjectIdFn = (state: ProjectListState) =>
  state.list.find((project) => project.current)?.id;
const selectCurrentProjectNameFn = (state: ProjectListState) =>
  state.list.find((project) => project.current)?.name;
const selectProjectListErrorFn = (state: ProjectListState) => state.error;
const selectProjectListLoadingFn = (state: ProjectListState) => state.loading;
const selectProjectListFn = (state: ProjectListState) => state.list;

export const selectProjectState =
  createFeatureSelector<ProjectState>(ProjectFeatureKey);
export const selectProject = createSelector(
  selectProjectState,
  selectProjectFn
);
export const selectCurrentProjectId = createSelector(
  selectProject,
  selectCurrentProjectIdFn
);
export const selectCurrentProjectName = createSelector(
  selectProject,
  selectCurrentProjectNameFn
);
export const selectProjectListError = createSelector(
  selectProject,
  selectProjectListErrorFn
);
export const selectProjectListLoading = createSelector(
  selectProject,
  selectProjectListLoadingFn
);
export const selectProjectList = createSelector(
  selectProject,
  selectProjectListFn
);

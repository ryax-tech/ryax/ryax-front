import { ProjectFeatureKey, ProjectState, VariablesState } from '../reducers';
import { createFeatureSelector, createSelector } from '@ngrx/store';

const selectVariablesFn = (state: ProjectState) => state.variable;
const selectVariablesLoadingFn = (state: VariablesState) => state.loading;
const selectVariablesListFn = (state: VariablesState) => state.data;

export const selectProjectState =
  createFeatureSelector<ProjectState>(ProjectFeatureKey);
export const selectVariables = createSelector(
  selectProjectState,
  selectVariablesFn
);
export const selectVariablesListLoading = createSelector(
  selectVariables,
  selectVariablesLoadingFn
);
export const selectVariablesList = createSelector(
  selectVariables,
  selectVariablesListFn
);

import { HttpErrorResponse } from '@angular/common/http';
import { createReducer, on } from '@ngrx/store';
import { Variable } from '../../entities/variable';
import { VariablesActions } from '../../state/actions';

export interface VariablesState {
  data: Variable[];
  loading: boolean;
  error: HttpErrorResponse | null;
}

export const initialVariablesState: VariablesState = {
  data: [],
  loading: false,
  error: null,
};

export const variablesReducer = createReducer<VariablesState>(
  initialVariablesState,
  on(VariablesActions.getVariables, (state) => ({
    ...state,
    loading: true,
    error: null,
  })),
  on(VariablesActions.getVariablesSuccess, (state, { variables }) => ({
    ...state,
    data: variables,
    loading: false,
    error: null,
  })),
  on(VariablesActions.getVariablesError, (state, { error }) => ({
    ...state,
    loading: false,
    error,
  }))
);

// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { HttpErrorResponse } from '@angular/common/http';
import { createReducer, on } from '@ngrx/store';
import { Project } from '../../entities/index';
import { ProjectActions } from '../actions';

export interface ProjectListState {
  list: Project[];
  loading: boolean;
  error: HttpErrorResponse | null;
}

export const initialProjectState: ProjectListState = {
  list: [],
  loading: false,
  error: null,
};

export const projectReducer = createReducer<ProjectListState>(
  initialProjectState,
  on(ProjectActions.getList, ProjectActions.activate, (state) => ({
    ...state,
    loading: true,
    error: null,
  })),
  on(ProjectActions.loadSuccess, (state, { projects }) => ({
    ...state,
    list: projects,
    loading: false,
    error: null,
  })),
  on(ProjectActions.loadError, ProjectActions.setError, (state, { error }) => ({
    ...state,
    loading: false,
    error,
  })),
  on(ProjectActions.setSuccess, (state, { projectId }) => ({
    ...state,
    list: state.list.map((project) => {
      return { ...project, current: project.id === projectId };
    }),
    loading: false,
    error: null,
  })),
  on(ProjectActions.editName, (state) => ({
    ...state,
    loading: true,
    error: null,
  })),
  on(ProjectActions.editNameSuccess, (state) => ({
    ...state,
    loading: false,
    error: null,
  })),
  on(ProjectActions.editNameError, (state, { error }) => ({
    ...state,
    loading: false,
    error,
  }))
);

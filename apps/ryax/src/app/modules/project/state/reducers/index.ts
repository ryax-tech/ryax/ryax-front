// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { InjectionToken } from '@angular/core';
import { ActionReducerMap } from '@ngrx/store';
import { ProjectListState, projectReducer } from './project.reducer';
import { variablesReducer, VariablesState } from './variables.reducer';

export * from './project.reducer';
export * from './variables.reducer';

export const ProjectFeatureKey = 'projectDomain';

export interface ProjectState {
  project: ProjectListState;
  variable: VariablesState;
}

export const ProjectReducerToken = new InjectionToken<
  ActionReducerMap<ProjectState>
>(ProjectFeatureKey);

export const ProjectReducerProvider = {
  provide: ProjectReducerToken,
  useValue: {
    project: projectReducer,
    variable: variablesReducer,
  },
};

// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ProjectModule } from './project.module';
import { projectRoutes } from './project.routes';

@NgModule({
  imports: [CommonModule, ProjectModule, RouterModule.forChild(projectRoutes)],
})
export class ProjectAccessModule {}

// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { AuthFacade } from '../../state/facade/index';

@Component({
  selector: 'ryax-is-logged',
  templateUrl: './is-logged.component.pug',
})
export class IsLoggedComponent {
  public token$ = this.authFacade.token$;

  constructor(private authFacade: AuthFacade) {}

  public isLogged(): Observable<boolean> {
    return this.token$.pipe(
      map((token) => {
        return !!token;
      })
    );
  }
}

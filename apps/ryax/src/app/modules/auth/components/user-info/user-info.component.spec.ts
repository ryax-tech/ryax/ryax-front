// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { AuthFacade } from '../../state/facade';

import { UserInfoComponent } from './user-info.component';

describe('UserInfoComponent', () => {
  let component: UserInfoComponent;
  let fixture: ComponentFixture<UserInfoComponent>;
  const authFacadeSpy = {
    getUser: jest.fn(),
    logout: jest.fn(),
  };

  beforeEach(async () => {
    authFacadeSpy.getUser.mockClear();
    authFacadeSpy.logout.mockClear();

    await TestBed.configureTestingModule({
      declarations: [UserInfoComponent],
      providers: [
        {
          provide: AuthFacade,
          useValue: authFacadeSpy,
        },
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UserInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call get user on init', () => {
    expect(authFacadeSpy.getUser).toHaveBeenCalled();
  });

  it('should logout on click', () => {
    component.onLogout();

    expect(authFacadeSpy.logout).toHaveBeenCalled();
  });
});

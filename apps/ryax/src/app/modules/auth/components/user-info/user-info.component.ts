// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component, OnInit } from '@angular/core';
import { AuthFacade } from '../../state/facade';

@Component({
  selector: 'ryax-user-info',
  templateUrl: './user-info.component.pug',
  styleUrls: ['./user-info.component.scss'],
})
export class UserInfoComponent implements OnInit {
  public remainingBudget = 100;
  public remainingBudgetPercentage = 50;
  public user$ = this.authFacade.user$;

  constructor(private authFacade: AuthFacade) {}

  public ngOnInit(): void {
    this.authFacade.getUser();
  }

  public onLogout(): void {
    this.authFacade.logout();
  }
}

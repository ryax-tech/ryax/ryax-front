// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { Credentials } from '../../entities';
import { AuthFacade } from '../../state/facade';
import { AuthLoginComponent } from './auth-login.component';

describe('AuthLoginComponent', () => {
  let component: AuthLoginComponent;
  let fixture: ComponentFixture<AuthLoginComponent>;
  let facade: AuthFacade;

  const authLoginFacadeSpy = {
    login: jest.fn(),
  };

  const credentials: Credentials = {
    username: 'username',
    password: 'password',
  };

  const badCredentials: Credentials = {
    username: 'username',
    password: '',
  };

  beforeEach(async () => {
    authLoginFacadeSpy.login.mockClear();

    await TestBed.configureTestingModule({
      imports: [ReactiveFormsModule],
      declarations: [AuthLoginComponent],
      providers: [{ provide: AuthFacade, useValue: authLoginFacadeSpy }],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AuthLoginComponent);
    facade = TestBed.inject(AuthFacade);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call facade on submit', () => {
    component.authForm.setValue(credentials);
    component.onSubmit();
    expect(facade.login).toHaveBeenCalledWith(credentials);
  });

  it('should not call facade if credentials are not provided', () => {
    component.authForm.setValue(badCredentials);
    component.onSubmit();
    expect(facade.login).not.toHaveBeenCalled();
  });
});

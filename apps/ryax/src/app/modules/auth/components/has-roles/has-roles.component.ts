// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component, Input } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Roles } from '../../entities/index';
import { AuthFacade } from '../../state/facade/index';

@Component({
  selector: 'ryax-has-roles',
  templateUrl: './has-roles.component.pug',
})
export class HasRolesComponent {
  public user$ = this.authFacade.user$;
  @Input() public roles: Roles[] = [];

  constructor(private authFacade: AuthFacade) {}

  public hasRoles(): Observable<boolean> {
    return this.user$.pipe(
      map((user) => {
        if (user) {
          return this.roles.findIndex((role) => role === user.role) >= 0;
        } else {
          return false;
        }
      })
    );
  }
}

// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { hot } from 'jest-marbles';
import { of } from 'rxjs';
import { Roles } from '../../entities/index';
import { AuthFacade } from '../../state/facade/index';

import { HasRolesComponent } from './has-roles.component';

describe('HasRolesComponent', () => {
  let component: HasRolesComponent;
  let fixture: ComponentFixture<HasRolesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [HasRolesComponent],
      providers: [
        { provide: AuthFacade, useValue: { user$: of({ role: Roles.USER }) } },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HasRolesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should show content when user roles are matching', () => {
    component.roles = [Roles.USER, Roles.ADMIN];

    expect(component.hasRoles()).toBeObservable(hot('(a|)', { a: true }));
  });

  it('should hide content when user roles are not matching', () => {
    component.roles = [Roles.DEVELOPER, Roles.ADMIN];

    expect(component.hasRoles()).toBeObservable(hot('(a|)', { a: false }));
  });
});

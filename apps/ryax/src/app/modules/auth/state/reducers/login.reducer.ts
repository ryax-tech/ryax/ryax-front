// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { HttpErrorResponse } from '@angular/common/http';
import { createReducer, on } from '@ngrx/store';
import { LoginActions } from '../actions';

export interface LoginState {
  token: string | null;
  loading: boolean;
  error: HttpErrorResponse | null;
}

export const initialAuthState: LoginState = {
  token: null,
  loading: false,
  error: null,
};

export const loginReducer = createReducer<LoginState>(
  initialAuthState,
  on(LoginActions.login, (state) => ({
    ...state,
    loading: true,
    error: null,
  })),
  on(
    LoginActions.loginSuccess,
    LoginActions.getTokenFromStorage,
    (state, { token }) => ({
      ...state,
      token,
      loading: false,
      error: null,
    })
  ),
  on(LoginActions.loginError, (state, { error }) => ({
    ...state,
    loading: false,
    error,
  })),
  on(LoginActions.logout, (state) => ({
    ...state,
    token: null,
  }))
);

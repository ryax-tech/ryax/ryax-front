// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { HttpErrorResponse } from '@angular/common/http';
import { createReducer, on } from '@ngrx/store';
import { Roles } from '../../entities/index';
import { User } from '../../entities/user';
import { UserActions } from '../actions';

export interface UserState {
  userDetails: User | null;
  loading: boolean;
  error: HttpErrorResponse | null;
}

export const initialUserState: UserState = {
  userDetails: { name: 'username', role: Roles.ANONYMOUS },
  loading: false,
  error: null,
};

export const userReducer = createReducer<UserState>(
  initialUserState,
  on(UserActions.get, (state) => ({
    ...state,
    loading: true,
    error: null,
  })),
  on(UserActions.loadSuccess, (state, { user }) => ({
    ...state,
    userDetails: user,
    loading: false,
    error: null,
  })),
  on(UserActions.loadError, (state, { error }) => ({
    ...state,
    loading: false,
    error,
  }))
);

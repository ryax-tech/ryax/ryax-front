// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { InjectionToken } from '@angular/core';
import { ActionReducerMap } from '@ngrx/store';
import { loginReducer, LoginState } from './login.reducer';
import { userReducer, UserState } from './user.reducer';

export * from './login.reducer';
export * from './user.reducer';

export const AuthFeatureKey = 'authDomain';

export interface AuthState {
  login: LoginState;
  user: UserState;
}

export const AuthReducerToken = new InjectionToken<ActionReducerMap<AuthState>>(
  AuthFeatureKey
);

export const AuthReducerProvider = {
  provide: AuthReducerToken,
  useValue: {
    login: loginReducer,
    user: userReducer,
  },
};

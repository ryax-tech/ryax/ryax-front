// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { createFeatureSelector, createSelector } from '@ngrx/store';
import { AuthFeatureKey, AuthState, UserState } from '../reducers';

const selectUserFn = (state: AuthState) => state.user;
const selectUserErrorFn = (state: UserState) => state.error;
const selectUserLoadingFn = (state: UserState) => state.loading;
const selectUserDetailsFn = (state: UserState) => state.userDetails;

export const selectAuthUserState =
  createFeatureSelector<AuthState>(AuthFeatureKey);
export const selectUser = createSelector(selectAuthUserState, selectUserFn);
export const selectUserError = createSelector(selectUser, selectUserErrorFn);
export const selectUserLoading = createSelector(
  selectUser,
  selectUserLoadingFn
);
export const selectUserDetails = createSelector(
  selectUser,
  selectUserDetailsFn
);

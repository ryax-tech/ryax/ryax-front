// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { createFeatureSelector, createSelector } from '@ngrx/store';
import { AuthFeatureKey, AuthState, LoginState } from '../reducers';

const selectLoginFn = (state: AuthState) => state.login;
const selectLoginErrorFn = (state: LoginState) => state.error;
const selectLoginLoadingFn = (state: LoginState) => state.loading;
const selectLoginJWTFn = (state: LoginState) => state.token;

export const selectAuthLoginState =
  createFeatureSelector<AuthState>(AuthFeatureKey);
export const selectLoginState = createSelector(
  selectAuthLoginState,
  selectLoginFn
);
export const selectLoginError = createSelector(
  selectLoginState,
  selectLoginErrorFn
);
export const selectLoginLoading = createSelector(
  selectLoginState,
  selectLoginLoadingFn
);
export const selectLoginJWT = createSelector(
  selectLoginState,
  selectLoginJWTFn
);

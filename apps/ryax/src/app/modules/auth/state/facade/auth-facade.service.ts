// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { Credentials } from '../../entities';
import { LoginActions, UserActions } from '../actions';
import { AuthState } from '../reducers';
import {
  selectLoginError,
  selectLoginJWT,
  selectLoginLoading,
} from '../selectors';
import { selectUserDetails } from '../selectors/user.selectors';

@Injectable({
  providedIn: 'root',
})
export class AuthFacade {
  public token$ = this.store.select(selectLoginJWT);
  public error$ = this.store.select(selectLoginError);
  public loading$ = this.store.select(selectLoginLoading);
  public user$ = this.store.select(selectUserDetails);

  constructor(private readonly store: Store<AuthState>) {}

  public login(credentials: Credentials) {
    this.store.dispatch(LoginActions.login(credentials));
  }

  public logout() {
    this.store.dispatch(LoginActions.logout());
  }

  public getUser() {
    this.store.dispatch(UserActions.get());
  }
}

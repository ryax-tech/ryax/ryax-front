// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { TestBed } from '@angular/core/testing';
import { Store } from '@ngrx/store';
import { Credentials } from '../../entities';

import { AuthFacade } from './index';

describe('AuthFacade', () => {
  let service: AuthFacade;
  const storeSpy = {
    dispatch: jest.fn(),
    select: jest.fn(),
  };
  const credentials: Credentials = {
    username: 'test',
    password: 'pass',
  };

  beforeEach(() => {
    storeSpy.dispatch.mockClear();

    TestBed.configureTestingModule({
      providers: [AuthFacade, { provide: Store, useValue: storeSpy }],
    });
    service = TestBed.inject(AuthFacade);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should dispatch login action with credentials', () => {
    service.login(credentials);
    expect(storeSpy.dispatch).toHaveBeenCalledWith({
      type: '[Auth Login] Send credentials',
      ...credentials,
    });
  });

  it('should dispatch logout action', () => {
    service.logout();
    expect(storeSpy.dispatch).toHaveBeenCalledWith({
      type: '[Auth Login] Logout',
    });
  });

  it('should dispatch getUser action', () => {
    service.getUser();
    expect(storeSpy.dispatch).toHaveBeenCalledWith({
      type: '[User] Get User details',
    });
  });
});

// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { HttpErrorResponse } from '@angular/common/http';
import { createAction } from '@ngrx/store';
import { User } from '../../entities/user';

export const get = createAction('[User] Get User details');

export const loadSuccess = createAction(
  '[User] Load User details success',
  (user: User) => ({ user })
);

export const loadError = createAction(
  '[User] Load User details error',
  (error: HttpErrorResponse) => ({ error })
);

// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { HTTP_INTERCEPTORS, HttpClient } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';
import { of } from 'rxjs';
import { TokenInjectorService } from './token-injector.service';
import { AuthFacade } from '../../state/facade/index';

describe('TokenInjectorService', () => {
  let httpMock: HttpTestingController;
  let http: HttpClient;
  const authFacadeSpy = {
    token$: of<string | null>(),
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        { provide: AuthFacade, useValue: authFacadeSpy },
        {
          provide: HTTP_INTERCEPTORS,
          useClass: TokenInjectorService,
          multi: true,
        },
      ],
    });
    httpMock = TestBed.inject(HttpTestingController);
    http = TestBed.inject(HttpClient);
  });

  it('should add an Authorization header if it exists', () => {
    authFacadeSpy.token$ = of<string>('TOKEN');

    http.get(`an-url`).subscribe((response) => {
      expect(response).toBeTruthy();
    });

    const httpRequest = httpMock.expectOne(`an-url`);

    expect(httpRequest.request.headers.has('Authorization')).toEqual(true);
    expect(httpRequest.request.headers.get('Authorization')).toBe('TOKEN');
  });

  it('should do nothing if no token exists', () => {
    authFacadeSpy.token$ = of(null);

    http.get(`an-url`).subscribe((response) => {
      expect(response).toBeTruthy();
    });

    const httpRequest = httpMock.expectOne(`an-url`);

    expect(httpRequest.request.headers.has('Authorization')).toEqual(false);
  });
});

// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { UserAdapter } from '../../entities/adapters';
import { UserDto } from '../../entities/dtos';
import { User } from '../../entities';

@Injectable()
export class UserApiService {
  private baseUrl = '/api/authorization';

  constructor(private http: HttpClient) {}

  public getUser(): Observable<User> {
    return this.http
      .get<UserDto>(this.baseUrl + '/me')
      .pipe(map((dto) => new UserAdapter().adaptUser(dto)));
  }
}

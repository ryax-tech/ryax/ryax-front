// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { AccountToken, Credentials } from '../../entities';
import { TokenAdapter } from '../../entities/adapters';
import { LoginResponseDto } from '../../entities/dtos';

@Injectable()
export class AuthLoginApiService {
  private baseUrl = '/api/authorization';

  constructor(private http: HttpClient) {}

  public login(credentials: Credentials): Observable<AccountToken> {
    return this.http
      .post<LoginResponseDto>(this.baseUrl + '/login', credentials)
      .pipe(map((dto) => new TokenAdapter().adaptLoginResponse(dto)));
  }
}

// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { hot } from 'jest-marbles';
import { of } from 'rxjs';
import { AuthFacade } from '../../state/facade';

import { AlreadyLoggedGuard } from './already-logged.guard';

describe('AlreadyLoggedGuard', () => {
  let guard: AlreadyLoggedGuard;
  const authLoginFacadeSpy = {
    token$: of(null),
  };
  const routerSpy = {
    parseUrl: jest.fn().mockImplementation((value) => [value]),
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        { provide: AuthFacade, useValue: authLoginFacadeSpy },
        { provide: Router, useValue: routerSpy },
      ],
    });
  });

  describe('When token is defined', () => {
    beforeEach(() => {
      TestBed.overrideProvider(AuthFacade, {
        useValue: {
          token$: hot('a', { a: 'MyToken' }),
        },
      });
      guard = TestBed.inject(AlreadyLoggedGuard);
    });

    it('should redirect to /login', () => {
      expect(guard.canActivate()).toBeObservable(
        hot('a', { a: ['/dashboard'] })
      );
    });
  });

  describe('When token is NOT defined', () => {
    beforeEach(() => {
      TestBed.overrideProvider(AuthFacade, {
        useValue: {
          token$: hot('a', { a: null }),
        },
      });
      guard = TestBed.inject(AlreadyLoggedGuard);
    });

    it('should allow to access route', () => {
      expect(guard.canActivate()).toBeObservable(hot('a', { a: true }));
    });
  });
});

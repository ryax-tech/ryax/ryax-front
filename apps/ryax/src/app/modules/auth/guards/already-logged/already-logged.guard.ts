// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Injectable } from '@angular/core';
import { Router, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { AuthFacade } from '../../state/facade';

@Injectable({
  providedIn: 'root',
})
export class AlreadyLoggedGuard {
  constructor(private authFacade: AuthFacade, private router: Router) {}

  public canActivate(): Observable<boolean | UrlTree> {
    return this.authFacade.token$.pipe(
      map((token) => {
        if (!token) {
          return true;
        } else {
          return this.router.parseUrl('/dashboard');
        }
      })
    );
  }
}

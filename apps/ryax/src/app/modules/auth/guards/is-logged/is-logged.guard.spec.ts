// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { hot } from 'jest-marbles';
import { of } from 'rxjs';
import { AuthFacade } from '../../state/facade';

import { IsLoggedGuard } from './is-logged.guard';

describe('IsLoggedGuard', () => {
  let guard: IsLoggedGuard;
  const authLoginFacadeSpy = {
    token$: of(null),
  };
  const routerSpy = {
    parseUrl: jest.fn().mockImplementation((value) => [value]),
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        { provide: AuthFacade, useValue: authLoginFacadeSpy },
        { provide: Router, useValue: routerSpy },
      ],
    });
  });

  describe('When token is defined', () => {
    beforeEach(() => {
      TestBed.overrideProvider(AuthFacade, {
        useValue: {
          token$: hot('a', { a: 'MyToken' }),
        },
      });
      guard = TestBed.inject(IsLoggedGuard);
    });

    it('should allow to access route', () => {
      expect(guard.canActivate()).toBeObservable(hot('a', { a: true }));
    });
  });

  describe('When token is NOT defined', () => {
    beforeEach(() => {
      TestBed.overrideProvider(AuthFacade, {
        useValue: {
          token$: hot('a', { a: null }),
        },
      });
      guard = TestBed.inject(IsLoggedGuard);
    });

    it('should redirect to /login', () => {
      expect(guard.canActivate()).toBeObservable(hot('a', { a: ['/login'] }));
    });
  });
});

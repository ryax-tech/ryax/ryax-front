import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Action } from '../../entities';

@Component({
  selector: 'ryax-workflow-action-details-card',
  templateUrl: './workflow-action-details-card.component.pug',
  styleUrls: ['./workflow-action-details-card.component.scss'],
})
export class WorkflowActionDetailsCardComponent {
  @Input() public showDetails = false;
  @Output() public showDetailsChange: EventEmitter<boolean> =
    new EventEmitter<boolean>();
  @Input() public index = 0;
  @Input({ required: true }) public logs!: string;
  @Input() public loading = false;
  @Input({ required: true }) public actionDetails!: Action;
  public toggleDetails() {
    this.showDetails = !this.showDetails;
    this.showDetailsChange.emit(this.showDetails);
  }
}

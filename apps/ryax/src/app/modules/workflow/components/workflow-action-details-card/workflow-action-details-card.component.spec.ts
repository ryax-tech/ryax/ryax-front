import { ComponentFixture, TestBed } from '@angular/core/testing';
import { WorkflowActionDetailsCardComponent } from './workflow-action-details-card.component';

describe('WorkflowActionDetailsCardComponent', () => {
  let component: WorkflowActionDetailsCardComponent;
  let fixture: ComponentFixture<WorkflowActionDetailsCardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [WorkflowActionDetailsCardComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(WorkflowActionDetailsCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

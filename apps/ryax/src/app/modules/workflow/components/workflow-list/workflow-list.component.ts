// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component, OnInit } from '@angular/core';
import { WorkflowFilters } from '../../entities/index';
import { WorkflowFacade } from '../../state/facade';

@Component({
  selector: 'ryax-workflow-list',
  templateUrl: './workflow-list.component.pug',
  styleUrls: ['./workflow-list.component.scss'],
})
export class WorkflowListComponent implements OnInit {
  public workflowList$ = this.facade.workflowList$;
  public workflowListLoading$ = this.facade.workflowListLoading$;
  public workflowFilters$ = this.facade.workflowFilters$;
  public searchTerm = '';
  public activeFilters: WorkflowFilters = {
    deploymentStatus: [],
    categories: [],
    endpoint: [],
    trigger: [],
  };

  constructor(private facade: WorkflowFacade) {}

  public ngOnInit(): void {
    this.facade.getWorkflows();
  }

  public refreshList(): void {
    this.facade.getWorkflows();
  }

  public toggleFilter(
    add: boolean,
    category: 'deploymentStatus' | 'categories' | 'trigger',
    filter: string
  ) {
    if (add) {
      this.activeFilters[category] = [...this.activeFilters[category], filter];
    } else {
      this.activeFilters[category] = [
        ...this.activeFilters[category].filter((value) => value != filter),
      ];
    }

    this.activeFilters = { ...this.activeFilters };
  }
}

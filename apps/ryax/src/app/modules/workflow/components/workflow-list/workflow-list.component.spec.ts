// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { WorkflowFacade } from '../../state/facade';

import { WorkflowListComponent } from './workflow-list.component';

describe('WorkflowListComponent', () => {
  let component: WorkflowListComponent;
  let fixture: ComponentFixture<WorkflowListComponent>;

  const workflowFacadeSpy = {
    getWorkflows: jest.fn(),
  };

  beforeEach(async () => {
    workflowFacadeSpy.getWorkflows.mockClear();

    await TestBed.configureTestingModule({
      declarations: [WorkflowListComponent],
      providers: [{ provide: WorkflowFacade, useValue: workflowFacadeSpy }],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkflowListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should get workflows onInit', () => {
    expect(workflowFacadeSpy.getWorkflows).toHaveBeenCalled();
  });
});

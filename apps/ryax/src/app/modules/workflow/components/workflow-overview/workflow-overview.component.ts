// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component, OnDestroy } from '@angular/core';
import { DeploymentStatus, TimelineColors } from '../../../shared/entity';
import { WorkflowFacade } from '../../state/facade';

@Component({
  selector: 'ryax-workflow-overview',
  templateUrl: './workflow-overview.component.pug',
  styleUrls: ['./workflow-overview.component.scss'],
})
export class WorkflowOverviewComponent implements OnDestroy {
  public workflow$ = this.facade.workflow$;
  public logs$ = this.facade.logs$;
  public timelineColor = TimelineColors.Purple;
  public showDetails: { [index: number]: boolean } = {};
  public watchLogs = false;

  constructor(private facade: WorkflowFacade) {}
  protected readonly DeploymentStatus = DeploymentStatus;

  public watchLogsToggle(): void {
    this.watchLogs = !this.watchLogs;
    if (this.watchLogs) {
      this.workflow$.subscribe((workflow) => {
        if (
          workflow &&
          workflow.deploymentStatus == DeploymentStatus.DEPLOYED
        ) {
          this.facade.startFetchLogs();
        }
      });
    } else {
      this.facade.stopFetchLogs();
    }
  }

  ngOnDestroy(): void {
    this.facade.stopFetchLogs();
  }
}

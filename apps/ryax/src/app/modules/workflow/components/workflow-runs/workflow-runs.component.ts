// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component } from '@angular/core';
import { NzTableQueryParams } from 'ng-zorro-antd/table';
import { WorkflowFacade } from '../../state/facade';
import { RunsFacade } from '../../../runs/state/runs.facade';
import { WorkflowRunStatus } from '../../../shared/entity';

@Component({
  selector: 'ryax-workflow-runs',
  templateUrl: './workflow-runs.component.pug',
  styleUrls: ['./workflow-runs.component.scss'],
})
export class WorkflowRunsComponent {
  public runList$ = this.facade.workflowRunsList$;
  public runTotal$ = this.facade.workflowRunsLength$;
  public runLoading$ = this.facade.workflowRunsLoading$;

  constructor(private facade: WorkflowFacade, private runFacade: RunsFacade) {}

  public getListPart(params: NzTableQueryParams) {
    this.facade.getWorkflowRuns(params.pageIndex - 1, params.pageSize);
  }

  public stopRun(id: string) {
    this.runFacade.stopRun(id);
  }

  public deleteRun(id: string) {
    this.runFacade.deleteRun(id);
  }

  protected readonly WorkflowRunStatus = WorkflowRunStatus;
}

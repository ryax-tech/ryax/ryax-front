// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component, Input } from '@angular/core';
import { WorkflowLight } from '../../entities';

@Component({
  selector: 'ryax-workflow-card',
  templateUrl: './workflow-card.component.pug',
  styleUrls: ['./workflow-card.component.scss'],
})
export class WorkflowCardComponent {
  @Input() public workflow: WorkflowLight = {} as WorkflowLight;
}

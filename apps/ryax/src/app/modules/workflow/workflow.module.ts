// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { AuthModule } from '../auth/auth.module';
import { ProjectModule } from '../project/project.module';
import { SharedModule } from '../shared/shared.module';
import { StudioModule } from '../studio/studio.module';
import { WorkflowAlertsComponent } from './components/workflow-alerts/workflow-alerts.component';
import { WorkflowCardComponent } from './components/workflow-card/workflow-card.component';
import { WorkflowRunsComponent } from './components/workflow-runs/workflow-runs.component';
import { WorkflowListComponent } from './components/workflow-list/workflow-list.component';
import { WorkflowOverviewComponent } from './components/workflow-overview/workflow-overview.component';
import { WorkflowComponent } from './components/workflow/workflow.component';
import { WorkflowActionDetailsCardComponent } from './components/workflow-action-details-card/workflow-action-details-card.component';
import { WorkflowApiService } from './services/workflow-api.service';
import { WorkflowEffects, WorkflowListEffects } from './state/effects';
import {
  WorkflowFeatureKey,
  WorkflowReducerProvider,
  WorkflowReducerToken,
} from './state/reducers';

@NgModule({
  declarations: [
    WorkflowComponent,
    WorkflowAlertsComponent,
    WorkflowCardComponent,
    WorkflowRunsComponent,
    WorkflowListComponent,
    WorkflowOverviewComponent,
    WorkflowActionDetailsCardComponent,
  ],
  imports: [
    CommonModule,
    StoreModule.forFeature(WorkflowFeatureKey, WorkflowReducerToken),
    EffectsModule.forFeature([WorkflowListEffects, WorkflowEffects]),
    RouterModule,
    SharedModule,
    AuthModule,
    StudioModule,
    ProjectModule,
  ],
  providers: [WorkflowReducerProvider, WorkflowApiService],
  exports: [WorkflowListComponent],
})
export class WorkflowModule {}

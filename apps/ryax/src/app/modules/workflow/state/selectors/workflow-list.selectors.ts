// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { createFeatureSelector, createSelector } from '@ngrx/store';
import { WorkflowFilters } from '../../entities/index';
import {
  GlobalWorkflowState,
  WorkflowFeatureKey,
  WorkflowListState,
} from '../reducers';

const selectWorkflowListFn = (state: GlobalWorkflowState) => state.workflowList;
const selectWorkflowListErrorFn = (state: WorkflowListState) => state.error;
const selectWorkflowListLoadingFn = (state: WorkflowListState) => state.loading;
const selectWorkflowListDataFn = (state: WorkflowListState) => state.data;
const selectWorkflowFiltersFn = (state: WorkflowListState): WorkflowFilters => {
  return {
    trigger: state.data
      .map((wf) => wf.trigger)
      .filter((value, index, array) => array.indexOf(value) === index),
    categories: state.data
      .map((wf) => wf.categories)
      .reduce((all, curr) => all.concat(curr), [])
      .filter((value, index, array) => array.indexOf(value) === index),
    endpoint: [],
    deploymentStatus: state.data
      .map((wf) => wf.deploymentStatus)
      .filter((value, index, array) => array.indexOf(value) === index),
  };
};

export const selectGlobalWorkflowState =
  createFeatureSelector<GlobalWorkflowState>(WorkflowFeatureKey);
export const selectWorkflowListState = createSelector(
  selectGlobalWorkflowState,
  selectWorkflowListFn
);
export const selectWorkflowListError = createSelector(
  selectWorkflowListState,
  selectWorkflowListErrorFn
);
export const selectWorkflowListLoading = createSelector(
  selectWorkflowListState,
  selectWorkflowListLoadingFn
);
export const selectWorkflowListData = createSelector(
  selectWorkflowListState,
  selectWorkflowListDataFn
);
export const selectWorkflowFilters = createSelector(
  selectWorkflowListState,
  selectWorkflowFiltersFn
);

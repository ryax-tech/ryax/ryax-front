// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { HttpErrorResponse } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { Actions } from '@ngrx/effects';
import { provideMockActions } from '@ngrx/effects/testing';
import { provideMockStore } from '@ngrx/store/testing';
import { cold, hot } from 'jest-marbles';
import { NzNotificationService } from 'ng-zorro-antd/notification';
import { Observable } from 'rxjs';
import {
  DeploymentStatus,
  WorkflowRun,
  WorkflowRunStatus,
} from '../../../shared/entity';
import { Workflow } from '../../entities';
import { WorkflowApiService } from '../../services/workflow-api.service';
import { WorkflowActions } from '../actions';
import { selectWorkflowData } from '../selectors/index';
import { WorkflowEffects } from './workflow.effects';
import { DateTime } from 'luxon';

const workflow: Workflow = {
  id: '1',
  name: 'test1',
  deploymentStatus: DeploymentStatus.DEPLOYED,
  hasForm: false,
  description: 'Test description 1',
  modules: [],
  deploymentError: '',
  endpoint: '',
};

const execList: WorkflowRun[] = [
  {
    id: '2',
    status: WorkflowRunStatus.Created,
    step: 1,
    totalSteps: 3,
    actionRuns: [],
    workflowId: '',
    startedAt: DateTime.now(),
    duration: '12s',
  },
];

describe('WorkflowEffects', () => {
  let effects: WorkflowEffects;
  let actions$: Observable<Actions>;

  const workflowApiSpy = {
    getWorkflow: jest.fn(),
    getWorkflowRuns: jest.fn(),
  };

  const notifSpy = {
    create: jest.fn(),
  };

  beforeEach(() => {
    workflowApiSpy.getWorkflow.mockReset();
    workflowApiSpy.getWorkflowRuns.mockReset();
    notifSpy.create.mockReset();

    TestBed.configureTestingModule({
      providers: [
        WorkflowEffects,
        provideMockStore({
          initialState: {
            globalWorkflowDomain: {
              workflow: {
                data: {
                  id: 'test_id',
                },
              },
            },
          },
          selectors: [
            {
              selector: selectWorkflowData,
              value: { data: { id: 'test_id' } },
            },
          ],
        }),
        provideMockActions(() => actions$),
        { provide: WorkflowApiService, useValue: workflowApiSpy },
        { provide: NzNotificationService, useValue: notifSpy },
      ],
    });
    effects = TestBed.inject(WorkflowEffects);
  });

  it('should be created', () => {
    expect(effects).toBeTruthy();
  });

  it('should try to get workflow and corresponding executions when asked', () => {
    workflowApiSpy.getWorkflow.mockImplementation(() =>
      cold('-a|', { a: workflow })
    );
    workflowApiSpy.getWorkflowRuns.mockImplementation(() =>
      cold('-a|', { a: { list: execList, total: 30 } })
    );

    actions$ = hot('--a', {
      a: WorkflowActions.getWorkflow('1'),
    });

    const expected$ = hot('----(ab)', {
      a: WorkflowActions.workflowSuccess(workflow),
      b: WorkflowActions.getWorkflowRunsSuccess(execList, 30),
    });

    expect(effects.getWorkflow$).toBeObservable(expected$);
    expect(expected$).toSatisfyOnFlush(() => {
      expect(workflowApiSpy.getWorkflow).toHaveBeenCalledWith('1');
    });
  });

  it('should handle errors when trying to get workflow', () => {
    workflowApiSpy.getWorkflow.mockImplementation(() =>
      cold('-#', {}, { error: '400' })
    );
    workflowApiSpy.getWorkflowRuns.mockImplementation(() =>
      cold('-a|', { a: { list: execList, total: 30 } })
    );

    actions$ = hot('--a', {
      a: WorkflowActions.getWorkflow('1'),
    });

    const expected$ = hot('---a', {
      a: WorkflowActions.workflowError({ error: '400' } as HttpErrorResponse),
    });

    expect(effects.getWorkflow$).toBeObservable(expected$);
    expect(expected$).toSatisfyOnFlush(() => {
      expect(workflowApiSpy.getWorkflow).toHaveBeenCalledWith('1');
    });
  });

  it('should try to get workflow executions when asked', () => {
    workflowApiSpy.getWorkflowRuns.mockImplementation(() =>
      cold('-a|', { a: { list: execList, total: 30 } })
    );

    actions$ = hot('--a', {
      a: WorkflowActions.getWorkflowRuns(0, 10),
    });

    const expected$ = hot('---a', {
      a: WorkflowActions.getWorkflowRunsSuccess(execList, 30),
    });

    expect(effects.getWorkflowRuns$).toBeObservable(expected$);
    expect(expected$).toSatisfyOnFlush(() => {
      expect(workflowApiSpy.getWorkflowRuns).toHaveBeenCalledWith(
        'test_id',
        0,
        10
      );
    });
  });
});

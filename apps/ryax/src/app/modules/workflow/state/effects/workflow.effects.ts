// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { NzNotificationService } from 'ng-zorro-antd/notification';
import { of } from 'rxjs';
import {
  catchError,
  delay,
  map,
  switchMap,
  tap,
  withLatestFrom,
} from 'rxjs/operators';
import { DeploymentStatus, WorkflowRun } from '../../../shared/entity';
import { WorkflowApiService } from '../../services/workflow-api.service';
import { WorkflowActions } from '../actions';
import { WorkflowState } from '../reducers';
import {
  selectLastParams,
  selectWorkflowData,
  selectWorkflowForceStopRefresh,
} from '../selectors';
import { Workflow } from '../../entities';
import { WorkflowWebsocketService } from '../../services/workflow-websocket.service';
import { RunsActions } from '../../../runs/state/actions';

@Injectable()
export class WorkflowEffects {
  getWorkflow$ = createEffect(() =>
    this.actions$.pipe(
      ofType(
        WorkflowActions.getWorkflow,
        WorkflowActions.deploySuccess,
        WorkflowActions.stopSuccess
      ),
      switchMap((action) =>
        this.workflowApiService.getWorkflow(action.workflowId).pipe(
          map((workflow) => WorkflowActions.workflowSuccess(workflow)),
          catchError((err: HttpErrorResponse) =>
            of(WorkflowActions.workflowError(err))
          )
        )
      )
    )
  );

  getWorkflowRunsInit$ = createEffect(() =>
    this.actions$.pipe(
      ofType(
        WorkflowActions.getWorkflow,
        WorkflowActions.deploySuccess,
        WorkflowActions.stopSuccess
      ),
      switchMap((action) =>
        this.workflowApiService.getWorkflowRuns(action.workflowId).pipe(
          map((executions) =>
            WorkflowActions.getWorkflowRunsSuccess(
              executions.list,
              executions.total
            )
          ),
          catchError((err: HttpErrorResponse) =>
            of(WorkflowActions.getWorkflowRunsError(err))
          )
        )
      )
    )
  );

  getResultsConfig$ = createEffect(() =>
    this.actions$.pipe(
      ofType(
        WorkflowActions.getWorkflow,
        WorkflowActions.deploySuccess,
        WorkflowActions.stopSuccess
      ),
      switchMap((action) =>
        this.workflowApiService
          .getWorkflowResultsConfig(action.workflowId)
          .pipe(
            map((results) => WorkflowActions.getResultsConfigSuccess(results)),
            catchError((err: HttpErrorResponse) =>
              of(WorkflowActions.getResultsConfigError(err))
            )
          )
      )
    )
  );

  displayErrors$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(
          WorkflowActions.workflowError,
          WorkflowActions.getWorkflowRunsError,
          WorkflowActions.copyError,
          WorkflowActions.deleteWfError,
          WorkflowActions.getResultsConfigError,
          WorkflowActions.deployError,
          WorkflowActions.stopError,
          WorkflowActions.exportWfError
        ),
        tap((errAction) =>
          this.notification.create(
            'error',
            'Something went wrong ! http error : ' + errAction.error.status,
            errAction.error.message + ': ' + errAction.error.error.error
          )
        )
      ),
    { dispatch: false }
  );

  getWorkflowRuns$ = createEffect(() =>
    this.actions$.pipe(
      ofType(WorkflowActions.getWorkflowRuns),
      withLatestFrom(this.store$.select(selectWorkflowData)),
      switchMap(([action, workflow]) =>
        this.workflowApiService
          .getWorkflowRuns(workflow!.id, action.pageIndex, action.pageSize)
          .pipe(
            map((data: { list: WorkflowRun[]; total: number }) => {
              return WorkflowActions.getWorkflowRunsSuccess(
                data.list,
                data.total
              );
            }),
            catchError((err: HttpErrorResponse) => {
              return of(WorkflowActions.getWorkflowRunsError(err));
            })
          )
      )
    )
  );

  refreshWorkflowRuns$ = createEffect(() =>
    this.actions$.pipe(
      ofType(
        WorkflowActions.refreshWorkflowRuns,
        RunsActions.stopSuccess,
        RunsActions.deleteSuccess
      ),
      withLatestFrom(this.store$.select(selectWorkflowData)),
      withLatestFrom(this.store$.select(selectLastParams)),
      switchMap(([[, workflow], params]) => {
        return this.workflowApiService
          .getWorkflowRuns(workflow!.id, params[0], params[1])
          .pipe(
            map((data: { list: WorkflowRun[]; total: number }) => {
              return WorkflowActions.getWorkflowRunsSuccess(
                data.list,
                data.total
              );
            }),
            catchError((err: HttpErrorResponse) => {
              return of(WorkflowActions.getWorkflowRunsError(err));
            })
          );
      })
    )
  );

  deploy$ = createEffect(() =>
    this.actions$.pipe(
      ofType(WorkflowActions.deploy),
      withLatestFrom(
        this.store$.select(selectWorkflowData),
        (action, workflow) => workflow as Workflow
      ),
      switchMap((workflow) =>
        this.workflowApiService.deploy(workflow.id).pipe(
          map(() => {
            return WorkflowActions.deploySuccess(workflow.id);
          }),
          catchError((err: HttpErrorResponse) => {
            return of(WorkflowActions.deployError(err));
          })
        )
      )
    )
  );

  workflowRefresh$ = createEffect(() =>
    this.actions$.pipe(
      ofType(
        WorkflowActions.deploySuccess,
        WorkflowActions.stopSuccess,
        WorkflowActions.workflowSuccess,
        WorkflowActions.keepRefreshingWorkflow
      ),
      withLatestFrom(this.store$.select(selectWorkflowForceStopRefresh)),
      withLatestFrom(
        this.store$.select(selectWorkflowData),
        ([, forceStop], workflow) => ({
          workflowId: (workflow as Workflow).id,
          forceStop,
        })
      ),
      delay(1000),
      switchMap(({ workflowId, forceStop }) =>
        this.workflowApiService.getWorkflow(workflowId).pipe(
          map((workflow) => {
            if (forceStop) {
              return WorkflowActions.stopRefreshingWorkflowSuccess();
            }
            if (
              workflow.deploymentStatus === DeploymentStatus.DEPLOYING ||
              workflow.deploymentStatus === DeploymentStatus.UNDEPLOYING
            ) {
              return WorkflowActions.keepRefreshingWorkflow(workflow);
            }
            return WorkflowActions.refreshWorkflow(workflow);
          }),
          catchError((err: HttpErrorResponse) => {
            return of(WorkflowActions.workflowError(err));
          })
        )
      )
    )
  );

  stop$ = createEffect(() =>
    this.actions$.pipe(
      ofType(WorkflowActions.stop),
      withLatestFrom(
        this.store$.select(selectWorkflowData),
        (action, workflow) => ({
          graceful: action.graceful,
          workflow: workflow as Workflow,
        })
      ),
      switchMap(({ workflow, graceful }) =>
        this.workflowApiService.stop(workflow.id, graceful).pipe(
          map(() => {
            return WorkflowActions.stopSuccess(workflow.id);
          }),
          catchError((err: HttpErrorResponse) => {
            return of(WorkflowActions.stopError(err));
          })
        )
      )
    )
  );

  copy$ = createEffect(() =>
    this.actions$.pipe(
      ofType(WorkflowActions.copy),
      withLatestFrom(
        this.store$.select(selectWorkflowData),
        (action, workflow) => workflow as Workflow
      ),
      switchMap((workflow) =>
        this.workflowApiService.copy(workflow.id, workflow.name).pipe(
          map(() => {
            return WorkflowActions.copySuccess();
          }),
          catchError((err: HttpErrorResponse) => {
            return of(WorkflowActions.copyError(err));
          })
        )
      )
    )
  );

  export$ = createEffect(() =>
    this.actions$.pipe(
      ofType(WorkflowActions.exportWf),
      withLatestFrom(this.store$.select(selectWorkflowData)),
      switchMap(([, workflow]) =>
        // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
        this.workflowApiService.export(workflow!.id).pipe(
          map(() => {
            return WorkflowActions.exportWfSuccess();
          }),
          catchError((err: HttpErrorResponse) => {
            return of(WorkflowActions.exportWfError(err));
          })
        )
      )
    )
  );

  delete$ = createEffect(() =>
    this.actions$.pipe(
      ofType(WorkflowActions.deleteWf),
      withLatestFrom(this.store$.select(selectWorkflowData)),
      switchMap(([, workflow]) =>
        // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
        this.workflowApiService.delete(workflow!.id).pipe(
          map(() => {
            this.router.navigateByUrl('/dashboard');
            return WorkflowActions.deleteWfSuccess();
          }),
          catchError((err: HttpErrorResponse) => {
            return of(WorkflowActions.deleteWfError(err));
          })
        )
      )
    )
  );

  startLogs$ = createEffect(() =>
    this.actions$.pipe(
      ofType(WorkflowActions.startFetchLogs),
      withLatestFrom(this.store$.select(selectWorkflowData)),
      switchMap(([, workflow]) => {
        if (this.workflowWebsocketService.is_closed()) {
          this.workflowWebsocketService.connect(workflow!.id);
          this.workflowWebsocketService.socket$!.subscribe({
            next: (msg) =>
              this.store$.dispatch(
                WorkflowActions.newLogLinesFetched(
                  (msg as { logs: string }).logs
                )
              ),
            error: (error) =>
              this.store$.dispatch(WorkflowActions.fetchLogsError(error)),
          });
          this.workflowWebsocketService.sendMessage({ logs: '' });
        }
        return of(WorkflowActions.fetchLogsConnected());
      })
    )
  );
  stopLogs$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(WorkflowActions.stopFetchLogs),
        tap(() => {
          this.workflowWebsocketService.close();
        })
      ),
    { dispatch: false }
  );

  displaySimpleErrors$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(WorkflowActions.fetchLogsError),
        tap((errAction) =>
          this.notification.create(
            'error',
            'Unable to fetch the logs ! Please reload the page',
            errAction.error
          )
        )
      ),
    { dispatch: false }
  );

  constructor(
    private actions$: Actions,
    private workflowApiService: WorkflowApiService,
    private workflowWebsocketService: WorkflowWebsocketService,
    private notification: NzNotificationService,
    private store$: Store<WorkflowState>,
    private router: Router
  ) {}
}

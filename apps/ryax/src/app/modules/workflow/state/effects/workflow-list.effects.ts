// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { NzNotificationService } from 'ng-zorro-antd/notification';
import { of } from 'rxjs';
import { catchError, map, switchMap, tap } from 'rxjs/operators';
import { WorkflowLight } from '../../entities';
import { WorkflowApiService } from '../../services/workflow-api.service';
import { WorkflowListActions } from '../actions';

@Injectable()
export class WorkflowListEffects {
  getWorkflows$ = createEffect(() =>
    this.actions$.pipe(
      ofType(WorkflowListActions.getWorkflows),
      switchMap(() =>
        this.workflowApiService.getWorkflows().pipe(
          map((workflows: WorkflowLight[]) =>
            WorkflowListActions.workflowsSuccess(workflows)
          ),
          catchError((err: HttpErrorResponse) => {
            return of(WorkflowListActions.workflowsError(err));
          })
        )
      )
    )
  );

  displayErrors$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(WorkflowListActions.workflowsError),
        tap((errAction) =>
          this.notification.create(
            'error',
            'Something went wrong ! http error : ' + errAction.error.status,
            errAction.error.message
          )
        )
      ),
    { dispatch: false }
  );

  constructor(
    private actions$: Actions,
    private workflowApiService: WorkflowApiService,
    private notification: NzNotificationService
  ) {}
}

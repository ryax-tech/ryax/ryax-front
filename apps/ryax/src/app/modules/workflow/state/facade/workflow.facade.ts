// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { WorkflowActions, WorkflowListActions } from '../actions';
import { GlobalWorkflowState } from '../reducers';
import {
  selectResultsConfig,
  selectWorkflowData,
  selectWorkflowFilters,
  selectWorkflowListData,
  selectWorkflowListLoading,
  selectWorkflowLoading,
  selectWorkflowLogs,
  selectWorkflowRunsLength,
  selectWorkflowRunsList,
  selectWorkflowRunsLoading,
  selectWorkflowStatus,
} from '../selectors';

@Injectable({
  providedIn: 'root',
})
export class WorkflowFacade {
  public workflowList$ = this.store.select(selectWorkflowListData);
  public workflowListLoading$ = this.store.select(selectWorkflowListLoading);
  public workflow$ = this.store.select(selectWorkflowData);
  public workflowStatus$ = this.store.select(selectWorkflowStatus);
  public workflowLoading$ = this.store.select(selectWorkflowLoading);
  public workflowRunsList$ = this.store.select(selectWorkflowRunsList);
  public workflowRunsLength$ = this.store.select(selectWorkflowRunsLength);
  public workflowRunsLoading$ = this.store.select(selectWorkflowRunsLoading);
  public resultsConfig$ = this.store.select(selectResultsConfig);
  public workflowFilters$ = this.store.select(selectWorkflowFilters);
  public logs$ = this.store.select(selectWorkflowLogs);

  constructor(private readonly store: Store<GlobalWorkflowState>) {}

  public getWorkflows() {
    this.store.dispatch(WorkflowListActions.getWorkflows());
  }

  public getWorkflow(id: string) {
    this.store.dispatch(WorkflowActions.getWorkflow(id));
  }

  public getWorkflowRuns(pageIndex: number, pageSize: number) {
    this.store.dispatch(WorkflowActions.getWorkflowRuns(pageIndex, pageSize));
  }

  public refreshWorkflowRuns() {
    this.store.dispatch(WorkflowActions.refreshWorkflowRuns());
  }

  public deploy() {
    this.store.dispatch(WorkflowActions.deploy());
  }

  public stop(graceful: boolean) {
    this.store.dispatch(WorkflowActions.stop(graceful));
  }

  public copy() {
    this.store.dispatch(WorkflowActions.copy());
  }

  public export() {
    this.store.dispatch(WorkflowActions.exportWf());
  }

  public delete() {
    this.store.dispatch(WorkflowActions.deleteWf());
  }

  public stopRefreshing() {
    this.store.dispatch(WorkflowActions.stopRefreshingWorkflow());
  }

  public startFetchLogs() {
    this.store.dispatch(WorkflowActions.startFetchLogs());
  }

  public stopFetchLogs() {
    this.store.dispatch(WorkflowActions.stopFetchLogs());
  }
}

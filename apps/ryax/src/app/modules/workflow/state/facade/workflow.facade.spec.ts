// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { TestBed } from '@angular/core/testing';
import { Store } from '@ngrx/store';

import { WorkflowFacade } from './workflow.facade';

describe('WorkflowFacade', () => {
  let service: WorkflowFacade;
  const storeSpy = {
    dispatch: jest.fn(),
    select: jest.fn(),
  };

  beforeEach(() => {
    storeSpy.dispatch.mockClear();

    TestBed.configureTestingModule({
      providers: [WorkflowFacade, { provide: Store, useValue: storeSpy }],
    });
    service = TestBed.inject(WorkflowFacade);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should get list of workflow', () => {
    service.getWorkflows();
    expect(storeSpy.dispatch).toHaveBeenCalledWith({
      type: '[Workflow List] Get Workflow List',
    });
  });

  it('should get a single workflow', () => {
    service.getWorkflow('12');
    expect(storeSpy.dispatch).toHaveBeenCalledWith({
      type: '[Workflow] Get Workflow',
      workflowId: '12',
    });
  });
});

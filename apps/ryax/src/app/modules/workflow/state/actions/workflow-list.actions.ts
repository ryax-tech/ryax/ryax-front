// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { HttpErrorResponse } from '@angular/common/http';
import { createAction } from '@ngrx/store';
import { WorkflowLight } from '../../entities';

export const getWorkflows = createAction('[Workflow List] Get Workflow List');

export const workflowsSuccess = createAction(
  '[Workflow List] Get Workflow List success',
  (workflows: WorkflowLight[]) => ({ workflows })
);

export const workflowsError = createAction(
  '[Workflow List] Get Workflow List error',
  (error: HttpErrorResponse) => ({ error })
);

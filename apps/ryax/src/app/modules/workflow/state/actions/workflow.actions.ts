// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { HttpErrorResponse } from '@angular/common/http';
import { createAction } from '@ngrx/store';
import { WorkflowRun } from '../../../shared/entity';
import { Workflow } from '../../entities';

export const getWorkflow = createAction(
  '[Workflow] Get Workflow',
  (workflowId: string) => ({ workflowId })
);

export const workflowSuccess = createAction(
  '[Workflow] Get Workflow success',
  (workflow: Workflow) => ({ workflow })
);

export const workflowError = createAction(
  '[Workflow] Get Workflow error',
  (error: HttpErrorResponse) => ({ error })
);

export const getWorkflowRuns = createAction(
  '[Workflow] Get Workflow run',
  (pageIndex: number, pageSize: number) => ({ pageIndex, pageSize })
);

export const refreshWorkflowRuns = createAction(
  '[Workflow] Refresh Workflow run'
);

export const getWorkflowRunsSuccess = createAction(
  '[Workflow] Get Workflow run success',
  (workflowRuns: WorkflowRun[], total: number) => ({ workflowRuns, total })
);

export const getWorkflowRunsError = createAction(
  '[Workflow] Get Workflow run error',
  (error: HttpErrorResponse) => ({ error })
);

export const deploy = createAction('[Workflow] Deploy workflow');

export const deploySuccess = createAction(
  '[Workflow] Deploy workflow success',
  (workflowId: string) => ({ workflowId })
);

export const deployError = createAction(
  '[Workflow] Deploy workflow error',
  (error: HttpErrorResponse) => ({ error })
);

export const stop = createAction(
  '[Workflow] Stop workflow',
  (graceful: boolean) => ({ graceful })
);

export const stopSuccess = createAction(
  '[Workflow] Stop workflow success',
  (workflowId: string) => ({ workflowId })
);

export const stopError = createAction(
  '[Workflow] Stop workflow error',
  (error: HttpErrorResponse) => ({ error })
);

export const refreshWorkflow = createAction(
  '[Workflow] refresh workflow',
  (workflow: Workflow) => ({ workflow })
);

export const keepRefreshingWorkflow = createAction(
  '[Workflow] Keep refreshing workflow',
  (workflow: Workflow) => ({ workflow })
);

export const stopRefreshingWorkflow = createAction(
  '[Workflow] stop refreshing workflow'
);

export const stopRefreshingWorkflowSuccess = createAction(
  '[Workflow] stop refreshing workflow success'
);

export const copy = createAction('[Workflow] Copy workflow');

export const copySuccess = createAction('[Workflow] Copy workflow success');

export const copyError = createAction(
  '[Workflow] Copy workflow error',
  (error: HttpErrorResponse) => ({ error })
);

export const exportWf = createAction('[Workflow] Export workflow');

export const exportWfSuccess = createAction(
  '[Workflow] Export workflow success'
);

export const exportWfError = createAction(
  '[Workflow] Export workflow error',
  (error: HttpErrorResponse) => ({ error })
);

export const deleteWf = createAction('[Workflow] Delete workflow');

export const deleteWfSuccess = createAction(
  '[Workflow] Delete workflow success'
);

export const deleteWfError = createAction(
  '[Workflow] Delete workflow error',
  (error: HttpErrorResponse) => ({ error })
);

export const getResultsConfigSuccess = createAction(
  '[Workflow] Get results config success',
  (resultsConfig: { key: string; description: string }[]) => ({ resultsConfig })
);

export const getResultsConfigError = createAction(
  '[Workflow] Get results config error',
  (error: HttpErrorResponse) => ({ error })
);

export const getEndpointSuccess = createAction(
  '[Workflow] Get endpoint success',
  (endpoint: string) => ({ endpoint })
);

export const getEndpointError = createAction(
  '[Workflow] Get endpoint error',
  (error: HttpErrorResponse) => ({ error })
);
export const startFetchLogs = createAction(
  '[Workflow] Start fetching workflow trigger logs'
);
export const stopFetchLogs = createAction(
  '[Workflow] Stop fetching workflow trigger logs'
);

export const newLogLinesFetched = createAction(
  '[Workflow] Workflow trigger new logs lines fetched',
  (logs: string) => ({ logs })
);
export const fetchLogsConnected = createAction(
  '[Workflow] Get workflow trigger logs connected'
);
export const fetchLogsError = createAction(
  '[Workflow] Get workflow trigger logs error',
  (error: string) => ({ error })
);

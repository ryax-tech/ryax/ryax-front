// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { HttpErrorResponse } from '@angular/common/http';
import { createReducer, on } from '@ngrx/store';
import { WorkflowRun } from '../../../shared/entity';
import { Workflow } from '../../entities';
import { WorkflowActions } from '../actions';

export interface WorkflowState {
  data: Workflow | null;
  workflowRunList: WorkflowRun[];
  lastParams: [number, number];
  totalExecutions: number;
  loading: boolean;
  runsLoading: boolean;
  deployLoading: boolean;
  resultsConfig: { key: string; description: string }[];
  endpoint: string;
  error: HttpErrorResponse | null;
  forceStopRefresh: boolean;
  logs: { value: string; loading: boolean; error: string | null };
}

export const initialWorkflowState: WorkflowState = {
  data: null,
  workflowRunList: [],
  lastParams: [0, 10],
  totalExecutions: 0,
  loading: false,
  runsLoading: false,
  deployLoading: false,
  resultsConfig: [],
  endpoint: '',
  error: null,
  forceStopRefresh: false,
  logs: {
    value: '',
    loading: false,
    error: null,
  },
};

export const workflowReducer = createReducer<WorkflowState>(
  initialWorkflowState,
  on(WorkflowActions.getWorkflow, (state) => ({
    ...state,
    loading: true,
    error: null,
    forceStopRefresh: false,
  })),
  on(
    WorkflowActions.workflowSuccess,
    WorkflowActions.keepRefreshingWorkflow,
    WorkflowActions.refreshWorkflow,
    (state, { workflow }) => ({
      ...state,
      data: workflow,
      loading: false,
      error: null,
    })
  ),
  on(WorkflowActions.workflowError, (state, { error }) => ({
    ...state,
    loading: false,
    error,
  })),
  on(WorkflowActions.stopRefreshingWorkflow, (state) => ({
    ...state,
    forceStopRefresh: true,
  })),
  on(WorkflowActions.stopRefreshingWorkflowSuccess, (state) => ({
    ...state,
    forceStopRefresh: false,
  })),
  on(WorkflowActions.getWorkflowRuns, (state, { pageIndex, pageSize }) => ({
    ...state,
    lastParams: [pageIndex, pageSize],
    runsLoading: true,
    error: null,
  })),
  on(WorkflowActions.refreshWorkflowRuns, (state) => ({
    ...state,
    runsLoading: true,
    error: null,
  })),
  on(
    WorkflowActions.getWorkflowRunsSuccess,
    (state, { workflowRuns, total }) => ({
      ...state,
      workflowRunList: workflowRuns,
      totalExecutions: total,
      runsLoading: false,
      error: null,
    })
  ),
  on(WorkflowActions.getWorkflowRunsError, (state, { error }) => ({
    ...state,
    runsLoading: false,
    error,
  })),
  on(WorkflowActions.deploy, (state) => ({
    ...state,
    deployLoading: true,
    error: null,
  })),
  on(WorkflowActions.deploySuccess, (state) => ({
    ...state,
    deployLoading: false,
    error: null,
  })),
  on(WorkflowActions.deployError, (state, { error }) => ({
    ...state,
    deployLoading: false,
    error,
  })),
  on(WorkflowActions.stop, (state) => ({
    ...state,
    deployLoading: true,
    error: null,
  })),
  on(WorkflowActions.stopSuccess, (state) => ({
    ...state,
    deployLoading: false,
    error: null,
  })),
  on(WorkflowActions.stopError, (state, { error }) => ({
    ...state,
    deployLoading: false,
    error,
  })),
  on(WorkflowActions.copy, (state) => ({
    ...state,
    loading: true,
    error: null,
  })),
  on(WorkflowActions.copySuccess, (state) => ({
    ...state,
    loading: false,
    error: null,
  })),
  on(WorkflowActions.copyError, (state, { error }) => ({
    ...state,
    loading: false,
    error,
  })),
  on(WorkflowActions.exportWf, (state) => ({
    ...state,
    loading: true,
    error: null,
  })),
  on(WorkflowActions.exportWfSuccess, (state) => ({
    ...state,
    loading: false,
    error: null,
  })),
  on(WorkflowActions.exportWfError, (state, { error }) => ({
    ...state,
    loading: false,
    error,
  })),
  on(WorkflowActions.deleteWf, (state) => ({
    ...state,
    loading: true,
    error: null,
  })),
  on(WorkflowActions.deleteWfSuccess, (state) => ({
    ...state,
    loading: false,
    error: null,
  })),
  on(WorkflowActions.deleteWfError, (state, { error }) => ({
    ...state,
    loading: false,
    error,
  })),
  on(WorkflowActions.getResultsConfigSuccess, (state, { resultsConfig }) => ({
    ...state,
    resultsConfig,
    loading: false,
    error: null,
  })),
  on(WorkflowActions.getResultsConfigError, (state, { error }) => ({
    ...state,
    loading: false,
    error,
  })),
  on(WorkflowActions.getEndpointSuccess, (state, { endpoint }) => ({
    ...state,
    endpoint,
    loading: false,
    error: null,
  })),
  on(WorkflowActions.getEndpointError, (state, { error }) => ({
    ...state,
    loading: false,
    error,
  })),
  on(WorkflowActions.startFetchLogs, (state) => ({
    ...state,
    logs: {
      loading: true,
      value: state.logs.value,
      error: null,
    },
  })),
  on(WorkflowActions.stopFetchLogs, (state) => ({
    ...state,
    logs: {
      loading: false,
      value: '',
      error: null,
    },
  })),
  on(WorkflowActions.fetchLogsConnected, (state) => ({
    ...state,
    logs: {
      loading: false,
      value: state.logs.value,
      error: null,
    },
  })),
  on(WorkflowActions.newLogLinesFetched, (state, { logs }) => ({
    ...state,
    logs: {
      loading: true,
      value: state.logs.value + logs,
      error: null,
    },
  })),
  on(WorkflowActions.fetchLogsError, (state, { error }) => ({
    ...state,
    logs: {
      loading: false,
      value: state.logs.value,
      error: error,
    },
  }))
);

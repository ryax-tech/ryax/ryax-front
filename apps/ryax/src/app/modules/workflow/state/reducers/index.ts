// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
export * from './workflow-list.reducer';
export * from './workflow.reducer';

import { InjectionToken } from '@angular/core';
import { ActionReducerMap } from '@ngrx/store';
import {
  workflowListReducer,
  WorkflowListState,
} from './workflow-list.reducer';
import { workflowReducer, WorkflowState } from './workflow.reducer';

export interface GlobalWorkflowState {
  workflowList: WorkflowListState;
  workflow: WorkflowState;
}

export const WorkflowFeatureKey = 'globalWorkflowDomain';

export const WorkflowReducerToken = new InjectionToken<
  ActionReducerMap<GlobalWorkflowState>
>(WorkflowFeatureKey);

export const WorkflowReducerProvider = {
  provide: WorkflowReducerToken,
  useValue: {
    workflowList: workflowListReducer,
    workflow: workflowReducer,
  },
};

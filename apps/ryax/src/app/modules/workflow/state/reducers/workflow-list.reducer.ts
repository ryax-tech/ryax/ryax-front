// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { HttpErrorResponse } from '@angular/common/http';
import { createReducer, on } from '@ngrx/store';
import { WorkflowLight } from '../../entities';
import { WorkflowListActions } from '../actions';

export interface WorkflowListState {
  data: WorkflowLight[];
  loading: boolean;
  error: HttpErrorResponse | null;
}

export const initialWorkflowsState: WorkflowListState = {
  data: [],
  loading: false,
  error: null,
};

export const workflowListReducer = createReducer<WorkflowListState>(
  initialWorkflowsState,
  on(WorkflowListActions.getWorkflows, (state) => ({
    ...state,
    loading: true,
    error: null,
  })),
  on(WorkflowListActions.workflowsSuccess, (state, { workflows }) => ({
    ...state,
    data: workflows,
    loading: false,
    error: null,
  })),
  on(WorkflowListActions.workflowsError, (state, { error }) => ({
    ...state,
    loading: false,
    error,
  }))
);

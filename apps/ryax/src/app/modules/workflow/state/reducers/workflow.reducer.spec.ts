// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { HttpErrorResponse } from '@angular/common/http';
import { DateTime } from 'luxon';
import { DeploymentStatus, WorkflowRunStatus } from '../../../shared/entity';
import { WorkflowActions } from '../actions';
import {
  initialWorkflowState,
  workflowReducer,
  WorkflowState,
} from './workflow.reducer';

describe('workflowReducer', () => {
  const state: WorkflowState = initialWorkflowState;

  const error: HttpErrorResponse = {
    name: 'HttpErrorResponse',
    message: 'message',
    error: 401,
  } as HttpErrorResponse;

  it('on WorkflowActions.getWorkflow', () => {
    const action = WorkflowActions.getWorkflow('1');
    expect(workflowReducer(state, action)).toEqual({
      ...state,
      loading: true,
      error: null,
    });
  });

  it('on WorkflowActions.workflowsSuccess', () => {
    const action = WorkflowActions.workflowSuccess({
      id: '1',
      name: 'test1',
      description: 'Test 1',
      hasForm: false,
      deploymentStatus: DeploymentStatus.DEPLOYED,
      modules: [],
      endpoint: '',
      deploymentError: '',
    });
    expect(workflowReducer(state, action)).toEqual({
      ...state,
      data: {
        id: '1',
        name: 'test1',
        description: 'Test 1',
        hasForm: false,
        deploymentStatus: DeploymentStatus.DEPLOYED,
        modules: [],
      },
      loading: false,
      error: null,
    });
  });

  it('on WorkflowActions.workflowsError', () => {
    const action = WorkflowActions.workflowError(error);
    expect(workflowReducer(state, action)).toEqual({
      ...state,
      loading: false,
      error: error,
    });
  });

  it('on WorkflowActions.workflowsExecutionsSuccess', () => {
    const action = WorkflowActions.getWorkflowRunsSuccess(
      [
        {
          id: '12',
          status: WorkflowRunStatus.Created,
          step: 1,
          totalSteps: 3,
          duration: '9786',
          startedAt: DateTime.now(),
          actionRuns: [],
          workflowId: '',
        },
      ],
      30
    );
    expect(workflowReducer(state, action)).toEqual({
      ...state,
      workflowRuns: [
        {
          id: '12',
          status: WorkflowRunStatus.Created,
          step: 1,
          totalSteps: 3,
          duration: 9786,
          startedAt: new Date(98785987),
          moduleExecutions: [],
        },
      ],
      totalExecutions: 30,
      loading: false,
      error: null,
    });
  });
});

import { Injectable } from '@angular/core';
import { webSocket, WebSocketSubject } from 'rxjs/webSocket';

@Injectable({
  providedIn: 'root',
})
export class WorkflowWebsocketService {
  public url: string | null = null;
  public socket$: WebSocketSubject<unknown> | null = null;

  public connect(workflowId: string) {
    if (!this.socket$ || this.socket$.closed) {
      this.url =
        (window.location.protocol === 'https:' ? 'wss://' : 'ws://') +
        window.location.host +
        '/api/runner/ws/workflows/' +
        workflowId;
      this.socket$ = this.getNewWebSocket();
      //this.socket$.subscribe({
      //  next: msg => {
      //    console.log('message received: ')
      //    console.log(msg)
      //  }, // Called whenever there is a message from the server.
      //  error: err => console.log(err), // Called if at any point WebSocket API signals some kind of error.
      //  complete: () => console.log('complete') // Called when connection is closed (for whatever reason).
      //});
    }
    return this.socket$;
  }

  sendMessage(msg: any) {
    if (this.socket$) {
      this.socket$.next(msg);
      // console.log("message sent: " + msg)
    } else {
      console.warn('Unable to send message to the websocket');
    }
  }

  close() {
    if (this.socket$ && !this.is_closed()) {
      this.socket$.unsubscribe();
      this.socket$.complete();
    }
  }

  private getNewWebSocket() {
    return webSocket({
      url: this.url!,
    });
  }

  is_closed() {
    return this.socket$ == null || this.socket$!.closed;
  }
}

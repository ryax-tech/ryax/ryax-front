// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { WorkflowRunStatus } from '../../shared/entity';

import { WorkflowApiService } from './workflow-api.service';

describe('DashboardApiService', () => {
  let service: WorkflowApiService;
  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    service = TestBed.inject(WorkflowApiService);
    httpTestingController = TestBed.inject(HttpTestingController);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should get workflow list', () => {
    service.getWorkflows().subscribe((response) => {
      expect(response).toEqual([
        {
          id: '12',
          name: 'test1',
          description: 'Tada',
          runPrice: 18,
          timesRun: 8,
        },
        {
          id: '13',
          name: 'test2',
          description: 'Tadada',
          runPrice: 9,
          timesRun: 6,
        },
      ]);
    });

    const request = httpTestingController.expectOne('/studio/workflows');
    expect(request.request.method).toEqual('GET');
    request.flush([
      {
        id: '12',
        name: 'test1',
        description: 'Tada',
        run_price: 18,
        times_run: 8,
      },
      {
        id: '13',
        name: 'test2',
        description: 'Tadada',
        run_price: 9,
        times_run: 6,
      },
    ]);
  });

  it('should get single workflow', () => {
    service.getWorkflow('12').subscribe((response) => {
      expect(response).toEqual({
        id: '12',
        name: 'test1',
        description: 'Tada',
        runPrice: 18,
        timesRun: 8,
      });
    });

    const request = httpTestingController.expectOne('/studio/workflows/12');
    expect(request.request.method).toEqual('GET');
    request.flush({
      id: '12',
      name: 'test1',
      description: 'Tada',
      run_price: 18,
      times_run: 8,
    });
  });

  it('should get workflow runs list', () => {
    service.getWorkflowRuns('12').subscribe((response) => {
      expect(response).toEqual([
        {
          id: '2',
          startedAt: new Date(9856987),
          status: WorkflowRunStatus.Created,
          step: 0,
          modules: [],
        },
      ]);
    });

    const request = httpTestingController.expectOne(
      '/runner/workflow_runs?workflow_id=12&range=0-10'
    );
    expect(request.request.method).toEqual('GET');
    request.flush([
      {
        id: '2',
        started_at: 9856987,
        state: 'Created',
        modules: [],
      },
    ]);
  });
});

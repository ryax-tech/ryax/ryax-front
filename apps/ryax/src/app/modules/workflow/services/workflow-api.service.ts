// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { Project } from '../../project/entities/index';
import { WorkflowRunAdapter } from '../../shared/entity/adapters';
import { WorkflowRunDto } from '../../shared/entity/dtos';
import { WorkflowAdapter } from '../entities/adapters/workflow.adapter';
import { WorkflowDto, WorkflowLightDto } from '../entities/dtos/workflow.dto';
import { Workflow, WorkflowLight } from '../entities/index';
import { saveAs } from 'file-saver';

@Injectable({
  providedIn: 'root',
})
export class WorkflowApiService {
  private baseUrl = '/api/studio';

  constructor(private http: HttpClient) {}

  public getWorkflows(): Observable<WorkflowLight[]> {
    return this.http
      .get<WorkflowLightDto[]>(this.baseUrl + '/workflows')
      .pipe(
        map((dtos) =>
          dtos.map((dto) => new WorkflowAdapter().adaptWorkflowLight(dto))
        )
      );
  }

  public getWorkflow(id: string): Observable<Workflow> {
    return this.http
      .get<WorkflowDto>(this.baseUrl + '/workflows/' + id)
      .pipe(map((dto) => new WorkflowAdapter().adaptWorkflow(dto)));
  }

  public getWorkflowRuns(id: string, pageIndex = 0, pageSize = 10) {
    const base = pageIndex * pageSize;
    const pages = base + '-' + (base + pageSize);
    return this.http
      .get<WorkflowRunDto[]>(
        '/api/runner/workflow_runs?workflow_id=' + id + '&range=' + pages,
        { observe: 'response' }
      )
      .pipe(
        map((response) => {
          return {
            // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
            list: response.body!.map((dto) =>
              new WorkflowRunAdapter().adapt(dto)
            ),
            // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
            total: +response.headers.get('content-range')!.split('/')[1],
          };
        })
      );
  }

  public deploy(workflowId: string) {
    return this.http.post<void>(
      this.baseUrl + `/workflows/${workflowId}/deploy`,
      null
    );
  }

  public stop(workflowId: string, graceful: boolean) {
    return this.http.post<void>(
      this.baseUrl + `/workflows/${workflowId}/stop`,
      null,
      { params: { graceful: graceful } }
    );
  }

  public copy(workflowId: string, name: string) {
    return this.http.post<{ workflow_id: string }>(
      this.baseUrl + '/workflows',
      {
        name: name + ' copy',
        from_workflow: workflowId,
      }
    );
  }

  public export(workflowId: string) {
    return this.http
      .get(this.baseUrl + `/workflows/${workflowId}/export`, {
        responseType: 'blob',
      })
      .pipe(
        tap((fileContent) => saveAs(fileContent, `workflow-${workflowId}.zip`))
      );
  }

  public delete(workflowId: string) {
    return this.http.delete<void>(this.baseUrl + `/workflows/${workflowId}`);
  }

  public getWorkflowResultsConfig(
    workflowId: string
  ): Observable<{ key: string; description: string }[]> {
    return this.http
      .get<
        { key: string; workflow_module_io_id: string; description: string }[]
      >(this.baseUrl + `/v2/workflows/${workflowId}/results`)
      .pipe(
        map((values) =>
          values.map((value) => ({
            key: value.key,
            description: value.description,
          }))
        )
      );
  }

  public getProject(): Observable<Project> {
    return this.http.get<Project[]>('/api/authorization/v2/projects').pipe(
      // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
      map((projects) => projects.find((project) => project.current)!)
    );
  }
}

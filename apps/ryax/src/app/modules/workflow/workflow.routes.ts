// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { WorkflowAlertsComponent } from './components/workflow-alerts/workflow-alerts.component';
import { WorkflowRunsComponent } from './components/workflow-runs/workflow-runs.component';
import { WorkflowOverviewComponent } from './components/workflow-overview/workflow-overview.component';
import { WorkflowComponent } from './components/workflow/workflow.component';
import { Routes } from '@angular/router';

export const workflowRoutes: Routes = [
  {
    path: ':workflowId',
    component: WorkflowComponent,
    children: [
      {
        path: 'overview',
        component: WorkflowOverviewComponent,
      },
      {
        path: 'runs',
        component: WorkflowRunsComponent,
      },
      {
        path: 'alerts',
        component: WorkflowAlertsComponent,
      },
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'overview',
      },
    ],
  },
  {
    path: '',
    pathMatch: 'full',
    redirectTo: '/dashboard',
  },
];

// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { ActionDto } from '../dtos/action.dto';
import { Action } from '../action';
import { ActionAdapter } from './actionAdapter';

describe('ModuleAdapter', () => {
  const adapter: ActionAdapter = new ActionAdapter();

  const dto: ActionDto = {
    id: '',
    module_id: '',
    technical_name: 'test1',
    name: '',
    version: '',
    description: '',
    kind: '',
    inputs: [],
    outputs: [],
  };

  const module: Action = {
    id: '',
    definitionId: '',
    name: 'test1',
    version: '',
    description: '',
  };

  const customNameDto: ActionDto = {
    id: '',
    module_id: '',
    technical_name: 'test1',
    name: '',
    custom_name: 'test2',
    version: '',
    description: '',
    kind: '',
    inputs: [],
    outputs: [],
  };

  const customNameModule: Action = {
    id: '',
    definitionId: '',
    name: 'test2',
    version: '',
    description: '',
  };

  it('should adapt ModuleDto into Module', () => {
    expect(adapter.adapt(dto)).toEqual(module);
  });

  it('should adapt ModuleDto into Module even with custom name', () => {
    expect(adapter.adapt(customNameDto)).toEqual(customNameModule);
  });
});

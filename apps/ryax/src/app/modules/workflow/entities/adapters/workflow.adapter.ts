// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { TitleCasePipe } from '@angular/common';
import { DateTime } from 'luxon';
import { DeploymentStatus } from '../../../shared/entity/index';
import { WorkflowModuleAdapter } from '../../../studio/entities/adapters/workflow-module/workflow-module.adapter';
import {
  WorkflowDetailedDto,
  WorkflowDto,
  WorkflowLightDto,
} from '../dtos/workflow.dto';
import { BuilderStateFiller, Workflow, WorkflowLight } from '../workflow';
import { ActionAdapter } from './actionAdapter';

export class WorkflowAdapter {
  public adaptWorkflow(dto: WorkflowDto): Workflow {
    const moduleAdapter = new ActionAdapter();

    return {
      id: dto.id,
      name: dto.name,
      description: dto.description,
      hasForm: dto.has_form,
      deploymentStatus: (dto.deployment_status === 'None'
        ? new TitleCasePipe().transform(dto.status)
        : dto.deployment_status) as DeploymentStatus,
      modules: dto.modules
        ? dto.modules.map((module) => moduleAdapter.adapt(module))
        : [],
      endpoint: dto.endpoint,
      deploymentError: dto.deployment_error ?? '',
    };
  }

  public adaptWorkflowLight(dto: WorkflowLightDto): WorkflowLight {
    return {
      id: dto.id,
      name: dto.name,
      description: dto.description,
      hasForm: dto.has_form,
      deploymentStatus: (dto.deployment_status === 'None'
        ? new TitleCasePipe().transform(dto.status)
        : dto.deployment_status) as DeploymentStatus,
      endpoint: dto.endpoint,
      categories: dto.categories,
      trigger: dto.trigger,
      parameters: dto.parameters,
    };
  }

  public adaptWorkflowToStudio(dto: WorkflowDetailedDto): BuilderStateFiller {
    return {
      workflowId: dto.id,
      workflowName: dto.name,
      lastSave: DateTime.now(),
      workflow: new WorkflowModuleAdapter().adaptModulesToStudio(dto.modules),
    };
  }
}

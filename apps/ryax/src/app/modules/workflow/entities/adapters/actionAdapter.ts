// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { ActionDto } from '../dtos/action.dto';
import { Action } from '../action';

export class ActionAdapter {
  public adapt(dto: ActionDto): Action {
    return {
      id: dto.id,
      definitionId: dto.module_id,
      name: dto.custom_name ? dto.custom_name : dto.name,
      version: dto.version,
      description: dto.description,
    };
  }
}

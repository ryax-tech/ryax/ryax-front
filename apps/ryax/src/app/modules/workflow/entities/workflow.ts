// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { DateTime } from 'luxon';
import { DeploymentStatus } from '../../shared/entity';
import { WorkflowStep } from '../../studio/entities/workflow-step';
import { Action } from './action';

export interface WorkflowLight {
  id: string;
  name: string;
  description: string;
  deploymentStatus: DeploymentStatus;
  hasForm: boolean;
  endpoint: string;
  trigger: string;
  categories: string[];
  parameters: { name: string; value: string }[];
}

export interface WorkflowFilters {
  deploymentStatus: string[];
  categories: string[];
  endpoint: string[];
  trigger: string[];
}

export interface Workflow {
  id: string;
  name: string;
  description: string;
  hasForm: boolean;
  deploymentStatus: DeploymentStatus;
  deploymentError: string;
  modules: Action[];
  endpoint: string;
}

export interface BuilderStateFiller {
  workflow: WorkflowStep[];
  workflowId: string;
  workflowName: string;
  lastSave: DateTime;
}

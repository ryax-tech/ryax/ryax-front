// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
export interface Action {
  id: string;
  definitionId: string;
  name: string;
  version: string;
  description: string;
}
export interface ActionOverview {
  definitionId: string;
  name: string;
  version: string;
  description: string;
  inputs: { [key: string]: string };
  outputs: { [key: string]: string };
}

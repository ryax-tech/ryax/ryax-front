// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
export interface ActionDto {
  id: string;
  module_id: string;
  custom_name?: string;
  name: string;
  technical_name: string;
  version: string;
  description: string;
  kind: string; // WorkflowModuleKindDto;
  inputs: InputDto[];
  outputs: InputDto[];
}

export interface InputDto {
  id: string;
  display_name: string;
  enum_values: string[];
  help: string;
  value?: string;
  type: string;
  optional?: boolean;
}
export interface ActionOverviewDto {
  module_id: string;
  name: string;
  version: string;
  description: string;
  inputs: { [key: string]: string };
  outputs: { [key: string]: string };
}

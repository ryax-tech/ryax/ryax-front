// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { DragDropModule } from '@angular/cdk/drag-drop';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { SharedModule } from '../shared/shared.module';
import { BuilderComponent } from './components/builder/builder.component';
import { BuildingComponent } from './components/building/building.component';
import { ConfigFormComponent } from './components/config-form/config-form.component';
import { ConfigPanelComponent } from './components/config-panel/config-panel.component';
import { ResultModalComponent } from './components/result-modal/result-modal.component';
import { BuilderEffects } from './state/effects/builder.effects';
import { ImportEffects } from './state/effects/import.effects';
import {
  StudioFeatureKey,
  StudioReducerProvider,
  StudioReducerToken,
} from './state/reducers';
import { ActionDescriptionComponent } from './components/action-description/action-description.component';
import { ConfigDynamicOutputsComponent } from './components/config-dynamic-outputs/config-dynamic-outputs.component';
import { CreateWorkflowComponent } from './components/create-workflow/create-workflow.component';
import { NgPipesModule } from 'ngx-pipes';
import { ActionDeploymentComponent } from './components/action-deployment/action-deployment.component';
import { SiteCardComponent } from './components/site-card/site-card.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    DragDropModule,
    StoreModule.forFeature(StudioFeatureKey, StudioReducerToken),
    EffectsModule.forFeature([BuilderEffects, ImportEffects]),
    SharedModule,
    NgPipesModule,
  ],
  declarations: [
    BuilderComponent,
    BuildingComponent,
    ConfigFormComponent,
    ConfigPanelComponent,
    ActionDescriptionComponent,
    ConfigDynamicOutputsComponent,
    CreateWorkflowComponent,
    ResultModalComponent,
    ActionDeploymentComponent,
    SiteCardComponent,
  ],
  providers: [StudioReducerProvider],
  exports: [CreateWorkflowComponent],
})
export class StudioModule {}

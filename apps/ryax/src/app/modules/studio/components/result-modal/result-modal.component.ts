// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Output,
  SimpleChanges,
} from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { BuilderFacade } from '../../state/facade/builder-facade.service';

@Component({
  selector: 'ryax-result-modal',
  templateUrl: './result-modal.component.pug',
  styleUrls: ['./result-modal.component.scss'],
})
export class ResultModalComponent implements OnInit, OnChanges, OnDestroy {
  public results$ = this.builderFacade.results$;
  public everyOutput$ = this.builderFacade.everyOutput$;
  public everyFileOrDirOutput$ = this.builderFacade.everyFileOrDirOutput$;
  @Input() public isVisible = false;
  @Output() public isVisibleChange: EventEmitter<boolean> =
    new EventEmitter<boolean>();
  public form = this.fb.group({});
  public resultType$ = this.builderFacade.resultsType$;
  public editionType: string | null = null;
  private subs = new Subscription();

  constructor(private builderFacade: BuilderFacade, private fb: FormBuilder) {}

  public ngOnInit(): void {
    this.resultType$.subscribe((type) => {
      this.editionType = type;
    });
    this.form.addControl(
      'resultType',
      this.fb.control(this.editionType, Validators.nullValidator)
    );
    this.form.get('resultType')?.valueChanges.subscribe((selectedType) => {
      if (selectedType != this.resultType$) {
        this.builderFacade.clearResults();
        for (const [key] of Object.entries(this.form.value)) {
          if (key.startsWith('result-')) {
            this.form.removeControl(key, { emitEvent: false });
          }
        }
        this.form.reset({ resultType: selectedType }, { emitEvent: false });
        this.editionType = selectedType;
      }
    });

    this.results$.subscribe((results) => {
      results.workflow_results.forEach((result, index) => {
        const controlId = 'result-' + index.toString();
        if (!this.form.get(controlId)) {
          this.form.addControl(
            controlId,
            this.fb.group({
              key: this.fb.control(result.key, Validators.required),
              value: this.fb.control(result.value, Validators.required),
            })
          );
          // FIXME: Lead to a lost of focus...
          //this.form.controls[controlId].valueChanges.subscribe((result) => {
          //  // this.builderFacade.setResult(index, result)
          //  console.log("test")
          //})
        }
      });
    });
  }

  public ngOnChanges(changes: SimpleChanges) {
    if (changes['isVisible'].currentValue === true) {
      this.builderFacade.getResults();
    }
  }

  public addResult() {
    this.builderFacade.addResult();
  }

  public removeResult(index: number) {
    this.form.removeControl('result-' + index);
    this.form.get('result-' + index)?.reset();
    this.builderFacade.removeResult(index);
  }

  public handleCancel() {
    this.isVisibleChange.emit(false);
  }

  public handleOk() {
    const results: { key: string; value: string }[] = [];
    for (const [key, value] of Object.entries(this.form.value)) {
      if (key.startsWith('result-')) {
        const val = value as { key: string; value: string };
        results.push(val);
      }
    }
    this.builderFacade.saveResults(results);
    this.isVisibleChange.emit(false);
  }

  public ngOnDestroy() {
    this.subs.unsubscribe();
  }
}

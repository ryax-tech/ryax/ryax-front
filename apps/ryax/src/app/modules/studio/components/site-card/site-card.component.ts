import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnDestroy,
  Output,
} from '@angular/core';
import { NodePool, Site } from '../../entities/sites';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ActionDeployConstraints } from '../../entities/workflow-action';
import { Subscription } from 'rxjs';

@Component({
  selector: 'ryax-site-card',
  templateUrl: './site-card.component.pug',
  styleUrls: ['./site-card.component.scss'],
})
export class SiteCardComponent implements OnChanges, OnDestroy {
  @Input({ required: true }) sites: Site[] | null = null;
  @Input({ required: true }) constraints: ActionDeployConstraints | null = null;
  @Output() constraintsSelectionUpdated = new EventEmitter<
    [NodePool[], Site[]]
  >();
  public selectionForm: FormGroup = this.formBuilder.group({});
  allNodePools: NodePool[] = [];
  private sub: Subscription | null = null;

  constructor(private formBuilder: FormBuilder) {}

  ngOnChanges(): void {
    if (this.sites != null && this.constraints != null) {
      // reset state on site update
      this.allNodePools = [];

      this.sites.map((site) => {
        this.allNodePools = this.allNodePools.concat(site.nodePools);

        // Get initial value for selected elements
        let initValue = false;
        if (this.constraints) {
          if (this.constraints.siteList) {
            initValue = this.constraints.siteList.includes(site.id);
          }
        }
        if (this.selectionForm.controls[site.id]) {
          this.selectionForm.controls[site.id].setValue(initValue, {
            emitEvent: false,
          });
        } else {
          this.selectionForm.addControl(site.id, new FormControl(initValue));
        }

        site.nodePools.map((nodePool: NodePool) => {
          // Get initial value for selected elements
          initValue = false;
          if (this.constraints) {
            if (this.constraints.nodePoolList) {
              initValue = this.constraints.nodePoolList.includes(nodePool.id);
            }
          }
          if (this.selectionForm.controls[nodePool.id]) {
            this.selectionForm.controls[nodePool.id].setValue(initValue, {
              emitEvent: false,
            });
          } else {
            this.selectionForm.addControl(
              nodePool.id,
              new FormControl(initValue)
            );
          }
        });
      });
      if (this.sub) {
        this.sub.unsubscribe();
      }
      this.sub = this.selectionForm.valueChanges.subscribe(() =>
        this.onUpdate()
      );
    }
  }

  onUpdate(): void {
    if (this.sites && this.constraints) {
      const selectedSites = this.sites.filter(
        (site) => this.selectionForm.value[site.id]
      );
      const selectedNodePools = this.allNodePools.filter(
        (nodePool) => this.selectionForm.value[nodePool.id]
      );
      this.constraintsSelectionUpdated.emit([selectedNodePools, selectedSites]);
    }
  }

  ngOnDestroy(): void {
    this.sub?.unsubscribe();
  }

  getDurationFormatted(timeInSec: number) {
    if (timeInSec) {
      return timeInSec + '\u00A0s';
    }
    return 'No limit';
  }
}

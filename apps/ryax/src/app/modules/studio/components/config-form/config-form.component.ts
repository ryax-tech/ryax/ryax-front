// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component, Input, OnChanges, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NzUploadChangeParam } from 'ng-zorro-antd/upload';
import { firstValueFrom, Subscription } from 'rxjs';
import {
  debounceTime,
  distinctUntilChanged,
  filter,
  withLatestFrom,
} from 'rxjs/operators';
import {
  ActionIODefinition,
  InputSection,
} from '../../entities/workflow-action';
import { BuilderFacade } from '../../state/facade/builder-facade.service';
import { WorkflowStep } from '../../entities/workflow-step';
import { VariablesFacade } from '../../../project/state/facade/variables.facade';

@Component({
  selector: 'ryax-config-form',
  templateUrl: './config-form.component.pug',
  styleUrls: ['./config-form.component.scss'],
})
export class ConfigFormComponent implements OnChanges, OnDestroy {
  @Input() public config!: ActionIODefinition[];
  @Input() public inputSection!: InputSection;

  public form: FormGroup = this.fb.group({});
  public previousOutputs$ = this.builderFacade.previousOutputs$;
  public isCurrentStepSource$ = this.builderFacade.isCurrentStepSource$;
  public ids$ = this.builderFacade.ids$;
  public projectVariables$ = this.variableFacade.variables$;
  private sub: Subscription = new Subscription();
  public passwordVisible = false;

  constructor(
    private fb: FormBuilder,
    private builderFacade: BuilderFacade,
    private variableFacade: VariablesFacade
  ) {}

  public ngOnChanges() {
    this.sub.unsubscribe();
    this.sub = new Subscription();
    this.createNewForm();
  }

  public handleChange(info: NzUploadChangeParam, inputId: string): void {
    if (info.file.status === 'done') {
      this.builderFacade.saveFileSuccess(inputId, info.file.name);
      this.form.get(inputId + '.value')?.setValue(info.file.name);
    } else if (info.file.status === 'error') {
      this.builderFacade.saveFileError(info.file.error);
    }
  }

  public removeFile(inputId: string) {
    this.builderFacade.removeFile(inputId);
    this.form.get(inputId + '.value')?.setValue(undefined);
  }

  public clearValue(inputId: string) {
    this.form.get(inputId + '.value')?.setValue(null);
  }

  public ngOnDestroy() {
    this.sub.unsubscribe();
  }

  private getValuesInState(currentStep: WorkflowStep) {
    let toReturn;
    if (this.inputSection == InputSection.addonsInputs) {
      toReturn = currentStep.panels[1].addonInputFormValues;
    } else {
      toReturn = currentStep.panels[1].inputFormValues;
    }
    return toReturn;
  }

  private createNewForm() {
    this.form = this.fb.group({});

    this.config.forEach(async (conf) => {
      // Create Form Control
      this.form.addControl(
        conf.id,
        this.fb.group({
          valueType: this.fb.control('static', Validators.required),
          value: this.fb.control(null, Validators.required),
        })
      );

      // Reset value when switch between reference or static
      this.sub.add(
        this.form
          .get(conf.id + '.valueType')
          ?.valueChanges.subscribe((value) => {
            this.form.get(conf.id + '.value')?.setValue(null);
            if (value == 'variable') {
              this.variableFacade.getVariables();
            }
          })
      );

      // Set value from state in control
      const currentStep = await firstValueFrom(this.builderFacade.currentStep$);
      const stateValues = this.getValuesInState(currentStep);
      if (stateValues[conf.id] !== undefined) {
        this.form
          .get(conf.id)
          ?.patchValue(stateValues[conf.id], { emitEvent: false });
      }
    });

    // Auto-save
    this.sub.add(
      this.form.valueChanges
        .pipe(
          distinctUntilChanged(
            (prev, curr) => JSON.stringify(prev) === JSON.stringify(curr)
          ),
          withLatestFrom(this.builderFacade.currentStep$),
          filter(
            ([values, currentStep]) =>
              JSON.stringify(values) !==
              JSON.stringify(this.getValuesInState(currentStep))
          ),
          debounceTime(500)
        )
        .subscribe((value) => {
          this.builderFacade.saveForm(
            value[0],
            this.inputSection,
            this.form.valid
          );
        })
    );
  }
}

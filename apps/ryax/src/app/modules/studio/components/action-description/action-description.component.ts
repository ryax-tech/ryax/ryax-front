// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import {
  Component,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  SimpleChanges,
} from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
import { WorkflowActionDetailed } from '../../entities/workflow-action';
import { BuilderFacade } from '../../state/facade/builder-facade.service';

@Component({
  selector: 'ryax-action-description',
  templateUrl: './action-description.component.pug',
  styleUrls: ['./action-description.component.scss'],
})
export class ActionDescriptionComponent
  implements OnInit, OnChanges, OnDestroy
{
  @Input() public action: WorkflowActionDetailed | undefined;

  public currentVersion = this.fb.control('');
  public sub = new Subscription();

  constructor(private fb: FormBuilder, private facade: BuilderFacade) {}

  public ngOnInit() {
    this.sub.add(
      this.currentVersion.valueChanges
        .pipe(
          map((version_name) => {
            const new_version = this.action?.versions.find(
              (version) => version.name === version_name
            );
            if (new_version && this.action?.version != new_version.name) {
              return new_version.id;
            } else return null;
          })
        )
        .subscribe((version_id) => {
          if (version_id) {
            this.facade.changeVersion(version_id);
          }
        })
    );

    if (this.action !== undefined) {
      this.currentVersion.setValue(this.action?.version);
    }
  }

  public ngOnChanges(changes: SimpleChanges) {
    this.currentVersion.patchValue(changes['action'].currentValue.version, {
      emitEvent: false,
    });
  }

  public ngOnDestroy() {
    this.sub.unsubscribe();
  }
}

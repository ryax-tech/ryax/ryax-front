// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component, Input, OnChanges, OnDestroy, OnInit } from '@angular/core';
import { FormArray, FormBuilder, Validators } from '@angular/forms';
import { lastValueFrom, Subscription } from 'rxjs';
import {
  debounceTime,
  distinctUntilChanged,
  filter,
  withLatestFrom,
} from 'rxjs/operators';
import { ActionOutput } from '../../entities/workflow-action';
import { BuilderFacade } from '../../state/facade/builder-facade.service';

@Component({
  selector: 'ryax-config-dynamic-outputs',
  templateUrl: './config-dynamic-outputs.component.pug',
  styleUrls: ['./config-dynamic-outputs.component.scss'],
})
export class ConfigDynamicOutputsComponent
  implements OnInit, OnChanges, OnDestroy
{
  @Input() public config!: ActionOutput[];
  @Input() public actionName!: string;

  public form = this.fb.group({});
  public outputTypes: string[] = [
    'string',
    'integer',
    'password',
    'longstring',
    'float',
    'boolean',
    'enum',
    'file',
    'directory',
  ];
  public originList: string[] = ['path', 'query', 'header', 'cookie', 'body'];
  private sub: Subscription | undefined;
  public originRequired = true;

  constructor(private fb: FormBuilder, private facade: BuilderFacade) {}

  public ngOnInit() {
    // FIXME this should come from the backend
    if (this.actionName == 'postgw') {
      this.originRequired = false;
    }
    this.createNewForm();
  }

  public ngOnChanges(): void {
    this.createOrUpdateFormControls();
  }

  public addOutput() {
    this.facade.addOutput();
  }

  public removeOutput(id: string) {
    this.facade.removeOutput(id);
  }

  public addValue(id: string) {
    (this.form.get(id + '.enumValues') as FormArray).push(this.fb.control(''), {
      emitEvent: false,
    });
  }

  public removeValue(id: string, index: number) {
    (this.form.get(id + '.enumValues') as FormArray).removeAt(index);
  }

  public ngOnDestroy() {
    this.sub?.unsubscribe();
  }

  public getEnumValuesForm(outputId: string) {
    return (this.form.get(outputId + '.enumValues') as FormArray).controls;
  }

  customTrackBy(index: number, item: ActionOutput): string {
    return item.id;
  }

  private createNewForm() {
    this.form = this.fb.group({});

    this.createOrUpdateFormControls();

    // Auto-save
    this.sub = this.form.valueChanges
      .pipe(
        distinctUntilChanged(
          (prev, curr) => JSON.stringify(prev) === JSON.stringify(curr)
        ),
        withLatestFrom(this.facade.currentStep$),
        filter(
          ([values, currentStep]) =>
            JSON.stringify(values) !==
            JSON.stringify(currentStep.panels[1].outputFormValues)
        ),
        debounceTime(500)
      )
      .subscribe(([values]) => {
        this.facade.saveDynamicForm(values, this.form.valid);
      });
  }

  private createOrUpdateFormControls() {
    this.config.forEach(async (actionOutputConfig) => {
      if (this.form.get(actionOutputConfig.id) == null) {
        // Create Form Control
        this.form.addControl(
          actionOutputConfig.id,
          this.fb.group({
            id: this.fb.control(actionOutputConfig.id, Validators.required),
            type: this.fb.control(actionOutputConfig.type, Validators.required),
            help: this.fb.control(actionOutputConfig.help, Validators.required),
            name: this.fb.control(actionOutputConfig.name, Validators.required),
            origin: this.fb.control(
              actionOutputConfig.origin,
              Validators.required
            ),
            optional: this.fb.control(
              actionOutputConfig.optional,
              Validators.required
            ),
            enumValues: this.fb.array(actionOutputConfig.enumValues ?? []),
          })
        );

        // Set value from state in control
        const currentStep = await lastValueFrom(this.facade.currentStep$);
        if (currentStep.panels[1].outputFormValues[actionOutputConfig.id]) {
          this.form
            .get(actionOutputConfig.id)
            ?.setValue(
              currentStep.panels[1].outputFormValues[actionOutputConfig.id],
              { emitEvent: false }
            );
        }
      }
      // Remove old controls
      const allOutputs = this.config.map((output) => output.id);
      const toRemove = [];
      for (const control in this.form.controls) {
        if (!allOutputs.includes(control)) {
          toRemove.push(control);
        }
      }
      for (const control of toRemove) {
        this.form.removeControl(control);
      }
    });
    this.form.updateValueAndValidity();
  }
}

// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { CdkDrag, CdkDragDrop } from '@angular/cdk/drag-drop';
import {
  AfterViewInit,
  Component,
  ElementRef,
  OnInit,
  QueryList,
  ViewChildren,
} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { WorkflowStep } from '../../entities/workflow-step';
import { BuilderFacade } from '../../state/facade/builder-facade.service';
import { firstValueFrom } from 'rxjs';

@Component({
  selector: 'ryax-builder',
  templateUrl: './builder.component.pug',
  styleUrls: ['./builder.component.scss'],
})
export class BuilderComponent implements OnInit, AfterViewInit {
  public workflowId = '';
  public editWorkflowName = false;
  public openModal = false;
  public workflowNameInput = '';
  public modulesDescriptions$ = this.builderFacade.modulesDescriptions$;
  public isAllSet$ = this.builderFacade.isAllSet$;
  public configLoading$ = this.builderFacade.configLoading$;
  public studioErrors$ = this.builderFacade.studioErrors$;

  @ViewChildren('ryaxTitleEdit') private ryaxTitleEdit!: QueryList<ElementRef>;

  constructor(
    public builderFacade: BuilderFacade,
    private route: ActivatedRoute
  ) {}

  public ngOnInit() {
    if (this.route.snapshot.url[0].path !== 'new') {
      this.workflowId = this.route.snapshot.url[0].path;
      this.builderFacade.loadWorkflow(this.route.snapshot.url[0].path);
    }
  }

  public ngAfterViewInit() {
    this.ryaxTitleEdit.changes.subscribe((list: QueryList<ElementRef>) => {
      if (list.length > 0) {
        list.first.nativeElement.focus();
      }
    });
  }

  public openConfig(stepIndex: number) {
    this.builderFacade.toStep(stepIndex);
  }

  public drop(event: CdkDragDrop<WorkflowStep[]>) {
    this.builderFacade.reorganize(event.previousIndex, event.currentIndex);
  }

  public isDisabled(
    isAllSet: boolean | null,
    workflow: WorkflowStep[] | null,
    index: number
  ) {
    return !isAllSet && index + 1 === workflow?.length;
  }

  public sortPredicate(index: number, item: CdkDrag<number>) {
    return (
      index !== 0 &&
      item.data !== 0 &&
      !item.dropContainer.getSortedItems()[index].disabled
    );
  }

  public async startEdit() {
    this.workflowNameInput = await firstValueFrom(
      this.builderFacade.workflowName$
    );
    this.editWorkflowName = true;
  }

  public validate() {
    this.editWorkflowName = false;
    this.builderFacade.editName(this.workflowNameInput);
  }

  public deploy() {
    this.builderFacade.deploy();
  }

  public moduleCompare(index: number, module: WorkflowStep) {
    return JSON.stringify(module.panels[0].originalValue) + module.name;
  }
}

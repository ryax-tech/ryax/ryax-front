import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ActionDeploymentComponent } from './action-deployment.component';

describe('ActionDeploymentComponent', () => {
  let component: ActionDeploymentComponent;
  let fixture: ComponentFixture<ActionDeploymentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ActionDeploymentComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ActionDeploymentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

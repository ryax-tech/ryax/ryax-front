import { Component, Input, OnChanges, OnDestroy, OnInit } from '@angular/core';
import { WorkflowActionDetailed } from '../../entities/workflow-action';
import { FormBuilder, FormGroup } from '@angular/forms';
import { BuilderFacade } from '../../state/facade/builder-facade.service';
import { NodePool, Site } from '../../entities/sites';
import { Subscription } from 'rxjs';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';

@Component({
  selector: 'ryax-action-deployment',
  templateUrl: './action-deployment.component.pug',
  styleUrls: ['./action-deployment.component.scss'],
})
export class ActionDeploymentComponent implements OnChanges, OnDestroy, OnInit {
  @Input({ required: true }) action: WorkflowActionDetailed | null = null;
  @Input({ required: true }) sites: Site[] | null = null;
  filteredSites: Site[] | null = null;
  subscription: Subscription | null = null;
  subscription2: Subscription | null = null;

  siteTypes = ['ANY', 'KUBERNETES', 'SLURM_SSH'];

  public currentSiteType = this.fb.control('');
  public nodePoolList: NodePool[] = [];
  public siteList: Site[] = [];
  public objectivesForm: FormGroup = this.fb.group({
    energy: 0,
    performance: 0,
    cost: 0,
  });

  constructor(private fb: FormBuilder, private facade: BuilderFacade) {}

  ngOnInit(): void {
    this.subscription = this.currentSiteType.valueChanges
      .pipe(distinctUntilChanged())
      .subscribe((value) => {
        this.applyFilter(value);
        this.updateDeployConstraints(this.nodePoolList, this.siteList);
      });
    this.subscription2 = this.objectivesForm.valueChanges
      .pipe(debounceTime(1000))
      .subscribe(() => this.onObjectivesUpdated());
  }

  ngOnDestroy(): void {
    this.subscription?.unsubscribe();
    this.subscription2?.unsubscribe();
  }

  ngOnChanges(): void {
    if (this.action && this.action.constraints) {
      if (this.action.kind == 'Source') {
        this.currentSiteType.setValue('KUBERNETES', { emitEvent: false });
      } else {
        let new_value = this.action.constraints?.siteTypeList[0];
        if (!new_value) {
          new_value = 'ANY';
        }
        this.currentSiteType.setValue(new_value, { emitEvent: false });
      }
    }
    this.applyFilter(this.currentSiteType.value);

    if (this.action && this.action.objectives) {
      this.objectivesForm.controls['energy'].setValue(
        this.action.objectives.energy,
        { emitEvent: false }
      );
      this.objectivesForm.controls['performance'].setValue(
        this.action.objectives.performance,
        { emitEvent: false }
      );
      this.objectivesForm.controls['cost'].setValue(
        this.action.objectives.cost,
        { emitEvent: false }
      );
    }
  }

  applyFilter(siteType: string | null) {
    if (siteType && this.sites) {
      if (siteType == 'ANY') {
        this.filteredSites = this.sites;
      } else {
        this.filteredSites = this.sites.filter((site) => site.type == siteType);
      }
    }
  }

  countNodePool(sites: Site[] | null) {
    if (sites === null) {
      return 0;
    }

    return sites.reduce((nbNodePool, site) => {
      return nbNodePool + site.nodePools.length;
    }, 0);
  }

  updateDeployConstraints(nodePools: NodePool[], sites: Site[]) {
    if (nodePools != null && sites != null) {
      let siteType: string[] = [];
      if (this.currentSiteType && this.currentSiteType.value) {
        if (this.currentSiteType.value == 'ANY') {
          siteType = [];
        } else {
          siteType = [this.currentSiteType.value];
        }
      }
      this.nodePoolList = nodePools;
      this.siteList = sites;
      this.facade.setConstraints({
        nodePoolList: nodePools.map((nodePool: NodePool) => nodePool.id),
        siteList: sites.map((site: Site) => site.id),
        siteTypeList: siteType,
        archList: [], // FIXME: add arch list when this is relevant. It's already manage by the backend
      });
    }
  }

  onObjectivesUpdated(): void {
    if (this.action && this.action.objectives) {
      this.facade.setObjectives(this.objectivesForm.value);
    }
  }

  allowedSiteTypes(action: WorkflowActionDetailed): string[] {
    if (action.kind == 'Source') {
      return ['KUBERNETES'];
    }
    return this.siteTypes;
  }
}

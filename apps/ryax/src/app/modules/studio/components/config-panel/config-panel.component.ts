// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component, OnInit } from '@angular/core';
import {
  WorkflowActionDetailed,
  WorkflowActionLight,
} from '../../entities/workflow-action';
import { PanelStatus } from '../../entities/workflow-step';
import { BuilderFacade } from '../../state/facade/builder-facade.service';
import { VariablesFacade } from '../../../project/state/facade/variables.facade';
import { firstValueFrom } from 'rxjs';

@Component({
  selector: 'ryax-config-panel',
  templateUrl: './config-panel.component.pug',
  styleUrls: ['./config-panel.component.scss'],
})
export class ConfigPanelComponent implements OnInit {
  public panelsDisplay: boolean[] = [true, false];

  public isSource = true;

  public sourceList$ = this.builderFacade.sourceList$;
  public processList$ = this.builderFacade.processList$;
  public sites$ = this.builderFacade.sites$;

  public searchTerm = '';

  public currentStep$ = this.builderFacade.currentStep$;
  public currentIndex$ = this.builderFacade.currentIndex$;
  public currentPanelIndex$ = this.builderFacade.currentPanelIndex$;

  public currentStepNameInput = '';
  public editStepName = false;

  constructor(
    public builderFacade: BuilderFacade,
    public variableFacade: VariablesFacade
  ) {}

  public ngOnInit(): void {
    this.builderFacade.statusList$.subscribe((statusList: PanelStatus[]) => {
      const indexOpen = statusList.findIndex((status) => status !== 'finish');
      this.panelsDisplay = statusList.map(
        (value, index) =>
          index === indexOpen ||
          (indexOpen === -1 && index === statusList.length - 1)
      );
    });

    this.builderFacade.currentIndex$.subscribe((index: number) => {
      this.isSource = index === 0;
    });

    this.builderFacade.getModules();
    this.variableFacade.getVariables();
    this.builderFacade.loadSites();
  }

  public selectModule(module: WorkflowActionLight, newModule: boolean) {
    if (newModule) {
      this.builderFacade.addModule(module.id);
    } else {
      this.builderFacade.updateModule(module.id);
    }
    this.panelsDisplay = [false, true];
    this.editStepName = false;
  }

  public updatePanelIndex(index: number) {
    this.builderFacade.updatePanelIndex(index);
    this.editStepName = false;
  }

  public removeStep() {
    this.builderFacade.removeCurrentStep();
    this.editStepName = false;
  }

  public async startEdit() {
    this.editStepName = true;
    this.currentStepNameInput = (await firstValueFrom(this.currentStep$)).name;
  }

  public validate() {
    this.editStepName = false;
    this.builderFacade.saveActionName(this.currentStepNameInput);
  }

  public getDynamicOutputs(originalValue: WorkflowActionDetailed) {
    // Put latest created element first
    return [...originalValue.dynamicOutputs].reverse();
  }
}

// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { moveItemInArray } from '@angular/cdk/drag-drop';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { WorkflowAdapter } from '../../../workflow/entities/adapters/workflow.adapter';
import { WorkflowDetailedDto } from '../../../workflow/entities/dtos/workflow.dto';
import { BuilderStateFiller } from '../../../workflow/entities';
import { ModuleConfigAdapter } from '../../entities/adapters/module-config/module-config.adapter';
import { ModuleLinkAdapter } from '../../entities/adapters/module-link/module-link.adapter';
import { StudioErrorAdapter } from '../../entities/adapters/studio-error/studio-error.adapter';
import { WorkflowModuleAdapter } from '../../entities/adapters/workflow-module/workflow-module.adapter';
import { StudioErrorDto } from '../../entities/dtos/studio-error.dto';
import { WorkflowModuleDetailedDto } from '../../entities/dtos/workflow-module.dto';
import { StudioError } from '../../entities/studio-error';
import {
  ActionDeployConstraints,
  ActionDeployObjectives,
  ActionIODefinition,
  ActionOutput,
  InputSection,
  WorkflowActionDetailed,
  WorkflowActionLight,
} from '../../entities/workflow-action';
import { FormValues, OutputFormValues } from '../../entities/workflow-step';
import { WorkflowResultConfig } from '../../entities/workflow-results';
import { WorkflowResultConfigDto } from '../../entities/dtos/workflow-results.dto';
import { WorkflowResultsAdapter } from '../../entities/adapters/workflow-results.adapter';
import { Site } from '../../entities/sites';
import { SitesDto } from '../../entities/dtos/sites';
import { SiteAdapter } from '../../entities/adapters/site.adapter';

@Injectable({
  providedIn: 'root',
})
export class BuilderApiService {
  constructor(private http: HttpClient) {}

  public create() {
    return this.http.post<{ workflow_id: string }>('/api/studio/workflows', {
      name: 'My new workflow',
    });
  }

  public loadFullWorkflowForStudio(
    workflowId: string
  ): Observable<BuilderStateFiller> {
    return this.http
      .get<WorkflowDetailedDto>('/api/studio/v2/workflows/' + workflowId)
      .pipe(
        map((workflowDto: WorkflowDetailedDto) => {
          return new WorkflowAdapter().adaptWorkflowToStudio(workflowDto);
        })
      );
  }

  public loadSites(): Observable<Site[]> {
    return this.http.get<SitesDto>('/api/runner/sites').pipe(
      map(({ sites }) => {
        return new SiteAdapter().adapt(sites);
      })
    );
  }

  public getModuleList(): Observable<
    [WorkflowActionLight[], WorkflowActionLight[]]
  > {
    return this.http.get<WorkflowActionLight[]>('/api/studio/modules').pipe(
      map((modules) => {
        const sources = modules.filter((module) => module.kind === 'Source');
        const processes = modules.filter((module) => module.kind !== 'Source');
        return [sources, processes];
      })
    );
  }

  public addModuleToWorkflow(
    moduleDefId: string,
    workflowId: string,
    prevModuleId: string | undefined
  ): Observable<{ module: WorkflowActionDetailed; defaultValues: FormValues }> {
    return this.http
      .post<WorkflowModuleDetailedDto>(
        '/api/studio/v2/workflows/' + workflowId + '/modules',
        {
          module_definition_id: moduleDefId,
          parent_workflow_module_id: prevModuleId,
        }
      )
      .pipe(
        map((module) => ({
          module: new WorkflowModuleAdapter().adaptDetailed(module),
          defaultValues: new WorkflowModuleAdapter().adaptInputs(
            module.inputs.concat(module.addons_inputs)
          ),
        }))
      );
  }

  public updateModuleInWorkflow(
    moduleDefId: string,
    workflowId: string,
    currentModuleId: string | undefined
  ): Observable<{ module: WorkflowActionDetailed; defaultValues: FormValues }> {
    return this.http
      .post<WorkflowModuleDetailedDto>(
        '/api/studio/v2/workflows/' + workflowId + '/modules',
        {
          module_definition_id: moduleDefId,
          replace_workflow_module_id: currentModuleId,
        }
      )
      .pipe(
        map((module) => ({
          module: new WorkflowModuleAdapter().adaptDetailed(module),
          defaultValues: new WorkflowModuleAdapter().adaptInputs(
            module.inputs.concat(module.addons_inputs)
          ),
        }))
      );
  }

  public removeModuleFromWorkflow(
    workflowId: string,
    moduleId: string | undefined
  ): Observable<void> {
    return this.http.delete<void>(
      '/api/studio/workflows/' + workflowId + '/modules/' + moduleId
    );
  }

  public setModulesLinks(
    workflowId: string,
    moduleIds: string[],
    indexes: { currentIndex: number; previousIndex: number }
  ): Observable<{
    workflow: BuilderStateFiller;
    indexes: { currentIndex: number; previousIndex: number };
  }> {
    moveItemInArray(moduleIds, indexes.previousIndex, indexes.currentIndex);
    return this.http
      .put<void>(
        '/api/studio/v2/workflows/' + workflowId + '/links',
        new ModuleLinkAdapter().adapt(moduleIds)
      )
      .pipe(
        switchMap(() => this.loadFullWorkflowForStudio(workflowId)),
        map((fullWorkflow) => ({ workflow: fullWorkflow, indexes }))
      );
  }

  public updateActionInputsConfiguration(
    form: FormValues,
    inputSection: InputSection,
    workflowId: string,
    moduleId: string,
    moduleConf: {
      customName: string;
      inputs: ActionIODefinition[] | undefined;
      addonsInputs: ActionIODefinition[] | undefined;
    }
  ): Observable<void> {
    return this.http.put<void>(
      '/api/studio/v2/workflows/' + workflowId + '/modules/' + moduleId,
      new ModuleConfigAdapter().adaptInputs(form, inputSection, moduleConf)
    );
  }

  public updateActionDynamicOutputsConfiguration(
    form: OutputFormValues,
    workflowId: string,
    moduleId: string,
    moduleConf: { customName: string; outputs: ActionOutput[] | undefined }
  ): Observable<void> {
    return this.http.put<void>(
      '/api/studio/v2/workflows/' + workflowId + '/modules/' + moduleId,
      new ModuleConfigAdapter().adaptOutput(form, moduleConf)
    );
  }

  public updateModuleName(workflowId: string, moduleId: string, name: string) {
    return this.http.put<void>(
      '/api/studio/workflows/' + workflowId + '/modules/' + moduleId,
      { custom_name: name }
    );
  }

  public editName(name: string, moduleId: string): Observable<string> {
    return this.http
      .put<void>('/api/studio/workflows/' + moduleId, { name })
      .pipe(map(() => name));
  }

  public deploy(workflowId: string) {
    return this.http
      .post(`/api/studio/workflows/${workflowId}/deploy`, null)
      .pipe(map(() => workflowId));
  }

  public addOutput(ids: string[]) {
    const body = {
      display_name: '',
      enum_values: [],
      help: '',
      technical_name: 'new-input',
      type: 'string',
      origin: null,
    };
    return this.http
      .post(`/api/studio/workflows/${ids[0]}/modules/${ids[1]}/outputs`, body)
      .pipe(map(() => ids[0]));
  }

  public removeOutput(inputId: string, workflowId: string, moduleId: string) {
    return this.http
      .delete(
        `/api/studio/workflows/${workflowId}/modules/${moduleId}/outputs/${inputId}`
      )
      .pipe(map(() => workflowId));
  }

  public removeFile(inputId: string, workflowId: string, moduleId: string) {
    return this.http
      .delete(
        `/api/studio/workflows/${workflowId}/modules/${moduleId}/inputs/${inputId}/file`
      )
      .pipe(map(() => ({ inputId })));
  }

  public getResults(workflowId: string): Observable<WorkflowResultConfig> {
    return this.http
      .get<WorkflowResultConfigDto>(
        `/api/studio/v2/workflows/${workflowId}/run-results`
      )
      .pipe(map((results) => new WorkflowResultsAdapter().adapt(results)));
  }

  public saveResults(
    workflowId: string,
    results: { key: string; value: string }[]
  ) {
    return this.http.put(
      `/api/studio/v2/workflows/${workflowId}/results`,
      results.map(({ key, value }) => ({ key, workflow_module_io_id: value }))
    );
  }

  public getStudioErrors(workflowId: string): Observable<StudioError[]> {
    return this.http
      .get<StudioErrorDto[]>(`/api/studio/workflows/${workflowId}/errors`)
      .pipe(map((errors) => new StudioErrorAdapter().adapt(errors)));
  }

  public changeVersion(
    workflowId: string,
    workflowModuleId: string,
    newVersionModuleId: string
  ) {
    return this.http
      .post<{ workflow_module_id: string; removed_results: string[] }>(
        `/api/studio/workflows/${workflowId}/modules/${workflowModuleId}/change_version`,
        { module_id: newVersionModuleId }
      )
      .pipe(
        map(({ removed_results }) => ({ removedResults: removed_results }))
      );
  }

  setActionDeployConstraints(
    workflowId: string,
    workflowModuleId: string,
    actionDeployConstraints: ActionDeployConstraints
  ) {
    return this.http.put(
      `/api/studio/v2/workflows/${workflowId}/modules/${workflowModuleId}/constraints`,
      {
        site_list: actionDeployConstraints.siteList,
        site_type_list: actionDeployConstraints.siteTypeList,
        arch_list: actionDeployConstraints.archList,
        node_pool_list: actionDeployConstraints.nodePoolList,
      }
    );
  }

  setActionDeployObjectives(
    workflowId: string,
    workflowModuleId: string,
    actionDeployObjectives: ActionDeployObjectives
  ) {
    return this.http.put(
      `/api/studio/v2/workflows/${workflowId}/modules/${workflowModuleId}/objectives`,
      {
        energy: actionDeployObjectives.energy,
        performance: actionDeployObjectives.performance,
        cost: actionDeployObjectives.cost,
      }
    );
  }
}

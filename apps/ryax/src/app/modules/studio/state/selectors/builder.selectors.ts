// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { createFeatureSelector, createSelector } from '@ngrx/store';
import { BuilderState, StudioFeatureKey, StudioState } from '../reducers';

const selectBuilderFn = (state: StudioState) => state.builder;
const selectConfigLoadingFn = (state: BuilderState) => state.configLoading;
const selectLastSaveFn = (state: BuilderState) => state.lastSave;
const selectWorkflowNameFn = (state: BuilderState) => state.workflowName.value;
const selectWorkflowNameLoadingFn = (state: BuilderState) =>
  state.workflowName.loading;
const selectWorkflowIdFn = (state: BuilderState) => state.workflowId;
const selectWorkflowAndPreviousIdFn = (state: BuilderState) =>
  [
    state.workflowId,
    state.currentStepIndex >= 1 && state.workflow[state.currentStepIndex - 1]
      ? state.workflow[state.currentStepIndex - 1].panels[0].originalValue?.id
      : undefined,
  ] as [string, string | undefined];
const selectWorkflowFn = (state: BuilderState) => state.workflow;
const selectCurrentStepFn = (state: BuilderState) =>
  state.workflow[state.currentStepIndex];
const selectCurrentIndexFn = (state: BuilderState) => state.currentStepIndex;
const selectCurrentPanelIndexFn = (state: BuilderState) =>
  state.workflow[state.currentStepIndex].currentPanelIndex;
const selectSourceListFn = (state: BuilderState) => state.triggerList;
const selectProcessListFn = (state: BuilderState) => state.actionList;
const selectStatusListFn = (state: BuilderState) => {
  if (!state.workflow[state.currentStepIndex]) {
    return [];
  } else {
    return state.workflow[state.currentStepIndex].panels.map(
      (panel) => panel.status
    );
  }
};
const selectDeployableFn = (state: BuilderState) =>
  state.workflow.reduce((prev, step) => step.status === 'READY' && prev, true);
const selectPreviousOutputsFn = (state: BuilderState) => {
  return state.workflow
    .filter((step, i) => i < state.currentStepIndex)
    .map((step, index) => ({
      groupName: (step.panels[0].originalValue?.name ?? '') + ` ${index}`,
      values:
        step.panels[0].originalValue?.outputs
          .concat(step.panels[0].originalValue?.dynamicOutputs)
          .map((input) => ({
            name: input.name,
            value: input.id,
            type: input.type,
          })) ?? [],
    }))
    .filter((optionGroup) => optionGroup.values.length > 0);
};

const selectIsCurrentStepSourceFn = (state: BuilderState) =>
  state.currentStepIndex === 0;
const selectToBuildFn = (state: BuilderState) => ({
  name: state.workflowName.value,
  workflow: state.workflow,
});
const selectModuleDynamicOutputFn = (state: BuilderState) =>
  state.workflow[state.currentStepIndex].panels[0].originalValue
    ?.dynamicOutputs;
const selectIdsFn = (state: BuilderState) =>
  [
    state.workflowId,
    state.workflow[state.currentStepIndex].panels[0].originalValue?.id ?? '',
  ] as [string, string];
const selectWorkflowIdWithModuleIdsFn = (state: BuilderState) =>
  [
    state.workflowId,
    state.workflow.map((step) => step.panels[0].originalValue?.id ?? ''),
  ] as [string, string[]];
const selectModulesDescriptionsFn = (state: BuilderState) => {
  const allModulesOutputs = state.workflow.map((step, index) => {
    const modulesOutputs = step.panels[0].originalValue?.outputs.concat(
      step.panels[0].originalValue?.dynamicOutputs
    );
    return {
      groupName: (step.panels[0].originalValue?.name ?? '') + ` ${index}`,
      outputs: modulesOutputs ?? [],
    };
  });

  return state.workflow.map((step) => {
    let description = '';

    if (step.panels[0].originalValue) {
      const mainValue =
        step.panels[1].inputFormValues[step.panels[0].originalValue.mainValue];

      if (mainValue) {
        description += mainValue.valueType === 'link' ? 'Data from ' : '';

        description += allModulesOutputs.reduce((final, outputGroup) => {
          const index = outputGroup.outputs.findIndex(
            (output) => output.id === mainValue.value
          );
          if (index >= 0) {
            return `${outputGroup.outputs[index].name} in ${outputGroup.groupName}`;
          } else {
            return final;
          }
        }, '');
      }
      if (!description) {
        description = step.panels[0].originalValue.description;
      }
    }

    return description;
  });
};
const selectIsAllSetFn = (state: BuilderState) =>
  state.workflow.reduce(
    (isAllSet, step) => isAllSet && !!step.panels[0].originalValue,
    true
  );
const selectModuleConfigFn = (state: BuilderState) => {
  const currentModule = state.workflow[state.currentStepIndex];

  return {
    customName: currentModule.name,
    inputs: currentModule.panels[0].originalValue?.inputs,
    addonsInputs: currentModule.panels[0].originalValue?.addonsInputs,
    outputs: currentModule.panels[0].originalValue?.dynamicOutputs,
  };
};

const selectResultsFn = (state: BuilderState) => state.results;
const selectResultsTypeFn = (state: BuilderState) => state.results.type;

const selectEveryOutputFn = (state: BuilderState) => {
  return state.workflow.reduce(
    (result: { value: string; label: string }[], step) => {
      if (step.panels[0].originalValue) {
        const allOutputs = step.panels[0].originalValue.outputs
          .filter(
            (output) => output.type != 'file' && output.type != 'directory'
          )
          .concat(step.panels[0].originalValue.dynamicOutputs);
        return [
          ...result,
          ...allOutputs.map((output) => ({
            value: output.id,
            label:
              'Output "' +
              output.name +
              '" of "' +
              step.panels[0].originalValue?.name +
              '"',
          })),
        ];
      }
      return result;
    },
    []
  );
};
const selectFileOrDirOutputFn = (state: BuilderState) => {
  return state.workflow.reduce(
    (result: { value: string; label: string }[], step) => {
      if (step.panels[0].originalValue) {
        const allOutputs = step.panels[0].originalValue.outputs
          .concat(step.panels[0].originalValue.dynamicOutputs)
          .filter(
            (output) => output.type === 'file' || output.type === 'directory'
          );
        return [
          ...result,
          ...allOutputs.map((output) => ({
            value: output.id,
            label:
              'Output "' +
              output.name +
              '" of "' +
              step.panels[0].originalValue?.name +
              '"',
          })),
        ];
      }
      return result;
    },
    []
  );
};
const selectStudioErrorsFn = (state: BuilderState) => state.studioError;
const selectSitesFn = (state: BuilderState) => state.sites;

export const selectStudioState =
  createFeatureSelector<StudioState>(StudioFeatureKey);
export const selectBuilderState = createSelector(
  selectStudioState,
  selectBuilderFn
);
export const selectConfigLoading = createSelector(
  selectBuilderState,
  selectConfigLoadingFn
);
export const selectLastSave = createSelector(
  selectBuilderState,
  selectLastSaveFn
);
export const selectWorkflowName = createSelector(
  selectBuilderState,
  selectWorkflowNameFn
);
export const selectWorkflowNameLoading = createSelector(
  selectBuilderState,
  selectWorkflowNameLoadingFn
);
export const selectWorkflowId = createSelector(
  selectBuilderState,
  selectWorkflowIdFn
);
export const selectWorkflowAndPreviousId = createSelector(
  selectBuilderState,
  selectWorkflowAndPreviousIdFn
);
export const selectWorkflow = createSelector(
  selectBuilderState,
  selectWorkflowFn
);
export const selectCurrentIndex = createSelector(
  selectBuilderState,
  selectCurrentIndexFn
);
export const selectCurrentStep = createSelector(
  selectBuilderState,
  selectCurrentStepFn
);
export const selectCurrentPanelIndex = createSelector(
  selectBuilderState,
  selectCurrentPanelIndexFn
);
export const selectSourceList = createSelector(
  selectBuilderState,
  selectSourceListFn
);
export const selectProcessList = createSelector(
  selectBuilderState,
  selectProcessListFn
);
export const selectStatusList = createSelector(
  selectBuilderState,
  selectStatusListFn
);
export const selectDeployable = createSelector(
  selectBuilderState,
  selectDeployableFn
);
export const selectPreviousOutputs = createSelector(
  selectBuilderState,
  selectPreviousOutputsFn
);
export const selectIsCurrentStepSource = createSelector(
  selectBuilderState,
  selectIsCurrentStepSourceFn
);
export const selectToBuild = createSelector(
  selectBuilderState,
  selectToBuildFn
);
export const selectModuleDynamicOutput = createSelector(
  selectBuilderState,
  selectModuleDynamicOutputFn
);
export const selectIds = createSelector(selectBuilderState, selectIdsFn);
export const selectWorkflowIdWithModuleIds = createSelector(
  selectBuilderState,
  selectWorkflowIdWithModuleIdsFn
);
export const selectModulesDescriptions = createSelector(
  selectBuilderState,
  selectModulesDescriptionsFn
);
export const selectIsAllSet = createSelector(
  selectBuilderState,
  selectIsAllSetFn
);
export const selectModuleConfig = createSelector(
  selectBuilderState,
  selectModuleConfigFn
);
export const selectResults = createSelector(
  selectBuilderState,
  selectResultsFn
);
export const selectResultsType = createSelector(
  selectBuilderState,
  selectResultsTypeFn
);
export const selectEveryOutput = createSelector(
  selectBuilderState,
  selectEveryOutputFn
);
export const selectFileOrDirOutput = createSelector(
  selectBuilderState,
  selectFileOrDirOutputFn
);
export const selectStudioErrors = createSelector(
  selectBuilderState,
  selectStudioErrorsFn
);
export const selectSites = createSelector(selectBuilderState, selectSitesFn);

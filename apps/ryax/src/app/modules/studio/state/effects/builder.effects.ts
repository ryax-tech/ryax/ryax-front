// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { of } from 'rxjs';
import {
  catchError,
  map,
  switchMap,
  tap,
  withLatestFrom,
} from 'rxjs/operators';
import { BuilderStateFiller } from '../../../workflow/entities';
import { BuilderApiService } from '../../services/builder/builder-api.service';
import { BuilderActions } from '../actions';
import { BuilderState } from '../reducers';
import {
  selectIds,
  selectModuleConfig,
  selectWorkflowAndPreviousId,
  selectWorkflowId,
  selectWorkflowIdWithModuleIds,
} from '../selectors/builder.selectors';
import { NzNotificationService } from 'ng-zorro-antd/notification';
import { StudioError } from '../../entities/studio-error';
import { Site } from '../../entities/sites';

@Injectable()
export class BuilderEffects {
  createWorkflow$ = createEffect(() =>
    this.actions$.pipe(
      ofType(BuilderActions.create),
      switchMap(() => this.builderService.create()),
      switchMap((response) =>
        this.builderService
          .loadFullWorkflowForStudio(response.workflow_id)
          .pipe(
            map((workflow) => {
              this.router.navigateByUrl('/studio/' + workflow.workflowId);
              return BuilderActions.createSuccess(workflow);
            }),
            catchError((err: HttpErrorResponse) => {
              return of(BuilderActions.createError(err));
            })
          )
      )
    )
  );

  loadWorkflow$ = createEffect(() =>
    this.actions$.pipe(
      ofType(BuilderActions.loadWorkflow),
      switchMap(({ workflowId }) =>
        this.builderService.loadFullWorkflowForStudio(workflowId).pipe(
          map((workflow: BuilderStateFiller) => {
            return BuilderActions.loadWorkflowSuccess(workflow);
          }),
          catchError((err: HttpErrorResponse) => {
            return of(BuilderActions.loadWorkflowError(err));
          })
        )
      )
    )
  );

  loadSites$ = createEffect(() =>
    this.actions$.pipe(
      ofType(BuilderActions.loadSites),
      switchMap(() =>
        this.builderService.loadSites().pipe(
          map((sites: Site[]) => {
            return BuilderActions.loadSitesSuccess(sites);
          }),
          catchError((err: HttpErrorResponse) => {
            return of(BuilderActions.loadSitesError(err));
          })
        )
      )
    )
  );

  deploy$ = createEffect(() =>
    this.actions$.pipe(
      ofType(BuilderActions.deploy),
      withLatestFrom(this.store$.select(selectWorkflowId)),
      switchMap(([, workflowId]) =>
        this.builderService.deploy(workflowId).pipe(
          map((workflowId: string) => {
            this.router.navigateByUrl('/workflow/' + workflowId);
            return BuilderActions.deploySuccess();
          }),
          catchError((err: HttpErrorResponse) => {
            return of(BuilderActions.deployError(err));
          })
        )
      )
    )
  );

  getModules$ = createEffect(() =>
    this.actions$.pipe(
      ofType(BuilderActions.getModules),
      switchMap(() =>
        this.builderService.getModuleList().pipe(
          map((modules) => {
            return BuilderActions.moduleSuccess(modules);
          }),
          catchError((err: HttpErrorResponse) => {
            return of(BuilderActions.moduleError(err));
          })
        )
      )
    )
  );

  addModuleToWorkflow$ = createEffect(() =>
    this.actions$.pipe(
      ofType(BuilderActions.choseStepModule),
      withLatestFrom(this.store$.select(selectWorkflowAndPreviousId)),
      switchMap(([{ selectedModuleId }, ids]) =>
        this.builderService.addModuleToWorkflow(selectedModuleId, ...ids).pipe(
          switchMap(({ module, defaultValues }) => {
            return [
              BuilderActions.choseStepModuleSuccess(module, defaultValues),
            ];
          }),
          catchError((err: HttpErrorResponse) => {
            return of(BuilderActions.choseStepModuleError(err));
          })
        )
      )
    )
  );

  updateModuleInWorkflow$ = createEffect(() =>
    this.actions$.pipe(
      ofType(BuilderActions.updateStepSelection),
      withLatestFrom(this.store$.select(selectIds)),
      switchMap(([{ selectedModuleId }, ids]) =>
        this.builderService
          .updateModuleInWorkflow(selectedModuleId, ...ids)
          .pipe(
            map(({ module, defaultValues }) => {
              return BuilderActions.updateStepSelectionSuccess(
                module,
                defaultValues
              );
            }),
            catchError((err: HttpErrorResponse) => {
              return of(BuilderActions.updateStepSelectionError(err));
            })
          )
      )
    )
  );

  updateStepConfiguration$ = createEffect(() =>
    this.actions$.pipe(
      ofType(BuilderActions.updateStepConfiguration),
      withLatestFrom(this.store$.select(selectIds)),
      withLatestFrom(
        this.store$.select(selectModuleConfig),
        ([{ form, inputSection, valid }, ids], moduleConf) => ({
          form,
          inputSection,
          valid,
          ids,
          moduleConf,
        })
      ),
      switchMap(({ form, inputSection, valid, ids, moduleConf }) =>
        this.builderService
          .updateActionInputsConfiguration(
            form,
            inputSection,
            ...ids,
            moduleConf
          )
          .pipe(
            map(() => {
              return BuilderActions.updateStepConfigurationSuccess(form, valid);
            }),
            catchError((err: HttpErrorResponse) => {
              return of(BuilderActions.updateStepConfigurationError(err));
            })
          )
      )
    )
  );

  updateDynamicStepConfiguration$ = createEffect(() =>
    this.actions$.pipe(
      ofType(BuilderActions.updateDynamicStepConfiguration),
      withLatestFrom(this.store$.select(selectIds)),
      withLatestFrom(
        this.store$.select(selectModuleConfig),
        ([{ form, valid }, ids], moduleConf) => ({
          form,
          valid,
          ids,
          moduleConf,
        })
      ),
      switchMap(({ form, valid, ids, moduleConf }) =>
        this.builderService
          .updateActionDynamicOutputsConfiguration(form, ...ids, moduleConf)
          .pipe(
            switchMap(() => [
              BuilderActions.updateDynamicStepConfigurationSuccess(form, valid),
              BuilderActions.loadWorkflow(ids[0]),
            ]),
            catchError((err: HttpErrorResponse) => {
              return of(
                BuilderActions.updateDynamicStepConfigurationError(err)
              );
            })
          )
      )
    )
  );

  saveActionName$ = createEffect(() =>
    this.actions$.pipe(
      ofType(BuilderActions.saveActionName),
      withLatestFrom(this.store$.select(selectIds)),
      switchMap(([{ name }, ids]) =>
        this.builderService.updateModuleName(...ids, name).pipe(
          map(() => {
            return BuilderActions.saveActionNameSuccess(name);
          }),
          catchError((err: HttpErrorResponse) => {
            return of(BuilderActions.saveActionNameError(err));
          })
        )
      )
    )
  );

  removeModuleFromWorkflow$ = createEffect(() =>
    this.actions$.pipe(
      ofType(BuilderActions.removeCurrentStep),
      withLatestFrom(this.store$.select(selectIds)),
      switchMap(([, ids]) =>
        this.builderService.removeModuleFromWorkflow(...ids).pipe(
          map(() => {
            return BuilderActions.removeCurrentStepSuccess();
          }),
          catchError((err: HttpErrorResponse) => {
            return of(BuilderActions.removeCurrentStepError(err));
          })
        )
      )
    )
  );

  setModulesLinks$ = createEffect(() =>
    this.actions$.pipe(
      ofType(BuilderActions.reorganize),
      withLatestFrom(this.store$.select(selectWorkflowIdWithModuleIds)),
      switchMap(([indexes, ids]) =>
        this.builderService.setModulesLinks(...ids, indexes).pipe(
          switchMap(({ workflow, indexes }) => {
            return [
              BuilderActions.loadWorkflowSuccess(workflow),
              BuilderActions.sendLinksSuccess(
                indexes.previousIndex,
                indexes.currentIndex
              ),
            ];
          }),
          catchError((err: HttpErrorResponse) => {
            return of(BuilderActions.sendLinksError(err));
          })
        )
      )
    )
  );

  updateWorkflowName$ = createEffect(() =>
    this.actions$.pipe(
      ofType(BuilderActions.editName),
      withLatestFrom(this.store$.select(selectWorkflowId)),
      switchMap(([{ name }, id]) =>
        this.builderService.editName(name, id).pipe(
          map((name) => {
            return BuilderActions.editNameSuccess(name);
          }),
          catchError((err: HttpErrorResponse) => {
            return of(BuilderActions.editNameError(err));
          })
        )
      )
    )
  );

  addOutput$ = createEffect(() =>
    this.actions$.pipe(
      ofType(BuilderActions.addOutput),
      withLatestFrom(this.store$.select(selectIds)),
      switchMap(([, ids]) =>
        this.builderService.addOutput(ids).pipe(
          switchMap((id) => {
            return [
              BuilderActions.addOutputSuccess(),
              BuilderActions.loadWorkflow(id),
            ];
          }),
          catchError((err: HttpErrorResponse) => {
            return of(BuilderActions.addOutputError(err));
          })
        )
      )
    )
  );

  removeOutput$ = createEffect(() =>
    this.actions$.pipe(
      ofType(BuilderActions.removeOutput),
      withLatestFrom(this.store$.select(selectIds)),
      switchMap(([{ id }, ids]) =>
        this.builderService.removeOutput(id, ...ids).pipe(
          switchMap((id) => {
            return [
              BuilderActions.removeOutputSuccess(),
              BuilderActions.loadWorkflow(id),
            ];
          }),
          catchError((err: HttpErrorResponse) => {
            return of(BuilderActions.removeOutputError(err));
          })
        )
      )
    )
  );

  removeFile$ = createEffect(() =>
    this.actions$.pipe(
      ofType(BuilderActions.removeFile),
      withLatestFrom(this.store$.select(selectIds)),
      switchMap(([{ inputId }, ids]) =>
        this.builderService.removeFile(inputId, ...ids).pipe(
          map(({ inputId }) => {
            return BuilderActions.removeFileSuccess(inputId);
          }),
          catchError((err: HttpErrorResponse) => {
            return of(BuilderActions.removeFileError(err));
          })
        )
      )
    )
  );

  getResults$ = createEffect(() =>
    this.actions$.pipe(
      ofType(BuilderActions.getResults),
      withLatestFrom(this.store$.select(selectWorkflowId)),
      switchMap(([, id]) =>
        this.builderService.getResults(id).pipe(
          map((results) => {
            return BuilderActions.getResultsSuccess(results);
          }),
          catchError((err: HttpErrorResponse) => {
            return of(BuilderActions.getResultsError(err));
          })
        )
      )
    )
  );

  saveResults$ = createEffect(() =>
    this.actions$.pipe(
      ofType(BuilderActions.saveResults),
      withLatestFrom(this.store$.select(selectWorkflowId)),
      switchMap(([{ results }, id]) =>
        this.builderService.saveResults(id, results).pipe(
          map(() => {
            return BuilderActions.saveResultsSuccess();
          }),
          catchError((err: HttpErrorResponse) => {
            return of(BuilderActions.saveResultsError(err));
          })
        )
      )
    )
  );

  setConstraints$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(BuilderActions.setConstraints),
      withLatestFrom(this.store$.select(selectIds)),
      switchMap(([{ deployConstraints }, ids]) =>
        this.builderService
          .setActionDeployConstraints(...ids, deployConstraints)
          .pipe(
            map(() => {
              return BuilderActions.setConstraintsSuccess(deployConstraints);
            }),
            catchError((err: HttpErrorResponse) => {
              return of(BuilderActions.setConstraintsError(err));
            })
          )
      )
    );
  });

  setObjectives$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(BuilderActions.setObjectives),
      withLatestFrom(this.store$.select(selectIds)),
      switchMap(([{ deployObjectives }, ids]) =>
        this.builderService
          .setActionDeployObjectives(...ids, deployObjectives)
          .pipe(
            map(() => {
              return BuilderActions.setObjectivesSuccess(deployObjectives);
            }),
            catchError((err: HttpErrorResponse) => {
              return of(BuilderActions.setObjectivesError(err));
            })
          )
      )
    );
  });

  getStudioErrors$ = createEffect(() =>
    this.actions$.pipe(
      ofType(
        BuilderActions.loadWorkflowSuccess,
        BuilderActions.choseStepModuleSuccess,
        BuilderActions.updateStepSelectionSuccess,
        BuilderActions.saveFileSuccess,
        BuilderActions.removeFileSuccess,
        BuilderActions.removeCurrentStepSuccess,
        BuilderActions.updateStepConfigurationSuccess
      ),
      withLatestFrom(this.store$.select(selectWorkflowId)),
      switchMap(([, id]) => {
        if (id !== null) {
          return this.builderService.getStudioErrors(id).pipe(
            map((errorList) => {
              return BuilderActions.getStudioErrorsSuccess(errorList);
            }),
            catchError((err: HttpErrorResponse) => {
              return of(BuilderActions.getStudioErrorsError(err));
            })
          );
        } else {
          return of(BuilderActions.getStudioErrorsSuccess([] as StudioError[]));
        }
      })
    )
  );

  changeVersion$ = createEffect(() =>
    this.actions$.pipe(
      ofType(BuilderActions.changeVersion),
      withLatestFrom(this.store$.select(selectIds)),
      switchMap(([{ id }, ids]) =>
        this.builderService.changeVersion(...ids, id).pipe(
          switchMap(({ removedResults }) => {
            return [
              BuilderActions.changeVersionSuccess(removedResults),
              BuilderActions.loadWorkflow(ids[0]),
            ];
          }),
          catchError((err: HttpErrorResponse) => {
            return of(BuilderActions.changeVersionError(err));
          })
        )
      )
    )
  );

  displayResultRemovedWarning$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(BuilderActions.changeVersionSuccess),
        tap(({ removedResults }) => {
          if (removedResults.length > 0) {
            this.notification.create(
              'warning',
              'Workflow Results Removed',
              'The following results were removed: ' + removedResults.join(', ')
            );
          }
        })
      ),
    { dispatch: false }
  );

  displayErrors$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(
          BuilderActions.getResultsError,
          BuilderActions.saveResultsError,
          BuilderActions.getStudioErrorsError,
          BuilderActions.createError,
          BuilderActions.deployError,
          BuilderActions.moduleError,
          BuilderActions.addOutputError,
          BuilderActions.changeVersionError,
          BuilderActions.saveActionNameError,
          BuilderActions.updateDynamicStepConfigurationError,
          BuilderActions.updateStepConfigurationError,
          BuilderActions.choseStepModuleError,
          BuilderActions.removeCurrentStepError,
          BuilderActions.updateStepSelectionError,
          BuilderActions.removeOutputError,
          BuilderActions.removeFileError,
          BuilderActions.editNameError,
          BuilderActions.loadWorkflowError,
          BuilderActions.sendLinksError,
          BuilderActions.loadSitesError,
          BuilderActions.setConstraintsError,
          BuilderActions.setObjectivesError
        ),
        tap((errAction) =>
          this.notification.create(
            'error',
            'Something went wrong ! http error : ' + errAction.error.status,
            errAction.error.message + ': ' + errAction.error.error.error
          )
        )
      ),
    { dispatch: false }
  );

  constructor(
    private actions$: Actions,
    private builderService: BuilderApiService,
    private router: Router,
    private notification: NzNotificationService,
    private store$: Store<BuilderState>
  ) {}
}

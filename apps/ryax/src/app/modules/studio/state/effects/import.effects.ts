// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, map, switchMap, withLatestFrom } from 'rxjs/operators';
import { GitScan } from '../../entities/git-scan';
import { ImportApiService } from '../../services/import/import-api.service';
import { ImportActions } from '../actions';
import { selectBuildMetadata } from '../selectors/import.selectors';

@Injectable()
export class ImportEffects {
  loadExistingRepos$ = createEffect(() =>
    this.actions$.pipe(
      ofType(ImportActions.loadExistingRepos),
      switchMap(() =>
        this.importApiService.loadExistingRepos().pipe(
          map((gits: any[]) => ImportActions.loadExistingReposSuccess(gits)),
          catchError((err: HttpErrorResponse) => {
            return of(ImportActions.loadExistingReposError(err));
          })
        )
      )
    )
  );

  loadNewRepo$ = createEffect(() =>
    this.actions$.pipe(
      ofType(ImportActions.loadNewRepo),
      switchMap(({ gitUrl }) =>
        this.importApiService.loadNewRepo(gitUrl).pipe(
          map((git: GitScan) => ImportActions.loadNewRepoSuccess(git)),
          catchError((err: HttpErrorResponse) => {
            return of(ImportActions.loadNewRepoError(err));
          })
        )
      )
    )
  );

  buildModule$ = createEffect(() =>
    this.actions$.pipe(
      ofType(ImportActions.build),
      withLatestFrom(selectBuildMetadata),
      switchMap((metadata) =>
        this.importApiService.buildModule(metadata).pipe(
          map(() => ImportActions.buildSuccess()),
          catchError((err: HttpErrorResponse) => {
            return of(ImportActions.buildError(err));
          })
        )
      )
    )
  );

  constructor(
    private actions$: Actions,
    private importApiService: ImportApiService
  ) {}
}

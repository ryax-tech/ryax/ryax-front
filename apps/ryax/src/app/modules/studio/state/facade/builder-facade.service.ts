// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { FormValues, OutputFormValues } from '../../entities/workflow-step';
import { StudioState } from '../reducers';
import { BuilderActions } from '../actions';
import {
  selectConfigLoading,
  selectCurrentIndex,
  selectCurrentPanelIndex,
  selectCurrentStep,
  selectDeployable,
  selectEveryOutput,
  selectFileOrDirOutput,
  selectIds,
  selectIsAllSet,
  selectIsCurrentStepSource,
  selectLastSave,
  selectModuleDynamicOutput,
  selectModulesDescriptions,
  selectPreviousOutputs,
  selectProcessList,
  selectResults,
  selectResultsType,
  selectSourceList,
  selectStatusList,
  selectStudioErrors,
  selectWorkflow,
  selectWorkflowName,
  selectWorkflowNameLoading,
  selectSites,
} from '../selectors/builder.selectors';
import {
  ActionDeployConstraints,
  ActionDeployObjectives,
  InputSection,
} from '../../entities/workflow-action';
import { firstValueFrom } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class BuilderFacade {
  public workflowName$ = this.store.select(selectWorkflowName);
  public workflowNameLoading$ = this.store.select(selectWorkflowNameLoading);
  public workflow$ = this.store.select(selectWorkflow);
  public currentIndex$ = this.store.select(selectCurrentIndex);
  public currentStep$ = this.store.select(selectCurrentStep);
  public currentPanelIndex$ = this.store.select(selectCurrentPanelIndex);
  public statusList$ = this.store.select(selectStatusList);
  public deployable$ = this.store.select(selectDeployable);
  public lastSave$ = this.store.select(selectLastSave);
  public previousOutputs$ = this.store.select(selectPreviousOutputs);
  public isCurrentStepSource$ = this.store.select(selectIsCurrentStepSource);
  public sourceList$ = this.store.select(selectSourceList);
  public processList$ = this.store.select(selectProcessList);
  public dynamicOutputs$ = this.store.select(selectModuleDynamicOutput);
  public modulesDescriptions$ = this.store.select(selectModulesDescriptions);
  public isAllSet$ = this.store.select(selectIsAllSet);
  public ids$ = this.store.select(selectIds);
  public results$ = this.store.select(selectResults);
  public resultsType$ = this.store.select(selectResultsType);
  public everyOutput$ = this.store.select(selectEveryOutput);
  public everyFileOrDirOutput$ = this.store.select(selectFileOrDirOutput);
  public configLoading$ = this.store.select(selectConfigLoading);
  public studioErrors$ = this.store.select(selectStudioErrors);
  public sites$ = this.store.select(selectSites);

  constructor(private readonly store: Store<StudioState>) {}

  public create() {
    this.store.dispatch(BuilderActions.create());
  }

  public loadWorkflow(workflowId: string) {
    this.store.dispatch(BuilderActions.loadWorkflow(workflowId));
  }

  public loadSites() {
    this.store.dispatch(BuilderActions.loadSites());
  }

  public getModules() {
    this.store.dispatch(BuilderActions.getModules());
  }

  public toStep(stepIndex: number) {
    this.store.dispatch(BuilderActions.toStep(stepIndex));
  }

  public updatePanelIndex(index: number) {
    this.store.dispatch(BuilderActions.toPanel(index));
  }

  public async addStep() {
    if (await firstValueFrom(this.isAllSet$)) {
      this.store.dispatch(BuilderActions.addStep());
    }
  }

  public removeCurrentStep() {
    this.store.dispatch(BuilderActions.removeCurrentStep());
  }

  public editName(name: string) {
    this.store.dispatch(BuilderActions.editName(name));
  }

  public reorganize(previousIndex: number, currentIndex: number) {
    this.store.dispatch(BuilderActions.reorganize(previousIndex, currentIndex));
  }

  public addModule(selectedModuleId: string) {
    this.store.dispatch(BuilderActions.choseStepModule(selectedModuleId));
  }

  public updateModule(selectedModuleId: string) {
    this.store.dispatch(BuilderActions.updateStepSelection(selectedModuleId));
  }

  public saveForm(
    form: FormValues,
    inputSection: InputSection,
    valid: boolean
  ) {
    this.store.dispatch(
      BuilderActions.updateStepConfiguration(form, inputSection, valid)
    );
  }

  public saveDynamicForm(form: OutputFormValues, valid: boolean) {
    this.store.dispatch(
      BuilderActions.updateDynamicStepConfiguration(form, valid)
    );
  }

  public deploy() {
    this.store.dispatch(BuilderActions.deploy());
  }

  public done() {
    this.store.dispatch(BuilderActions.done());
  }

  public addOutput() {
    this.store.dispatch(BuilderActions.addOutput());
  }

  public removeOutput(id: string) {
    this.store.dispatch(BuilderActions.removeOutput(id));
  }

  public saveFileSuccess(inputId: string, fileName: string) {
    this.store.dispatch(BuilderActions.saveFileSuccess(inputId, fileName));
  }

  public saveFileError(err: HttpErrorResponse) {
    this.store.dispatch(BuilderActions.saveFileError(err));
  }

  public removeFile(inputId: string) {
    this.store.dispatch(BuilderActions.removeFile(inputId));
  }

  public getResults() {
    this.store.dispatch(BuilderActions.getResults());
  }

  public setResult(index: number, result: { key: string; value: string }) {
    this.store.dispatch(BuilderActions.setResult(index, result));
  }

  public clearResults() {
    this.store.dispatch(BuilderActions.clearResults());
  }

  public addResult() {
    this.store.dispatch(BuilderActions.addResults());
  }

  public removeResult(index: number) {
    this.store.dispatch(BuilderActions.removeResult(index));
  }

  public saveResults(results: { key: string; value: string }[]) {
    this.store.dispatch(BuilderActions.saveResults(results));
  }

  public saveActionName(name: string) {
    this.store.dispatch(BuilderActions.saveActionName(name));
  }

  public changeVersion(id: string) {
    this.store.dispatch(BuilderActions.changeVersion(id));
  }

  public setConstraints(deployConstraints: ActionDeployConstraints) {
    this.store.dispatch(BuilderActions.setConstraints(deployConstraints));
  }

  public setObjectives(deployObjectives: ActionDeployObjectives) {
    this.store.dispatch(BuilderActions.setObjectives(deployObjectives));
  }
}

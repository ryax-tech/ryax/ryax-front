// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { HttpErrorResponse } from '@angular/common/http';
import { createAction } from '@ngrx/store';
import { BuilderStateFiller } from '../../../workflow/entities';
import { StudioError } from '../../entities/studio-error';
import {
  ActionDeployConstraints,
  ActionDeployObjectives,
  InputSection,
  WorkflowActionDetailed,
  WorkflowActionLight,
} from '../../entities/workflow-action';
import { FormValues, OutputFormValues } from '../../entities/workflow-step';
import { WorkflowResultConfig } from '../../entities/workflow-results';
import { Site } from '../../entities/sites';

export const create = createAction('[Builder] Create new workflow');

export const createSuccess = createAction(
  '[Builder] Create new workflow success',
  (workflow: BuilderStateFiller) => ({ workflow })
);

export const createError = createAction(
  '[Builder] Create new workflow error',
  (error: HttpErrorResponse) => ({ error })
);

export const loadWorkflow = createAction(
  '[Builder] Load existing workflow',
  (workflowId: string) => ({ workflowId })
);

export const loadWorkflowSuccess = createAction(
  '[Builder] Load existing workflow success',
  (workflow: BuilderStateFiller) => ({ workflow })
);

export const loadWorkflowError = createAction(
  '[Builder] Load existing workflow error',
  (error: HttpErrorResponse) => ({ error })
);

export const loadSites = createAction('[Builder] Load sites');
export const loadSitesSuccess = createAction(
  '[Builder] Load sites success',
  (sites: Site[]) => ({ sites })
);

export const loadSitesError = createAction(
  '[Builder] Load sites error',
  (error: HttpErrorResponse) => ({ error })
);

export const editName = createAction('[Builder] Edit name', (name: string) => ({
  name,
}));

export const editNameSuccess = createAction(
  '[Builder] Edit name success',
  (name: string) => ({ name })
);

export const editNameError = createAction(
  '[Builder] Edit name error',
  (error: HttpErrorResponse) => ({ error })
);

export const addStep = createAction('[Builder] Add step to current workflow');

export const removeCurrentStep = createAction(
  '[Builder] Remove current step from workflow'
);

export const removeCurrentStepSuccess = createAction(
  '[Builder] Remove current step from workflow success'
);

export const removeCurrentStepError = createAction(
  '[Builder] Remove current step from workflow error',
  (error: HttpErrorResponse) => ({ error })
);

export const choseStepModule = createAction(
  '[Builder] Select a module for current step for the first time',
  (selectedModuleId: string) => ({ selectedModuleId })
);

export const choseStepModuleSuccess = createAction(
  '[Builder] Select a module for current step for the first time success',
  (selectedModule: WorkflowActionDetailed, defaultValues: FormValues) => ({
    selectedModule,
    defaultValues,
  })
);

export const choseStepModuleError = createAction(
  '[Builder] Select a module for current step for the first time error',
  (error: HttpErrorResponse) => ({ error })
);

export const updateStepSelection = createAction(
  '[Builder] Update current step selected module',
  (selectedModuleId: string) => ({ selectedModuleId })
);

export const updateStepSelectionSuccess = createAction(
  '[Builder] Update current step selected module success',
  (selectedModule: WorkflowActionDetailed, defaultValues: FormValues) => ({
    selectedModule,
    defaultValues,
  })
);

export const updateStepSelectionError = createAction(
  '[Builder] Update current step selected module error',
  (error: HttpErrorResponse) => ({ error })
);

export const updateStepConfiguration = createAction(
  '[Builder] Update current step module configuration',
  (form: FormValues, inputSection: InputSection, valid: boolean) => ({
    form,
    inputSection,
    valid,
  })
);

export const updateStepConfigurationSuccess = createAction(
  '[Builder] Update current step module configuration success',
  (form: FormValues, valid: boolean) => ({ form, valid })
);

export const updateStepConfigurationError = createAction(
  '[Builder] Update current step module configuration error',
  (error: HttpErrorResponse) => ({ error })
);

export const updateDynamicStepConfiguration = createAction(
  '[Builder] Update current step module configuration with dynamic outputs',
  (form: OutputFormValues, valid: boolean) => ({ form, valid })
);

export const updateDynamicStepConfigurationSuccess = createAction(
  '[Builder] Update current step module configuration with dynamic outputs success',
  (form: OutputFormValues, valid: boolean) => ({ form, valid })
);

export const updateDynamicStepConfigurationError = createAction(
  '[Builder] Update current step module configuration with dynamic outputs error',
  (error: HttpErrorResponse) => ({ error })
);

export const reorganize = createAction(
  '[Builder] Reorganize steps',
  (previousIndex: number, currentIndex: number) => ({
    previousIndex,
    currentIndex,
  })
);

export const sendLinksSuccess = createAction(
  '[Builder] Save module links success',
  (previousIndex: number, currentIndex: number) => ({
    previousIndex,
    currentIndex,
  })
);

export const sendLinksError = createAction(
  '[Builder] Save module links error',
  (error: HttpErrorResponse) => ({ error })
);

export const toStep = createAction(
  '[Builder] Go to step number ...',
  (stepNumber: number) => ({ stepNumber })
);

export const toPanel = createAction(
  '[Builder] Go to panel number ...',
  (panelIndex: number) => ({ panelIndex })
);

export const deploy = createAction('[Builder] Deploy Workflow');

export const deploySuccess = createAction('[Builder] Workflow deploy success');

export const deployError = createAction(
  '[Builder] Deploy error',
  (error: HttpErrorResponse) => ({ error })
);

export const done = createAction('[Builder] Workflow done');

export const getModules = createAction('[Builder] Get modules lists');

export const moduleSuccess = createAction(
  '[Builder] Module load success',
  (moduleLists: [WorkflowActionLight[], WorkflowActionLight[]]) => ({
    triggerList: moduleLists[0],
    actionList: moduleLists[1],
  })
);

export const moduleError = createAction(
  '[Builder] Module load error',
  (error: HttpErrorResponse) => ({ error })
);

export const addOutput = createAction('[Builder] Add output');

export const addOutputSuccess = createAction('[Builder] Add output success');

export const addOutputError = createAction(
  '[Builder] Add output error',
  (error: HttpErrorResponse) => ({ error })
);

export const removeOutput = createAction(
  '[Builder] Remove output',
  (id: string) => ({ id })
);

export const removeOutputSuccess = createAction(
  '[Builder] Remove output success'
);

export const removeOutputError = createAction(
  '[Builder] Remove output error',
  (error: HttpErrorResponse) => ({ error })
);

export const saveFileSuccess = createAction(
  '[Builder] Save file success',
  (inputId: string, fileName: string) => ({ inputId, fileName })
);

export const saveFileError = createAction(
  '[Builder] Save file error',
  (error: HttpErrorResponse) => ({ error })
);

export const removeFile = createAction(
  '[Builder] Remove file',
  (inputId: string) => ({ inputId })
);

export const removeFileSuccess = createAction(
  '[Builder] Remove file success',
  (inputId: string) => ({ inputId })
);

export const removeFileError = createAction(
  '[Builder] Remove file error',
  (error: HttpErrorResponse) => ({ error })
);

export const getResults = createAction('[Builder] Get results');

export const getResultsSuccess = createAction(
  '[Builder] Get results success',
  (results: WorkflowResultConfig) => ({ results })
);

export const getResultsError = createAction(
  '[Builder] Get results error',
  (error: HttpErrorResponse) => ({ error })
);

export const addResults = createAction('[Builder] Add result to result list');

export const removeResult = createAction(
  '[Builder] Remove result at index',
  (index: number) => ({ index })
);

export const clearResults = createAction('[Builder] Clear results');

export const setResult = createAction(
  '[Builder] Set result',
  (index: number, result: { key: string; value: string }) => ({ index, result })
);

export const saveResults = createAction(
  '[Builder] Save results',
  (results: { key: string; value: string }[]) => ({ results })
);

export const saveResultsSuccess = createAction(
  '[Builder] Save results success'
);

export const saveResultsError = createAction(
  '[Builder] Save results error',
  (error: HttpErrorResponse) => ({ error })
);

export const getStudioErrorsSuccess = createAction(
  '[Builder] Get studio errors success',
  (errorList: StudioError[]) => ({ errorList })
);

export const getStudioErrorsError = createAction(
  '[Builder] Get studio errors error',
  (error: HttpErrorResponse) => ({ error })
);

export const saveActionName = createAction(
  '[Builder] Save action name',
  (name: string) => ({ name })
);

export const saveActionNameSuccess = createAction(
  '[Builder] Save action name success',
  (name: string) => ({ name })
);

export const saveActionNameError = createAction(
  '[Builder] Save action name error',
  (error: HttpErrorResponse) => ({ error })
);

export const changeVersion = createAction(
  '[Builder] Change version',
  (id: string) => ({ id })
);

export const changeVersionSuccess = createAction(
  '[Builder] Change version success',
  (removedResults: string[]) => ({ removedResults })
);

export const changeVersionError = createAction(
  '[Builder] Change version error',
  (error: HttpErrorResponse) => ({ error })
);

export const setConstraints = createAction(
  '[Builder] Set Action Deployment Constraints',
  (deployConstraints: ActionDeployConstraints) => ({ deployConstraints })
);

export const setConstraintsSuccess = createAction(
  '[Builder] Set constraints success',
  (deployConstraints: ActionDeployConstraints) => ({ deployConstraints })
);

export const setConstraintsError = createAction(
  '[Builder] Set constraints error',
  (error: HttpErrorResponse) => ({ error })
);

export const setObjectives = createAction(
  '[Builder] Set Action Deployment Objectives',
  (deployObjectives: ActionDeployObjectives) => ({ deployObjectives })
);

export const setObjectivesSuccess = createAction(
  '[Builder] Set Objectives success',
  (deployObjectives: ActionDeployObjectives) => ({ deployObjectives })
);

export const setObjectivesError = createAction(
  '[Builder] Set Objectives error',
  (error: HttpErrorResponse) => ({ error })
);

// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { HttpErrorResponse } from '@angular/common/http';
import { createAction } from '@ngrx/store';
import { GitScan, ScannedModule } from '../../entities/git-scan';
import { Metadata } from '../../entities/metadata';
import { NzUploadChangeParam } from 'ng-zorro-antd/upload';

export const start = createAction('[Import] Import new code');

export const done = createAction('[Import] Import done');

export const loadExistingRepos = createAction(
  '[Import] Load existing git repos'
);

export const loadExistingReposSuccess = createAction(
  '[Import] Load existing git repos successfully',
  (gitContents: any[]) => ({ gitContents })
);

export const loadExistingReposError = createAction(
  '[Import] Load existing git repos error',
  (error: HttpErrorResponse) => ({ error })
);

export const selectRepo = createAction(
  '[Import] Select an existing repo',
  (index: number) => ({ index })
);

export const loadNewRepo = createAction(
  '[Import] Load a new git repo',
  (gitUrl: string) => ({ gitUrl })
);

export const loadNewRepoSuccess = createAction(
  '[Import] Load a new git repo successfully',
  (gitScan: GitScan) => ({ gitScan })
);

export const loadNewRepoError = createAction(
  '[Import] Load a new git repo error',
  (error: HttpErrorResponse) => ({ error })
);

export const selectContent = createAction(
  '[Import] Select a file or dir from git repo',
  (scannedModule: ScannedModule) => ({ scannedModule })
);

export const takeAllContent = createAction(
  '[Import] Take all content from git repo'
);

export const codeDropped = createAction(
  '[Import] Code dropped directly',
  (code: NzUploadChangeParam) => ({ code })
);

export const toPanel = createAction(
  '[Import] Go to panel',
  (index: number) => ({ index })
);

export const build = createAction('[Import] Build module');

export const buildSuccess = createAction('[Import] Built module successfully');

export const buildError = createAction(
  '[Import] Build module error',
  (error: HttpErrorResponse) => ({ error })
);

export const saveMetadata = createAction(
  '[Import] Save code metadata',
  (metadata: Metadata) => ({ metadata })
);

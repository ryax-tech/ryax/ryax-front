// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { moveItemInArray } from '@angular/cdk/drag-drop';
import { HttpErrorResponse } from '@angular/common/http';
import { createReducer, on } from '@ngrx/store';
import { DateTime } from 'luxon';
import { mutableOn } from 'ngrx-etc';
import { DefaultValueState } from '../../../shared/entity/default-states';
import { StudioError } from '../../entities/studio-error';
import { WorkflowActionLight } from '../../entities/workflow-action';
import {
  ModulePanels,
  StepStatus,
  WorkflowStep,
} from '../../entities/workflow-step';
import { BuilderActions } from '../actions';
import { WorkflowResultConfig } from '../../entities/workflow-results';
import { Site } from '../../entities/sites';

export interface BuilderState {
  triggerList: WorkflowActionLight[];
  actionList: WorkflowActionLight[];
  workflow: WorkflowStep[];
  workflowId: string;
  workflowName: DefaultValueState<string>;
  currentStepIndex: number;
  results: WorkflowResultConfig;
  lastSave: DateTime;
  loading: boolean;
  configLoading: boolean;
  error: HttpErrorResponse | null;
  studioError: StudioError[];
  sites: Site[];
}

const initialPanelsState: ModulePanels = [
  {
    name: 'Select',
    status: 'process',
  },
  {
    name: 'Configure',
    status: 'wait',
    inputFormValues: {},
    outputFormValues: {},
    addonInputFormValues: {},
  },
];

const initialWorkflowState: WorkflowStep[] = [
  {
    name: 'Source',
    status: 'UNDEFINED',
    type: 'SOURCE',
    panels: initialPanelsState,
    currentPanelIndex: 0,
  },
];

export const initialBuilderState: BuilderState = {
  triggerList: [],
  actionList: [],
  workflowName: {
    value: '',
    loading: false,
    error: null,
  },
  workflowId: '',
  workflow: initialWorkflowState,
  currentStepIndex: 0,
  results: { type: 'JSON', workflow_results: [] },
  lastSave: DateTime.now(),
  loading: false,
  configLoading: false,
  error: null,
  studioError: [],
  sites: [],
};

const newStepState: WorkflowStep = {
  name: 'New Process',
  status: 'UNDEFINED',
  type: 'PROCESS',
  panels: initialPanelsState,
  currentPanelIndex: 0,
};

export const builderReducer = createReducer<BuilderState>(
  initialBuilderState,
  on(BuilderActions.create, (state) => ({
    ...state,
    loading: true,
    error: null,
  })),
  on(BuilderActions.createSuccess, (state) => ({
    ...state,
    workflow: initialWorkflowState,
    lastSave: DateTime.now(),
    loading: false,
  })),
  on(BuilderActions.createError, (state, { error }) => ({
    ...state,
    loading: false,
    error,
  })),
  on(BuilderActions.loadWorkflow, (state) => ({
    ...state,
    loading: true,
    error: null,
  })),
  on(BuilderActions.loadWorkflowSuccess, (state, { workflow }) => ({
    ...state,
    workflowName: { value: workflow.workflowName, loading: false, error: null },
    workflowId: workflow.workflowId,
    workflow: workflow.workflow,
    lastSave: workflow.lastSave,
    loading: false,
    configLoading: false,
  })),
  on(BuilderActions.loadWorkflowError, (state, { error }) => ({
    ...state,
    loading: false,
    error,
  })),
  on(BuilderActions.loadSitesSuccess, (state, { sites }) => ({
    ...state,
    sites: sites,
  })),
  on(BuilderActions.editName, (state) => ({
    ...state,
    workflowName: { ...state.workflowName, loading: true, error: null },
  })),
  on(BuilderActions.editNameSuccess, (state, { name }) => ({
    ...state,
    workflowName: { value: name, loading: false, error: null },
  })),
  on(BuilderActions.editNameError, (state, { error }) => ({
    ...state,
    workflowName: { ...state.workflowName, loading: false, error },
  })),
  mutableOn(
    BuilderActions.setConstraintsSuccess,
    (state, { deployConstraints }) => {
      if (state.workflow[state.currentStepIndex].panels[0].originalValue) {
        state.workflow[
          state.currentStepIndex
        ].panels[0].originalValue!.constraints = deployConstraints;
      }
      return state;
    }
  ),
  mutableOn(
    BuilderActions.setObjectivesSuccess,
    (state, { deployObjectives }) => {
      if (state.workflow[state.currentStepIndex].panels[0].originalValue) {
        state.workflow[
          state.currentStepIndex
        ].panels[0].originalValue!.objectives = deployObjectives;
      }
      return state;
    }
  ),
  on(BuilderActions.addStep, (state) => ({
    ...state,
    workflow: [...state.workflow, newStepState],
    currentStepIndex: state.workflow.length,
  })),
  on(BuilderActions.removeCurrentStep, (state) => ({
    ...state,
    loading: true,
    error: null,
  })),
  mutableOn(BuilderActions.removeCurrentStepSuccess, (state) => {
    state.loading = false;
    state.workflow = [
      ...state.workflow.filter(
        (value, index) => index !== state.currentStepIndex
      ),
    ];
    state.currentStepIndex = state.currentStepIndex - 1;
    state.lastSave = DateTime.now();
    return state;
  }),
  on(BuilderActions.removeCurrentStepError, (state, { error }) => ({
    ...state,
    loading: false,
    error,
  })),
  on(
    BuilderActions.updateStepSelection,
    BuilderActions.choseStepModule,
    (state) => ({
      ...state,
      configLoading: true,
      error: null,
    })
  ),
  mutableOn(
    BuilderActions.updateStepSelectionSuccess,
    BuilderActions.choseStepModuleSuccess,
    (state, { selectedModule, defaultValues }) => {
      state.configLoading = false;
      state.workflow[state.currentStepIndex].name = selectedModule.name;
      state.workflow[state.currentStepIndex].panels[0].originalValue =
        selectedModule;
      state.workflow[state.currentStepIndex].panels[0].status = 'finish';
      state.workflow[state.currentStepIndex].panels[1].inputFormValues =
        defaultValues;
      state.workflow[state.currentStepIndex].panels[1].addonInputFormValues =
        defaultValues;
      state.workflow[state.currentStepIndex].panels[1].status = 'process';
      state.workflow[state.currentStepIndex].currentPanelIndex = 2;
      state.lastSave = DateTime.now();
      return state;
    }
  ),
  on(
    BuilderActions.updateStepSelectionError,
    BuilderActions.choseStepModuleError,
    (state, { error }) => ({
      ...state,
      configLoading: false,
      error,
    })
  ),
  on(
    BuilderActions.updateStepConfiguration,
    BuilderActions.updateDynamicStepConfiguration,
    (state) => ({
      ...state,
      configLoading: true,
      error: null,
    })
  ),
  mutableOn(
    BuilderActions.updateStepConfigurationSuccess,
    (state, { form, valid }) => {
      state.workflow[state.currentStepIndex].panels[1].inputFormValues = form;
      state.workflow[state.currentStepIndex].panels[1].status = valid
        ? 'finish'
        : 'process';
      state.workflow[state.currentStepIndex].status = valid
        ? 'READY'
        : 'UNCONFIGURED';
      state.configLoading = false;
      return state;
    }
  ),
  mutableOn(
    BuilderActions.updateDynamicStepConfigurationSuccess,
    (state, { form, valid }) => {
      state.workflow[state.currentStepIndex].panels[1].outputFormValues = form;
      state.workflow[state.currentStepIndex].panels[1].status = valid
        ? 'finish'
        : 'process';
      state.workflow[state.currentStepIndex].status = valid
        ? 'READY'
        : 'UNCONFIGURED';
      return state;
    }
  ),
  on(
    BuilderActions.updateStepConfigurationError,
    BuilderActions.updateDynamicStepConfigurationError,
    (state, { error }) => ({
      ...state,
      configLoading: false,
      error,
    })
  ),
  mutableOn(BuilderActions.saveFileSuccess, (state, { inputId, fileName }) => {
    state.workflow[state.currentStepIndex].panels[1].inputFormValues[
      inputId
    ].value = fileName;
    state.configLoading = false;
    return state;
  }),
  on(BuilderActions.saveFileError, (state, { error }) => ({
    ...state,
    configLoading: false,
    error,
  })),
  on(BuilderActions.removeFile, (state) => ({
    ...state,
    configLoading: true,
    error: null,
  })),
  mutableOn(BuilderActions.removeFileSuccess, (state, { inputId }) => {
    state.workflow[state.currentStepIndex].panels[1].inputFormValues[
      inputId
    ].value = '';
    state.configLoading = false;
    return state;
  }),
  on(BuilderActions.removeFileError, (state, { error }) => ({
    ...state,
    configLoading: false,
    error,
  })),
  on(BuilderActions.toStep, (state, { stepNumber }) => ({
    ...state,
    currentStepIndex: stepNumber,
  })),
  mutableOn(BuilderActions.toPanel, (state, { panelIndex }) => {
    state.workflow[state.currentStepIndex].currentPanelIndex = panelIndex;
    return state;
  }),
  on(BuilderActions.reorganize, (state) => ({
    ...state,
    loading: true,
    error: null,
  })),
  // on(BuilderActions.sendLinks, (state) => ({
  //   ...state,
  //   loading: true,
  //   error: null
  // })),
  on(
    BuilderActions.sendLinksSuccess,
    (state, { previousIndex, currentIndex }) => {
      const workflow = [...state.workflow];
      moveItemInArray(workflow, previousIndex, currentIndex);
      let newIndex = state.currentStepIndex;
      if (state.currentStepIndex === previousIndex) {
        newIndex = currentIndex;
      } else if (state.currentStepIndex === currentIndex) {
        newIndex = previousIndex;
      }

      return {
        ...state,
        currentStepIndex: newIndex,
        loading: false,
      };
    }
  ),
  on(BuilderActions.sendLinksError, (state, { error }) => ({
    ...state,
    loading: false,
    error,
  })),
  on(BuilderActions.getModules, (state) => ({
    ...state,
    loading: true,
    error: null,
  })),
  on(BuilderActions.moduleSuccess, (state, { triggerList, actionList }) => ({
    ...state,
    triggerList,
    actionList,
    loading: false,
    error: null,
  })),
  on(BuilderActions.moduleError, (state, { error }) => ({
    ...state,
    loading: false,
    error,
  })),
  on(BuilderActions.getResults, (state) => ({
    ...state,
    loading: true,
    error: null,
  })),
  on(BuilderActions.getResultsSuccess, (state, { results }) => ({
    ...state,
    loading: false,
    results,
  })),
  on(BuilderActions.getResultsError, (state, { error }) => ({
    ...state,
    loading: false,
    error,
  })),
  on(BuilderActions.addResults, (state) => ({
    ...state,
    results: {
      type: state.results.type,
      workflow_results: [
        ...state.results.workflow_results,
        { key: '', value: '', type: '', description: '' },
      ],
    },
  })),
  on(BuilderActions.setResult, (state, { index, result }) => ({
    ...state,
    results: {
      type: state.results.type,
      workflow_results: [
        ...state.results.workflow_results.map((stateResult, stateIndex) => {
          if (index === stateIndex) {
            return {
              key: result.key,
              value: result.value,
              type: '',
              description: '',
            };
          } else {
            return stateResult;
          }
        }),
      ],
    },
  })),
  on(BuilderActions.removeResult, (state, { index }) => ({
    ...state,
    results: {
      type: state.results.type,
      workflow_results: [
        ...state.results.workflow_results.filter((result, i) => index !== i),
      ],
    },
  })),
  on(BuilderActions.clearResults, (state) => ({
    ...state,
    results: { type: state.results.type, workflow_results: [] },
  })),
  on(BuilderActions.done, () => ({
    ...initialBuilderState,
  })),
  on(BuilderActions.deploy, (state) => ({
    ...state,
    loading: true,
    error: null,
  })),
  on(BuilderActions.deploySuccess, (state) => ({
    ...state,
    loading: false,
    error: null,
  })),
  on(BuilderActions.deployError, (state, { error }) => ({
    ...state,
    loading: false,
    error,
  })),
  on(BuilderActions.loadWorkflow, (state) => ({
    ...state,
  })),
  on(BuilderActions.getStudioErrorsSuccess, (state, { errorList }) => ({
    ...state,
    studioError: errorList,
    workflow: state.workflow.map((step) => {
      let status: StepStatus;
      if (!step.panels[0].originalValue) {
        status = 'UNDEFINED';
      } else {
        status =
          errorList.findIndex(
            (error) =>
              error.workflow_module_id === step.panels[0].originalValue?.id
          ) >= 0
            ? 'UNCONFIGURED'
            : 'READY';
      }
      return {
        ...step,
        status,
      };
    }),
  })),
  on(BuilderActions.getStudioErrorsError, (state, { error }) => ({
    ...state,
    error,
  })),
  on(BuilderActions.saveActionName, (state) => ({
    ...state,
  })),
  mutableOn(BuilderActions.saveActionNameSuccess, (state, { name }) => {
    state.workflow[state.currentStepIndex].name = name;
    return state;
  }),
  on(BuilderActions.saveActionNameError, (state, { error }) => ({
    ...state,
    error,
  })),
  on(BuilderActions.changeVersion, (state) => ({
    ...state,
    loading: true,
    error: null,
  })),
  on(BuilderActions.changeVersionSuccess, (state) => ({
    ...state,
    loading: false,
  })),
  on(BuilderActions.changeVersionError, (state, { error }) => ({
    ...state,
    loading: false,
    error,
  }))
);

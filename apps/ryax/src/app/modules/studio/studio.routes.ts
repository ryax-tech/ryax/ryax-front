// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Route } from '@angular/router';
import { BuilderComponent } from './components/builder/builder.component';
import { BuildingComponent } from './components/building/building.component';

export const studioRoutes: Route[] = [
  {
    path: 'new',
    component: BuilderComponent,
  },
  {
    path: ':id',
    component: BuilderComponent,
  },
  {
    path: 'build',
    component: BuildingComponent,
  },
];

export interface NodePool {
  id: string;
  name: string;
  arch: string;
  cpu: number;
  gpu: number;
  memoryInMB: string;
  timeInSec: number;
}
export interface Site {
  id: string;
  name: string;
  type: string;
  nodePools: NodePool[];
}

// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
export interface GitScan {
  name: string;
  id: string;
  url: string;
  lastScan: LastScanContent;
}

export interface LastScanContent {
  sha: string;
  notBuiltModules: ScannedModule[];
  builtModules: ScannedModule[];
}

export interface ScannedModule {
  sha: string;
  dynamicOutputs: boolean;
  description: string | null;
  buildError: any;
  creationDate: string;
  type: 'python3' | null;
  name: string | null;
  id: string;
  kind: 'Source' | 'Processor' | 'Publisher' | null;
  metadataPath: string;
  technicalName: string;
  scanErrors: string;
  status: 'Ready' | 'Success' | 'Error';
  version: string;
}

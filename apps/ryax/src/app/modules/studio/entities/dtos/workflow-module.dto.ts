// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
export interface WorkflowModuleLightDto {
  description: string;
  id: string;
  module_id: string;
  kind: string;
  name: string;
  version: string;
}

export interface WorkflowModuleDetailedDto {
  description: string;
  id: string;
  lockfile: string;
  module_id: string;
  kind: 'Source' | 'Processor';
  custom_name?: string;
  name: string;
  technical_name: string;
  has_dynamic_outputs: boolean;
  version: string;
  versions: { version: string; id: string }[];
  resources: ResourcesDto | null;
  categories: CategoryDto[];
  inputs: IosDto[];
  outputs: IosDto[];
  dynamic_outputs: IosDto[];
  addons_inputs: IosDto[];
  constraints: {
    site_list: string[];
    site_type_list: string[];
    node_pool_list: string[];
    arch_list: string[];
  };
  objectives: {
    energy: number;
    cost: number;
    performance: number;
  };
}

interface ResourcesDto {
  cpu: number | null;
  gpu: number | null;
  time: number | null;
  memory: number | null;
}

interface CategoryDto {
  name: string;
  id: string;
}

export interface IosDto {
  display_name: string;
  enum_values: string[];
  help: string;
  id: string;
  technical_name: string;
  type: string;
  reference_value?: string;
  project_variable_value?: string;
  static_value?: string | number | boolean | null;
  default_value?: string | number | boolean | null;
  origin?: string | null;
  optional?: boolean;
}

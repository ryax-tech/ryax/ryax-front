export interface NodePoolDto {
  id: string;
  name: string;
  architecture: string;
  cpu: number;
  memory: number;
  gpu: number;
  maximum_time_in_sec: number;
}
export interface SiteDto {
  id: string;
  name: string;
  type: 'KUBERNETES' | 'HPC';
  node_pools: NodePoolDto[];
}
export interface SitesDto {
  sites: SiteDto[];
}

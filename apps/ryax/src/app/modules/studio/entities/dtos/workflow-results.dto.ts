export interface WorkflowResultConfigItemDto {
  workflow_module_io_id: string;
  description: string;
  key: string;
  type: string;
}

export interface WorkflowResultConfigDto {
  type: string;
  workflow_results: WorkflowResultConfigItemDto[];
}

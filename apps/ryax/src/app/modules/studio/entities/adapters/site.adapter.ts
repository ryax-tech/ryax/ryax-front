import { NodePoolDto, SiteDto } from '../dtos/sites';
import { NodePool, Site } from '../sites';

export class SiteAdapter {
  private adaptNodePool(dto: NodePoolDto): NodePool {
    return {
      cpu: dto.cpu,
      name: dto.name,
      id: dto.id,
      timeInSec: dto.maximum_time_in_sec,
      gpu: dto.gpu,
      arch: dto.architecture,
      memoryInMB: (dto.memory / 1024 / 1024).toFixed(0),
    };
  }
  public adapt(dtos: SiteDto[]): Site[] {
    return dtos.map((dto) => ({
      id: dto.id,
      name: dto.name,
      type: dto.type as 'KUBERNETES' | 'SLURM_SSH',
      nodePools: dto.node_pools.map((nodePool) => this.adaptNodePool(nodePool)),
    }));
  }
}

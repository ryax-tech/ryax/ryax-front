// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import {
  IosDto,
  WorkflowModuleDetailedDto,
} from '../../dtos/workflow-module.dto';
import { WorkflowActionDetailed } from '../../workflow-action';
import {
  FormValues,
  OutputFormValues,
  WorkflowStep,
} from '../../workflow-step';
import {
  HumanizeDuration,
  HumanizeDurationLanguage,
} from 'humanize-duration-ts';

export class WorkflowModuleAdapter {
  langService: HumanizeDurationLanguage = new HumanizeDurationLanguage();
  humanizer: HumanizeDuration = new HumanizeDuration(this.langService);

  humanFileSize(size: number) {
    const i = size == 0 ? 0 : Math.floor(Math.log(size) / Math.log(1024));
    return (
      Number((size / Math.pow(1024, i)).toFixed(2)) +
      ' ' +
      ['B', 'kB', 'MB', 'GB', 'TB'][i]
    );
  }

  public adaptDetailed(dto: WorkflowModuleDetailedDto): WorkflowActionDetailed {
    return {
      id: dto.id,
      definitionId: dto.module_id,
      name: dto.custom_name ? dto.custom_name : dto.name,
      description: dto.description,
      kind: dto.kind,
      hasDynamic: dto.has_dynamic_outputs,
      version: dto.version,
      technicalName: dto.technical_name,
      lockfile: dto.lockfile,
      mainValue: dto.inputs[0] ? dto.inputs[0].id : '',
      categories: dto.categories,
      versions: dto.versions?.map((versionDto) => ({
        name: versionDto.version,
        id: versionDto.id,
      })),
      resources: {
        cpu: dto.resources?.cpu,
        memory: dto.resources?.memory
          ? this.humanFileSize(dto.resources.memory)
          : null,
        time: dto.resources?.time
          ? this.humanizer.humanize(dto.resources.time * 1000)
          : null,
        gpu: dto.resources?.gpu,
      },
      constraints:
        'constraints' in dto
          ? {
              siteList: dto.constraints.site_list,
              siteTypeList: dto.constraints.site_type_list,
              archList: dto.constraints.arch_list,
              nodePoolList: dto.constraints.node_pool_list,
            }
          : null,
      objectives:
        'objectives' in dto
          ? {
              energy: dto.objectives.energy,
              cost: dto.objectives.cost,
              performance: dto.objectives.performance,
            }
          : null,
      inputs:
        dto.inputs?.map((input) => ({
          id: input.id,
          name: input.display_name,
          technicalName: input.technical_name,
          optional: input.optional ? input.optional : false,
          help: input.help,
          inputType: input.type,
          enumValues: input.enum_values?.map((value) => ({
            value,
            name: value,
          })),
        })) ?? [],
      addonsInputs:
        dto.addons_inputs?.map((input) => ({
          id: input.id,
          name: input.display_name,
          technicalName: input.technical_name,
          optional: input.optional ? input.optional : false,
          help: input.help,
          inputType: input.type,
          enumValues: input.enum_values?.map((value) => ({
            value,
            name: value,
          })),
        })) ?? [],
      outputs:
        dto.outputs?.map((output) => ({
          id: output.id,
          help: output.help,
          name: output.display_name,
          technicalName: output.technical_name,
          type: output.type,
          enumValues: output.enum_values,
          optional: output.optional ? output.optional : false,
        })) ?? [],
      dynamicOutputs:
        dto.dynamic_outputs?.map((dyna_output) => ({
          id: dyna_output.id,
          help: dyna_output.help,
          name: dyna_output.display_name,
          technicalName: dyna_output.technical_name,
          type: dyna_output.type,
          enumValues: dyna_output.enum_values,
          origin: dyna_output.origin,
          optional: dyna_output.optional ? dyna_output.optional : false,
        })) ?? [],
    } as WorkflowActionDetailed;
  }

  public adaptModulesToStudio(
    modules: WorkflowModuleDetailedDto[] | undefined
  ): WorkflowStep[] {
    if (modules && modules.length > 0) {
      return modules.map((moduleDto) => {
        return {
          name: moduleDto.custom_name ? moduleDto.custom_name : moduleDto.name,
          status: 'READY',
          type: moduleDto.kind === 'Source' ? 'SOURCE' : 'PROCESS',
          panels: [
            {
              name: 'Select',
              status: 'finish',
              originalValue: new WorkflowModuleAdapter().adaptDetailed(
                moduleDto
              ),
            },
            {
              name: 'Configure',
              inputFormValues: this.adaptInputs(moduleDto.inputs),
              addonInputFormValues: this.adaptInputs(moduleDto.addons_inputs),
              outputFormValues: this.adaptDynamicOutput(
                moduleDto.dynamic_outputs
              ),
              status: 'finish',
            },
          ],
          currentPanelIndex: 2,
        };
      });
    } else {
      return [
        {
          name: 'Source',
          status: 'UNDEFINED',
          type: 'SOURCE',
          panels: [
            {
              name: 'Select',
              status: 'process',
              originalValue: undefined,
            },
            {
              name: 'Configure',
              inputFormValues: {},
              addonInputFormValues: {},
              outputFormValues: {},
              status: 'wait',
            },
          ],
          currentPanelIndex: 0,
        },
      ];
    }
  }

  public adaptInputs(inputs: IosDto[]): FormValues {
    return inputs.reduce((prev, input) => {
      let inputValue;
      if (input.reference_value) {
        inputValue = { valueType: 'link', value: input.reference_value };
      } else if (input.project_variable_value) {
        inputValue = {
          valueType: 'variable',
          value: input.project_variable_value,
        };
      } else if (input.static_value !== undefined) {
        inputValue = { valueType: 'static', value: input.static_value };
      } else {
        inputValue = {
          valueType: 'static',
          value: input.default_value ? input.default_value : null,
        };
      }
      return { ...prev, [input.id]: inputValue };
    }, {});
  }

  public adaptDynamicOutput(dyna_outputs: IosDto[]): OutputFormValues {
    return dyna_outputs?.reduce(
      (
        sum: OutputFormValues,
        { id, help, type, display_name, enum_values, optional, origin }
      ) => {
        return {
          ...sum,
          [id]: {
            type,
            help,
            optional: optional ? optional : false,
            name: display_name,
            origin: origin ?? 'path',
            enumValues: enum_values,
          },
        };
      },
      {}
    );
  }
}

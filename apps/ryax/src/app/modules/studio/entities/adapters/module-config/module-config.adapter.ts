// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import {
  ActionIODefinition,
  ActionOutput,
  InputSection,
} from '../../workflow-action';
import { FormValues, OutputFormValues } from '../../workflow-step';

export class ModuleConfigAdapter {
  public adaptInputs(
    formValues: FormValues,
    inputSection: InputSection,
    moduleConf: {
      customName: string;
      inputs: ActionIODefinition[] | undefined;
      addonsInputs: ActionIODefinition[] | undefined;
    }
  ) {
    let rawInputs;
    if (inputSection == InputSection.addonsInputs) {
      rawInputs = moduleConf.addonsInputs;
    } else {
      //(inputSection == InputSection.inputs) {
      rawInputs = moduleConf.inputs;
    }
    const inputs = rawInputs
      ?.filter((input) => {
        // Do not send value for files or directories that are static
        return !(
          (input.inputType === 'file' || input.inputType === 'directory') &&
          formValues[input.id].valueType === 'static'
        );
      })
      .map((input) => {
        let key;
        switch (formValues[input.id]?.valueType) {
          case 'link': {
            key = 'reference_value';
            break;
          }
          case 'variable': {
            key = 'project_variable_value';
            break;
          }
          case 'static': {
            key = 'static_value';
            break;
          }
        }
        let formValue;
        if (formValues[input.id].value !== undefined) {
          formValue = formValues[input.id].value;
        }

        return { id: input.id, [key]: formValue };
      });

    if (inputSection == InputSection.addonsInputs) {
      return { custom_name: moduleConf.customName, addons_inputs: inputs };
    } else {
      //(inputSection == InputSection.inputs) {
      return { custom_name: moduleConf.customName, inputs: inputs };
    }
  }

  public adaptOutput(
    formValues: OutputFormValues,
    moduleConf: { customName: string; outputs: ActionOutput[] | undefined }
  ) {
    const dynamic_outputs = moduleConf.outputs?.map((output) => {
      return {
        help: formValues[output.id].help,
        id: output.id,
        technical_name: output.technicalName,
        display_name: formValues[output.id].name,
        type: formValues[output.id].type,
        origin: formValues[output.id].origin,
        enum_values: formValues[output.id].enumValues,
        optional: formValues[output.id].optional,
      };
    });

    return { dynamic_outputs };
  }
}

// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
export class ModuleLinkAdapter {
  public adapt(moduleIdList: string[]) {
    return {
      links: moduleIdList.map((id, index) => ({
        module_id: id,
        next_modules_ids: moduleIdList[index + 1]
          ? [moduleIdList[index + 1]]
          : [],
      })),
    };
  }
}

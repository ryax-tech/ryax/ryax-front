import {
  WorkflowResultConfigDto,
  WorkflowResultConfigItemDto,
} from '../dtos/workflow-results.dto';
import {
  WorkflowResultConfig,
  WorkflowResultConfigItem,
} from '../workflow-results';

export class WorkflowResultsAdapter {
  public adapt(dto: WorkflowResultConfigDto): WorkflowResultConfig {
    return {
      workflow_results: this.adaptItem(dto.workflow_results),
      type: dto.type as 'JSON' | 'FileOrDir',
    };
  }

  public adaptItem(
    dtos: WorkflowResultConfigItemDto[]
  ): WorkflowResultConfigItem[] {
    return dtos.map((dto) => ({
      description: dto.description,
      key: dto.key,
      value: dto.workflow_module_io_id,
      type: dto.type,
    }));
  }
}

// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

export interface WorkflowActionLight {
  id: string;
  definitionId: string;
  name: string;
  kind: string;
  description: string;
  version: string;
}

export interface ActionResources {
  cpu: number | null;
  gpu: number | null;
  time: string | null;
  memory: string | null;
}

export interface ActionDeployConstraints {
  siteList: string[];
  nodePoolList: string[];
  archList: string[];
  siteTypeList: string[];
}

export interface ActionDeployObjectives {
  energy: number;
  cost: number;
  performance: number;
}

export interface WorkflowActionDetailed {
  id: string;
  definitionId: string;
  name: string;
  technicalName: string;
  kind: 'Source' | 'Processor';
  description: string;
  version: string;
  mainValue: string;
  hasDynamic: boolean;
  lockfile: string;
  categories: { name: string; id: string }[];
  versions: { name: string; id: string }[];
  resources: ActionResources;
  inputs: ActionIODefinition[];
  addonsInputs: ActionIODefinition[];
  outputs: ActionOutput[];
  dynamicOutputs: ActionOutput[];
  constraints: ActionDeployConstraints | null;
  objectives: ActionDeployObjectives | null;
}

export interface ActionIODefinition {
  id: string;
  inputType: string;
  name: string;
  technicalName: string;
  help?: string;
  optional: boolean;
  enumValues?: Option[];
}

export enum InputSection {
  inputs,
  addonsInputs,
}

interface Option {
  value: unknown;
  name: string;
  default?: boolean;
}

export interface ActionOutput {
  id: string;
  name: string;
  technicalName: string;
  help: string;
  type: string;
  optional: boolean;
  enumValues?: string[];
  origin?: string;
}

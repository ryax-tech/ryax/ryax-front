export interface WorkflowResultConfigItem {
  key: string;
  value: string;
  type: string;
  description: string;
}

export interface WorkflowResultConfig {
  type: 'JSON' | 'FileOrDir';
  workflow_results: WorkflowResultConfigItem[];
}

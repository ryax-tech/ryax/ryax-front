// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { WorkflowActionDetailed } from './workflow-action';

export interface WorkflowStep {
  name: string;
  status: StepStatus;
  type: 'SOURCE' | 'PROCESS';
  panels: ModulePanels;
  currentPanelIndex: number;
}

export type ModulePanels = [SelectPanel, ...ConfigurationPanel[]];

interface SelectPanel {
  name: string;
  status: PanelStatus;
  originalValue?: WorkflowActionDetailed;
}

interface ConfigurationPanel {
  name: string;
  status: PanelStatus;
  inputFormValues: FormValues;
  outputFormValues: OutputFormValues;
  addonInputFormValues: FormValues;
}

export interface FormValues {
  [key: string]: {
    valueType: 'link' | 'static' | 'variable';
    value: string | number | null;
  };
}

export interface OutputFormValues {
  [key: string]: {
    type: string;
    name: string;
    help: string;
    origin: string;
    optional: boolean;
    enumValues: string[];
  };
}

export type StepStatus = 'UNDEFINED' | 'UNCONFIGURED' | 'READY';
export type PanelStatus = 'wait' | 'process' | 'finish' | 'error';

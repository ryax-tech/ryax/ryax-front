// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component } from '@angular/core';
import { environment } from '../../../../environments/environment';
import { Roles } from '../../auth/entities';

@Component({
  selector: 'ryax-header',
  templateUrl: './header.component.pug',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent {
  public studioRoles = [Roles.ADMIN, Roles.DEVELOPER];
  public displayDemo = environment.demo;
}

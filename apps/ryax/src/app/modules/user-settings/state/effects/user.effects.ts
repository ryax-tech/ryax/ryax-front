// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { NzNotificationService } from 'ng-zorro-antd/notification';
import { of } from 'rxjs';
import { catchError, map, switchMap, tap } from 'rxjs/operators';
import { User } from '../../entities';
import { UserActions } from '../actions';
import { UserApiService } from '../../services/user-api/user-api.service';
import { AuthFacade } from '../../../auth/state/facade';

@Injectable()
export class UserEffects {
  user$ = createEffect(() =>
    this.actions$.pipe(
      ofType(UserActions.get),
      switchMap(() =>
        this.userApiService.getUser().pipe(
          map((user: User) => UserActions.getUserSuccess(user)),
          catchError((err: HttpErrorResponse) => {
            return of(UserActions.getUserError(err));
          })
        )
      )
    )
  );

  userPassword$ = createEffect(() =>
    this.actions$.pipe(
      ofType(UserActions.updatePassword),
      switchMap(({ password }) =>
        this.userApiService.updateUserPassword(password).pipe(
          map((user: User) => UserActions.updatePasswordSuccess(user)),
          catchError((err: HttpErrorResponse) => {
            return of(UserActions.updatePasswordError(err));
          })
        )
      )
    )
  );

  userEmail$ = createEffect(() =>
    this.actions$.pipe(
      ofType(UserActions.updateEmail),
      switchMap(({ email }) =>
        this.userApiService.updateUserEmail(email).pipe(
          map((user: User) => UserActions.updateEmailSuccess(user)),
          catchError((err: HttpErrorResponse) => {
            return of(UserActions.updateEmailError(err));
          })
        )
      )
    )
  );
  userName$ = createEffect(() =>
    this.actions$.pipe(
      ofType(UserActions.updateName),
      switchMap(({ name }) =>
        this.userApiService.updateUserName(name).pipe(
          map((user: User) => UserActions.updateNameSuccess(user)),
          catchError((err: HttpErrorResponse) => {
            return of(UserActions.updateNameError(err));
          })
        )
      )
    )
  );

  displayErrors$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(
          UserActions.getUserError,
          UserActions.updatePasswordError,
          UserActions.updateEmailError,
          UserActions.updateNameError
        ),
        tap((errAction) =>
          this.notification.create(
            'error',
            'Something went wrong !\nhttp error: ' + errAction.error.status,
            errAction.error.message
          )
        )
      ),
    { dispatch: false }
  );

  displaySuccess$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(
          UserActions.updatePasswordSuccess,
          UserActions.updateEmailSuccess,
          UserActions.updateNameSuccess
        ),
        tap(() =>
          this.notification.create('success', 'User updated successfully', '')
        ),
        tap(() => this.authFacade.getUser())
      ),
    { dispatch: false }
  );

  constructor(
    private actions$: Actions,
    private userApiService: UserApiService,
    private notification: NzNotificationService,
    private authFacade: AuthFacade
  ) {}
}

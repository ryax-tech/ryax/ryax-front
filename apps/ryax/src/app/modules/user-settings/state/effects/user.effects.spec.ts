// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { HttpErrorResponse } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { Actions } from '@ngrx/effects';
import { provideMockActions } from '@ngrx/effects/testing';
import { provideMockStore } from '@ngrx/store/testing';
import { cold, hot } from 'jest-marbles';
import { NzNotificationService } from 'ng-zorro-antd/notification';
import { Observable } from 'rxjs';
import { Roles } from '../../entities';
import { UserApiService } from '../../services/user-api/user-api.service';
import { UserActions } from '../actions';
import { UserEffects } from './user.effects';

describe('UserEffects', () => {
  let effects: UserEffects;
  let actions$: Observable<Actions>;

  const userApiServiceSpy = {
    getUser: jest.fn(),
  };

  const notifSpy = {
    create: jest.fn(),
  };

  beforeEach(() => {
    userApiServiceSpy.getUser.mockClear();

    TestBed.configureTestingModule({
      providers: [
        UserEffects,
        provideMockStore(),
        provideMockActions(() => actions$),
        { provide: UserApiService, useValue: userApiServiceSpy },
        { provide: NzNotificationService, useValue: notifSpy },
      ],
    });
    effects = TestBed.inject(UserEffects);
  });

  it('should be created', () => {
    expect(effects).toBeTruthy();
  });

  it('should try to get user details and return them', () => {
    const userDetails = {
      name: 'Test',
      role: Roles.ADMIN,
      comment: '',
      email: 'toto@tata.com',
    };

    userApiServiceSpy.getUser.mockImplementation(() =>
      cold('-a|', { a: userDetails })
    );

    actions$ = hot('-a', {
      a: UserActions.get(),
    });

    const expected$ = hot('--a', {
      a: UserActions.getUserSuccess(userDetails),
    });

    expect(effects.user$).toBeObservable(expected$);
    expect(expected$).toSatisfyOnFlush(() => {
      expect(userApiServiceSpy.getUser).toHaveBeenCalled();
    });
  });

  it('should try to get user details and return error when an error is received', () => {
    userApiServiceSpy.getUser.mockImplementation(() =>
      cold('-#', {}, { error: '400' })
    );

    actions$ = hot('-a', {
      a: UserActions.get(),
    });

    const expected$ = hot('--a', {
      a: UserActions.getUserError({ error: '400' } as HttpErrorResponse),
    });

    expect(effects.user$).toBeObservable(expected$);
    expect(expected$).toSatisfyOnFlush(() => {
      expect(userApiServiceSpy.getUser).toHaveBeenCalled();
    });
  });
});

// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { TestBed } from '@angular/core/testing';
import { Store } from '@ngrx/store';

import { UserSettingsFacade } from './index';

describe('UserSettingsFacade', () => {
  let service: UserSettingsFacade;
  const storeSpy = {
    dispatch: jest.fn(),
    select: jest.fn(),
  };
  beforeEach(() => {
    storeSpy.dispatch.mockClear();

    TestBed.configureTestingModule({
      providers: [UserSettingsFacade, { provide: Store, useValue: storeSpy }],
    });
    service = TestBed.inject(UserSettingsFacade);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should dispatch getUser action', () => {
    service.getUser();
    expect(storeSpy.dispatch).toHaveBeenCalledWith({
      type: '[User] Get User details',
    });
  });
});

// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { UserDto } from '../../entities/dtos';
import { Roles } from '../../entities/index';
import { User } from '../../entities/user';

import { UserApiService } from './user-api.service';

describe('UserApiService', () => {
  let service: UserApiService;
  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [UserApiService],
    });
    service = TestBed.inject(UserApiService);
    httpTestingController = TestBed.inject(HttpTestingController);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should get user when asks', () => {
    const user: User = {
      name: 'toto',
      role: Roles.ADMIN,
      email: 'toto',
      comment: 'toto',
    };
    const userDto: UserDto = {
      username: 'toto',
      comment: '',
      email: 'toto@tata.com',
      id: '1234',
      role: Roles.ADMIN,
    };

    service.getUser().subscribe((response) => {
      expect(response).toEqual(user);
    });

    const request = httpTestingController.expectOne('/authorization/me');
    expect(request.request.method).toEqual('GET');
    request.flush(userDto);
  });
});

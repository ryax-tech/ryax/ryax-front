// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { UserSettingsComponent } from './components/user-settings/user-settings.component';

export const UserSettingsRoutes = [
  {
    path: '',
    component: UserSettingsComponent,
  },
];

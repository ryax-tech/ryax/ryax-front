// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserChangeUsernameComponent } from './user-change-username.component';

describe('UserChangeUsernameComponent', () => {
  let component: UserChangeUsernameComponent;
  let fixture: ComponentFixture<UserChangeUsernameComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [UserChangeUsernameComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UserChangeUsernameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from '../../entities';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { UserSettingsFacade } from '../../state/facade';

@Component({
  selector: 'ryax-user-change-email',
  templateUrl: './user-change-email.component.pug',
  styleUrls: ['./user-change-email.component.scss'],
})
export class UserChangeEmailComponent {
  @Input() user$: Observable<User | null> = new Observable<User | null>();
  @Output() changingEmailDone = new EventEmitter<null>();
  validateForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    private userSettingsFacade: UserSettingsFacade
  ) {
    this.validateForm = this.fb.group({
      email: ['', [Validators.email, Validators.required]],
      confirm: ['', [this.confirmValidator]],
    });
  }

  submitForm(): void {
    this.userSettingsFacade.updateEmail(this.validateForm.value.email);
    this.changingEmailDone.emit();
  }

  cancel(): void {
    this.validateForm.reset();
    this.changingEmailDone.emit();
  }

  validateConfirmEmail(): void {
    setTimeout(() =>
      this.validateForm.controls['confirm'].updateValueAndValidity()
    );
  }

  confirmValidator = (control: FormControl): { [s: string]: boolean } => {
    if (!control.value) {
      return { error: true, required: true };
    } else if (control.value !== this.validateForm.controls['email'].value) {
      return { confirm: true, error: true };
    }
    return {};
  };
}

// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Component, OnInit } from '@angular/core';
import { UserSettingsFacade } from '../../state/facade';

@Component({
  selector: 'ryax-user-settings',
  templateUrl: './user-settings.component.pug',
  styleUrls: ['./user-settings.component.scss'],
})
export class UserSettingsComponent implements OnInit {
  public user$ = this.userSettingsFacade.user$;
  public loading$ = this.userSettingsFacade.loading$;
  public error$ = this.userSettingsFacade.error$;
  public changingPassword = false;
  public changingEmail = false;
  public changingName = false;

  constructor(private userSettingsFacade: UserSettingsFacade) {}

  public ngOnInit(): void {
    this.userSettingsFacade.getUser();
  }
}

// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import {
  UserApiAuthApiKeyDto,
  UserApiAuthApiKeySpecDto,
  UserApiAuthDto,
  UserApiAuthNewApiKeyDto,
} from '../dtos';
import {
  UserApiAuth,
  UserApiAuthApiKey,
  UserApiAuthApiKeySpec,
  UserApiAuthApiKeyStatus,
  UserApiAuthNewApiKey,
} from '../user-api-auth';
import { DateTime } from 'luxon';

export class UserApiAuthAdapter {
  public adapt(dto: UserApiAuthDto): UserApiAuth {
    return { apiKey: this.adaptApiKeySpec(dto.api_key) };
  }

  public adaptApiKeySpec(
    dtos: UserApiAuthApiKeySpecDto
  ): UserApiAuthApiKeySpec {
    return { enabled: dtos.enabled, keys: this.adaptApiKeyList(dtos.keys) };
  }

  public adaptApiKeyList(dtos: UserApiAuthApiKeyDto[]): UserApiAuthApiKey[] {
    return dtos.map((dto) => {
      return {
        id: dto.id,
        name: dto.name,
        creationDate: DateTime.fromISO(dto.creation_date),
        expirationDate: DateTime.fromISO(dto.expiration_date),
        lastUseDate:
          dto.last_use_date == null
            ? 'Never'
            : DateTime.fromISO(dto.last_use_date),
        status: dto.status as UserApiAuthApiKeyStatus,
      };
    });
  }

  public adaptNewKey(dto: UserApiAuthNewApiKeyDto): UserApiAuthNewApiKey {
    return {
      id: dto.id,
      name: dto.name,
      apiKey: dto.api_key,
      expirationDate: DateTime.fromISO(dto.expiration_date),
    };
  }
}

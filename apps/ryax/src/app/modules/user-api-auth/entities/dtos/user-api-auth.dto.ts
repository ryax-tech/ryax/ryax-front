// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
export type UserApiAuthApiKeyStatusDto = 'Valid' | 'Expired' | 'Revoked';

export interface UserApiAuthDto {
  api_key: UserApiAuthApiKeySpecDto;
}

export interface UserApiAuthApiKeySpecDto {
  enabled: boolean;
  keys: UserApiAuthApiKeyDto[];
}

export interface UserApiAuthApiKeyDto {
  id: string;
  name: string;
  creation_date: string;
  expiration_date: string;
  last_use_date: string;
  status: UserApiAuthApiKeyStatusDto;
}

export interface UserApiAuthNewApiKeyDto {
  name: string;
  id: string;
  api_key: string;
  expiration_date: string;
}

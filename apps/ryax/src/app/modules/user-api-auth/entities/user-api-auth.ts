// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { DateTime } from 'luxon';

export enum UserApiAuthApiKeyStatus {
  Valid = 'Valid',
  Expired = 'Expired',
  Revoked = 'Revoked',
}

export interface UserApiAuth {
  apiKey: UserApiAuthApiKeySpec;
}

export interface UserApiAuthApiKeySpec {
  enabled: boolean;
  keys: UserApiAuthApiKey[];
}

export interface UserApiAuthApiKey {
  id: string;
  name: string;
  creationDate: DateTime;
  expirationDate: DateTime;
  lastUseDate: DateTime | string;
  status: UserApiAuthApiKeyStatus;
}

export interface UserApiAuthNewApiKey {
  id: string;
  name: string;
  apiKey: string;
  expirationDate: DateTime;
}

// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { NzNotificationService } from 'ng-zorro-antd/notification';
import { of } from 'rxjs';
import { catchError, map, switchMap, tap } from 'rxjs/operators';
import { UserApiAuth } from '../../entities';
import { UserApiAuthActions } from '../actions';
import { UserApiAuthApiService } from '../../services/user-api-auth-api.service';

@Injectable()
export class UserApiAuthEffects {
  getUserApiAuth$ = createEffect(() =>
    this.actions$.pipe(
      ofType(UserApiAuthActions.getUserApiAuth),
      switchMap(() =>
        this.userApiAuthService.getUserApiAuth().pipe(
          map((userApiAuth: UserApiAuth) =>
            UserApiAuthActions.userApiAuthSuccess(userApiAuth)
          ),
          catchError((err: HttpErrorResponse) => {
            return of(UserApiAuthActions.userApiAuthError(err));
          })
        )
      )
    )
  );

  getNewApiKey$ = createEffect(() =>
    this.actions$.pipe(
      ofType(UserApiAuthActions.getNewApiKey),
      switchMap(({ name, expirationDate }) =>
        this.userApiAuthService.addApiKey(name, expirationDate).pipe(
          map((newApiKey) => UserApiAuthActions.getNewApiKeySuccess(newApiKey)),
          catchError((err: HttpErrorResponse) => {
            return of(UserApiAuthActions.userApiAuthError(err));
          })
        )
      )
    )
  );

  toggleApiKey$ = createEffect(() =>
    this.actions$.pipe(
      ofType(UserApiAuthActions.toggleApiKeyEnable),
      switchMap(() =>
        this.userApiAuthService.toggleApiKeyAuth().pipe(
          map((userApiAuth: UserApiAuth) =>
            UserApiAuthActions.userApiAuthSuccess(userApiAuth)
          ),
          catchError((err: HttpErrorResponse) => {
            return of(UserApiAuthActions.userApiAuthError(err));
          })
        )
      )
    )
  );

  revokeApiKey$ = createEffect(() =>
    this.actions$.pipe(
      ofType(UserApiAuthActions.revokeApiKey),
      switchMap(({ apiKeyId }) =>
        this.userApiAuthService.revokeApiKey(apiKeyId).pipe(
          map((userApiAuth: UserApiAuth) =>
            UserApiAuthActions.userApiAuthSuccess(userApiAuth)
          ),
          catchError((err: HttpErrorResponse) => {
            return of(UserApiAuthActions.userApiAuthError(err));
          })
        )
      )
    )
  );

  deleteApiKey$ = createEffect(() =>
    this.actions$.pipe(
      ofType(UserApiAuthActions.deleteApiKey),
      switchMap(({ apiKeyId }) =>
        this.userApiAuthService.deleteApiKey(apiKeyId).pipe(
          map((userApiAuth: UserApiAuth) =>
            UserApiAuthActions.userApiAuthSuccess(userApiAuth)
          ),
          catchError((err: HttpErrorResponse) => {
            return of(UserApiAuthActions.userApiAuthError(err));
          })
        )
      )
    )
  );

  displayErrors$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(UserApiAuthActions.userApiAuthError),
        tap((errAction) =>
          this.notification.create(
            'error',
            'Something went wrong ! http error : ' + errAction.error.status,
            errAction.error.message + ': ' + errAction.error.error.error
          )
        )
      ),
    { dispatch: false }
  );

  constructor(
    private actions$: Actions,
    private userApiAuthService: UserApiAuthApiService,
    private notification: NzNotificationService
  ) {}
}

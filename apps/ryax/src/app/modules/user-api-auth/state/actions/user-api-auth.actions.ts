import { createAction } from '@ngrx/store';
import { HttpErrorResponse } from '@angular/common/http';
import { UserApiAuth, UserApiAuthNewApiKey } from '../../entities';

export const getUserApiAuth = createAction(
  '[User API Auth Get] Get User API auth'
);

export const userApiAuthSuccess = createAction(
  '[User API Auth Get] Get User API auth success',
  (userApiAuth: UserApiAuth) => ({ userApiAuth })
);

export const userApiAuthError = createAction(
  '[User API Auth Get] Get User API auth error',
  (error: HttpErrorResponse) => ({ error })
);

export const getNewApiKey = createAction(
  '[User API Auth API Key Create] Create User API auth API Key',
  (name: string, expirationDate: Date | null) => ({ name, expirationDate })
);

export const getNewApiKeySuccess = createAction(
  '[User API Auth API Key Creation Success] User API auth API Key created',
  (userApiAuthNewKey: UserApiAuthNewApiKey) => ({ userApiAuthNewKey })
);

export const cleanApiKey = createAction(
  '[User API Auth API Key Clean] Clean User API auth API key'
);

export const revokeApiKey = createAction(
  '[User API Auth API Key Revoke] Revoke User API auth API Key',
  (apiKeyId: string) => ({ apiKeyId })
);

export const deleteApiKey = createAction(
  '[User API Auth API Key Delete] Delete User API auth API Key',
  (apiKeyId: string) => ({ apiKeyId })
);

export const toggleApiKeyEnable = createAction(
  '[User API Auth API Key Disable Method] Disable Method API Key as User api auth'
);

// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { userApiAuthReducer, UserApiAuthState } from './user-api-auth.reducer';
import { ActionReducerMap } from '@ngrx/store';
import { InjectionToken } from '@angular/core';

export * from './user-api-auth.reducer';

export const UserApiAuthFeatureKey = 'globalUserApiAuthDomain';

export interface GlobalUserApiAuthState {
  userApiAuth: UserApiAuthState;
}

export const UserApiAuthReducerToken = new InjectionToken<
  ActionReducerMap<GlobalUserApiAuthState>
>(UserApiAuthFeatureKey);

export const UserApiAuthReducerProvider = {
  provide: UserApiAuthReducerToken,
  useValue: {
    userApiAuth: userApiAuthReducer,
  },
};

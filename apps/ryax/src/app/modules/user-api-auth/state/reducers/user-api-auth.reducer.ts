// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { HttpErrorResponse } from '@angular/common/http';
import { createReducer, on } from '@ngrx/store';
import { UserApiAuth, UserApiAuthNewApiKey } from '../../entities';
import { UserApiAuthActions } from '../actions';

export interface UserApiAuthState {
  data: UserApiAuth | null;
  newKey: UserApiAuthNewApiKey | null;
  loading: boolean;
  error: HttpErrorResponse | null;
}

export const initialUserApiAuthState: UserApiAuthState = {
  data: null,
  newKey: null,
  loading: false,
  error: null,
};

export const userApiAuthReducer = createReducer<UserApiAuthState>(
  initialUserApiAuthState,
  on(UserApiAuthActions.getUserApiAuth, (state) => ({
    ...state,
    loading: true,
    error: null,
  })),
  on(UserApiAuthActions.userApiAuthSuccess, (state, { userApiAuth }) => ({
    ...state,
    data: userApiAuth,
    loading: false,
    error: null,
  })),
  on(UserApiAuthActions.userApiAuthError, (state, { error }) => ({
    ...state,
    loading: false,
    error,
  })),
  on(UserApiAuthActions.getNewApiKey, (state) => ({
    ...state,
    loading: true,
    error: null,
  })),
  on(
    UserApiAuthActions.getNewApiKeySuccess,
    (state, { userApiAuthNewKey }) => ({
      ...state,
      newKey: userApiAuthNewKey,
      loading: false,
      error: null,
    })
  ),
  on(UserApiAuthActions.cleanApiKey, (state) => ({
    ...state,
    newKey: null,
    loading: false,
    error: null,
  }))
);

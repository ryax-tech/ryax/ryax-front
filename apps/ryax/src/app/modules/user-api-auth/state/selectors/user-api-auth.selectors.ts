// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { createFeatureSelector, createSelector } from '@ngrx/store';
import {
  GlobalUserApiAuthState,
  UserApiAuthFeatureKey,
  UserApiAuthState,
} from '../reducers';

const selectUserApiAuthStateFn = (state: GlobalUserApiAuthState) =>
  state.userApiAuth;
const selectUserApiAuthErrorFn = (state: UserApiAuthState) => state.error;
const selectUserApiAuthLoadingFn = (state: UserApiAuthState) => state.loading;
const selectUserApiAuthApiKeyListFn = (state: UserApiAuthState) => {
  if (state.data) {
    return state.data.apiKey.keys;
  } else return [];
};
const selectUserApiAuthApiKeyEnabledFn = (state: UserApiAuthState) => {
  if (state.data) {
    return state.data.apiKey.enabled;
  } else return false;
};
const selectUserApiAuthNewAuthApiKeyFn = (state: UserApiAuthState) =>
  state.newKey;

export const selectGlobalApiAuthState =
  createFeatureSelector<GlobalUserApiAuthState>(UserApiAuthFeatureKey);
export const selectUserApiAuthState = createSelector(
  selectGlobalApiAuthState,
  selectUserApiAuthStateFn
);
export const selectUserApiAuthError = createSelector(
  selectUserApiAuthState,
  selectUserApiAuthErrorFn
);
export const selectUserApiAuthLoading = createSelector(
  selectUserApiAuthState,
  selectUserApiAuthLoadingFn
);
export const selectUserApiAuthApiKeyList = createSelector(
  selectUserApiAuthState,
  selectUserApiAuthApiKeyListFn
);
export const selectUserApiAuthApiKeyEnabled = createSelector(
  selectUserApiAuthState,
  selectUserApiAuthApiKeyEnabledFn
);
export const selectUserApiAuthNewApiKey = createSelector(
  selectUserApiAuthState,
  selectUserApiAuthNewAuthApiKeyFn
);

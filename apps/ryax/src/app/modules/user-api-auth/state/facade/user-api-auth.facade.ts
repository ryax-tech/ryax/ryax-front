// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { UserApiAuthActions } from '../actions';
import { UserApiAuthState } from '../reducers';
import {
  selectUserApiAuthApiKeyEnabled,
  selectUserApiAuthApiKeyList,
  selectUserApiAuthError,
  selectUserApiAuthLoading,
  selectUserApiAuthNewApiKey,
} from '../selectors';

@Injectable({
  providedIn: 'root',
})
export class UserApiAuthFacade {
  public apiKeyList$ = this.store.select(selectUserApiAuthApiKeyList);
  public apiKeyEnabled$ = this.store.select(selectUserApiAuthApiKeyEnabled);
  public userApiAuthLoading$ = this.store.select(selectUserApiAuthLoading);
  public userApiAuthError$ = this.store.select(selectUserApiAuthError);
  public newApiKey$ = this.store.select(selectUserApiAuthNewApiKey);

  constructor(private readonly store: Store<UserApiAuthState>) {}

  public getUserApiAuth() {
    this.store.dispatch(UserApiAuthActions.getUserApiAuth());
  }

  public addApiKey(name: string, expirationDate: Date | null) {
    this.store.dispatch(UserApiAuthActions.getNewApiKey(name, expirationDate));
  }

  public revokeApiKey(apiKeyId: string) {
    this.store.dispatch(UserApiAuthActions.revokeApiKey(apiKeyId));
  }

  public deleteApiKey(apiKeyId: string) {
    this.store.dispatch(UserApiAuthActions.deleteApiKey(apiKeyId));
  }

  public toggleApiKeyEnable() {
    this.store.dispatch(UserApiAuthActions.toggleApiKeyEnable());
  }

  public cleanApiKey() {
    this.store.dispatch(UserApiAuthActions.cleanApiKey());
  }
}

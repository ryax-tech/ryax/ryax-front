import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ApiKeyComponent } from './components/api-key/api-key.component';
import { SharedModule } from '../shared/shared.module';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import {
  UserApiAuthFeatureKey,
  UserApiAuthReducerProvider,
  UserApiAuthReducerToken,
} from './state/reducers';
import { UserApiAuthEffects } from './state/effects';
import { UserApiAuthApiService } from './services/user-api-auth-api.service';

@NgModule({
  declarations: [ApiKeyComponent],
  imports: [
    CommonModule,
    SharedModule,
    StoreModule.forFeature(UserApiAuthFeatureKey, UserApiAuthReducerToken),
    EffectsModule.forFeature([UserApiAuthEffects]),
  ],
  providers: [UserApiAuthReducerProvider, UserApiAuthApiService],
  exports: [ApiKeyComponent],
})
export class UserApiAuthModule {}

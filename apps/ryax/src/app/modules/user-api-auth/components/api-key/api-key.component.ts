import { Component, OnInit } from '@angular/core';
import { UserApiAuthFacade } from '../../state/facade';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NzNotificationService } from 'ng-zorro-antd/notification';

@Component({
  selector: 'ryax-api-key',
  templateUrl: './api-key.component.pug',
  styleUrls: ['./api-key.component.scss'],
})
export class ApiKeyComponent implements OnInit {
  public apiKeyList$ = this.facade.apiKeyList$;
  public apiKeyEnabled$ = this.facade.apiKeyEnabled$;
  public loading$ = this.facade.userApiAuthLoading$;
  public newApiKey$ = this.facade.newApiKey$;

  validateForm: FormGroup;

  constructor(
    private facade: UserApiAuthFacade,
    private fb: FormBuilder,
    private notification: NzNotificationService
  ) {
    this.validateForm = this.fb.group({
      name: ['', [Validators.required]],
      expirationDate: [null, []],
    });
  }

  ngOnInit() {
    this.facade.getUserApiAuth();
  }

  addApiKey() {
    this.facade.addApiKey(
      this.validateForm.value.name,
      this.validateForm.value.expirationDate
    );
  }

  closeNewApiKeyModal() {
    this.validateForm.reset();
    this.facade.getUserApiAuth();
    this.facade.cleanApiKey();
  }

  revokeApiKey(apiKeyId: string) {
    this.facade.revokeApiKey(apiKeyId);
  }

  deleteApiKey(apiKeyId: string) {
    this.facade.deleteApiKey(apiKeyId);
  }

  toggleApiKeyEnabled() {
    this.facade.toggleApiKeyEnable();
  }

  public copied(success: boolean) {
    if (success) {
      this.notification.create('success', 'Copied to clipboard!', '');
    } else {
      this.notification.create(
        'error',
        'Uh oh!',
        'Copy did not work as expected.'
      );
    }
  }

  notInThePast(date: Date): boolean {
    const now = new Date();
    return date <= now;
  }
}

// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { UserApiAuthDto, UserApiAuthNewApiKeyDto } from '../entities/dtos';
import { UserApiAuth, UserApiAuthNewApiKey } from '../entities';
import { UserApiAuthAdapter } from '../entities/adapters';
import { DateTime } from 'luxon';

@Injectable({
  providedIn: 'root',
})
export class UserApiAuthApiService {
  private baseUrl = '/api/runner/user-auth';

  constructor(private http: HttpClient) {}

  public getUserApiAuth(): Observable<UserApiAuth> {
    return this.http
      .get<UserApiAuthDto>(this.baseUrl)
      .pipe(map((dto) => new UserApiAuthAdapter().adapt(dto)));
  }

  public revokeApiKey(apiKeyId: string) {
    return this.http
      .post<UserApiAuthDto>(this.baseUrl + `/api-key/${apiKeyId}/revoke`, null)
      .pipe(map((dto) => new UserApiAuthAdapter().adapt(dto)));
  }

  public addApiKey(
    name: string,
    expiration_date: Date | null
  ): Observable<UserApiAuthNewApiKey> {
    return this.http
      .post<UserApiAuthNewApiKeyDto>(this.baseUrl + '/api-key', {
        name: name,
        expiration_date:
          expiration_date != null ? DateTime.fromJSDate(expiration_date) : null,
      })
      .pipe(map((dto) => new UserApiAuthAdapter().adaptNewKey(dto)));
  }

  public deleteApiKey(apiKeyId: string) {
    return this.http
      .delete<UserApiAuthDto>(this.baseUrl + `/api-key/${apiKeyId}`)
      .pipe(map((dto) => new UserApiAuthAdapter().adapt(dto)));
  }

  public toggleApiKeyAuth(): Observable<UserApiAuth> {
    return this.http
      .put<UserApiAuthDto>(this.baseUrl + `/api-key/toggle`, null)
      .pipe(map((dto) => new UserApiAuthAdapter().adapt(dto)));
  }
}

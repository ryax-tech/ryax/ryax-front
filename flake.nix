{
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-24.11";
    nix2container.url = "github:nlewo/nix2container";
    nix2container.inputs.nixpkgs.follows = "nixpkgs";
    flakeUtils.follows = "nix2container/flake-utils";
  };


  outputs = { self, nixpkgs, nix2container, flakeUtils }:
    # Change values here to support more arch
    flakeUtils.lib.eachSystem [ "aarch64-linux" "x86_64-linux" ]
      (system:
        let
          pkgs = import nixpkgs { inherit system; };
          nix2containerPkgs = nix2container.packages.${system};
          buidDir = "/tmp/ryax/ryax-front";
        in
        {

          devShell = (pkgs.buildFHSUserEnv {
            name = "dev-shell";
            targetPkgs = pkgs: [ pkgs.corepack pkgs.nodejs ];
          }).env;
          packages = {
            install = pkgs.writeShellApplication {
              name = "install";
              text = ''
                rm -rf ${buidDir}
                BUILD_TARGET="$1"

                echo Building "$BUILD_TARGET"
                yarn install
                yarn build:"$BUILD_TARGET"
                set -e && ls dist
                mkdir -p "$(dirname ${buidDir})"
                cp -r ./dist/apps/ryax ${buidDir}
              '';
              runtimeInputs = [ pkgs.corepack pkgs.nodejs ];
            };
            image = pkgs.callPackage ./nix/docker.nix {
              appDist = (/. + buidDir);
              nix2container = nix2containerPkgs.nix2container;
            };
          };
          # Enable autoformat
          formatter = pkgs.nixpkgs-fmt;
        }
      );
}

{ nix2container
, nginx
, buildEnv
, runCommand
, appDist
, nginxConfig ? ./nginx.conf
, tag ? "latest"
}:
let
  nginxConfigScript = runCommand "nginxConfig" { } ''
    # Create Nginx user
    mkdir -p $out/etc
    echo "nginx:x:1000:1000::/:" > $out/etc/passwd
    echo "nginx:x:1000:nginx" > $out/etc/group

    # Add Nginx config
    mkdir -p $out/etc/nginx
    ln -s ${nginxConfig} $out/etc/nginx/nginx.conf
    ln -s ${nginx}/conf/mime.types $out/etc/nginx/mime.types

    # Create /var folders
    mkdir -p $out/var/cache/nginx
    mkdir -p $out/var/log/nginx
  '';
  nginxLayer = nix2container.buildLayer {
    copyToRoot = [
      (buildEnv {
        name = "nginx";
        paths = [ nginx ];
        pathsToLink = [ "/bin" ];
      })
      nginxConfigScript
    ];
    perms = [
      {
        regex = "/(etc|var)/nginx";
        path = nginxConfigScript;
        mode = "700";
        uid = 1000;
        gid = 1000;
        uname = "nginx";
        gname = "nginx";
      }
    ];
  };


  app = runCommand "config" { } ''
    mkdir -p $out/data
    echo "Copying web app from path: ${appDist}"
    cp -r ${appDist}/* $out/data
  '';
  appLayer = nix2container.buildLayer {
    copyToRoot = [ app ];
    reproducible = false;
  };
in
nix2container.buildImage {
  name = "ryax-front";
  inherit tag;
  layers = [ nginxLayer appLayer ];
  config = {
    Cmd = [ "/bin/nginx" "-c" "/etc/nginx/nginx.conf" ];
    WorkingDir = "/data";
    ExposedPorts = {
      "80/tcp" = { };
    };
  };
}

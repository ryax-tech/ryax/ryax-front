[![coverage report](https://gitlab.com/ryax-tech/dev/ryax-front/badges/master/coverage.svg)](https://gitlab.com/ryax-tech/dev/ryax-front/-/commits/master)
[![pipeline status](https://gitlab.com/ryax-tech/dev/ryax-front/badges/master/pipeline.svg)](https://gitlab.com/ryax-tech/dev/ryax-front/-/commits/master)

# Ryax Front

This project was generated using [Nx](https://nx.dev).

## Install

Install the stack :
```sh
corepack yarn
```

Install the stack on NixOs:
```sh
nix develop
yarn
```

## Run

Use either `yarn start` to start your front locally.

Run `yarn start:testing` for a dev server. Navigate to http://localhost:9876.
The app will automatically reload if you change any of the source files.
Change `proxy-testing.conf.js` to select which dev cluster to use.

Use `yarn start:staging` to start with your front plugged to Staging-1.

## Generate new Component

Use `yarn gpc` to generate components using pug. For example, for a component named project-settings in the project module use:

```sh
yarn gpc modules/project/components/project-settings -m modules/project
```

## Lint and format

```sh
yarn lint
yarn format:write
# Or to check only
yarn format:check
```

## Build

Run `yarn build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `yarn test` to execute the unit tests via [Jest](https://jestjs.io).

Run `nx affected:test` to execute the unit tests affected by a change.
